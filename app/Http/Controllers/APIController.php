<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class APIController extends Controller
{

    public function __construct()
    {
        $this->userModel = new User;
        $this->serviceModel = new Service;
        $this->orderController = new OrderController;
    }

    public function view()
    {
        $user = Auth::user();

        if (!Auth::user()->api_token) {
            $user->api_token = Str::random(60);
            $user->save();
        }

        $data['token'] = $user->api_token;
        return view("admin.features.api", $data);
    }

    public function webhook(Request $req, $type)
    {
        switch ($type) {
            case 'order':
                $order = $req->input();
                $dataUpdate = [
                    "status" => $order["status"],
                    "notify" => $order['notify'],
                    "duration" => $order['duration'],
                    "viplike_status" => $order["viplike_status"]
                ];

                if ($order["status"] == 3) {
                    unset($dataUpdate[$order["status"]]);
                    $dataUpdate["notify"] = "Đợi hoàn tiền.";
                    $dataUpdate['note'] = $order['notify'];

                    if (isset($order["refundAmount"])) {
                        $dataUpdate['notify'] .= ' | SL: ' . $order["refundAmount"];
                    }
                }

                if (isset($order['note'])) {
                    $dataUpdate["note"] = $order["note"];
                }

                $result = DB::table("orders")->where("api_id", $order['id'])->update($dataUpdate);
                if (isset($result)) {
                    return response()->json([
                            "status" => 1,
                            "message" => "Update status oke"
                        ]
                    );
                }
                break;
            default:
                break;
        }
    }

    //Facebook
    public function LikePost(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            $req->merge([
                "service_id" => 39
            ]);

            return $this->orderController->FacebookBuff($req, "likepost", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Đang Bảo Trì... Vui Lòng Qua Server 2 !!']);
        }
    }

    public function LikePost2(Request $req)
    {
        try {
            $req->validate([
                "reaction" => "required|string",
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            $req->merge([
                "service_id" => 44
            ]);

            return $this->orderController->FacebookBuff($req, "likepost2", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Đang Bảo Trì... Vui Lòng Qua Server 1 !']);
        }
    }

    public function BuffMember(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 1000 || $req->input("amount") > 100000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 1000 đến 100000",
                    $req->input("dichvu")
                ], 400);
            }

            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 41
            ]);

            return $this->orderController->FacebookBuff($req, "buffmember", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffSubNhanh(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 1000 || $req->input("amount") > 100000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 1000 đến 100000",
                    $req->input("dichvu")
                ], 400);
            }
            // 		else if ($req->input("dichvu") != 19) {
            // 			return response()->json([
            // 				"success" => 0,
            // 				"message" => "Truyền sai dịch vụ"
            // 			], 400);
            // 		}
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 19
            ]);

            return $this->orderController->FacebookBuff($req, "buffsubnhanh", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function LikePageGiaRe(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 500 || $req->input("amount") > 50000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 500 đến 50000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 43
            ]);

            return $this->orderController->FacebookBuff($req, "bufflikepageinvite", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffShareWall(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 20 || $req->input("amount") > 5000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 20 đến 5000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            // 		if (!$checkID->success) {
            // 			return response()->json($checkID, 400);
            // 		}
            $req->merge([
                "service_id" => 10
            ]);

            return $this->orderController->FacebookBuff($req, "sharewall", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffShareLive(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 50 || $req->input("amount") > 5000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 5000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 17
            ]);

            return $this->orderController->FacebookBuff($req, "sharelive", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffBanBe(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 1000 || $req->input("amount") > 30000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 1000 đến 30000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 50
            ]);

            return $this->orderController->FacebookBuff($req, "tangbanbe", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffSubThuc(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 100 || $req->input("amount") > 100000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 100 đến 100000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 6
            ]);

            return $this->orderController->FacebookBuff($req, "sub", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLikePageThuc(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);

            if ($req->input("amount") < 100 || $req->input("amount") > 100000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 100 đến 100000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 7
            ]);

            return $this->orderController->FacebookBuff($req, "likepage", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffComment(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "noidung" => "required"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));
            $req->input("comment") == $comment;
            $arr = preg_split("/\|/", $req->input("noidung"));
            $req->merge([
                "service_id" => 38,
                "comment" => $comment,
                "amount" => count($arr),
            ]);

            if ($req->input("amount") < 10 || $req->input("amount") > 500) {
                return response()->json([
                    "success" => 0,
                    "message" => "số lượng tối thiểu là 10 comment và tối đa 500",
                ], 400);
            }

            return $this->orderController->FacebookBuff($req, "commentfb", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffComment2(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "noidung" => "required"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));
            $req->input("comment") == $comment;
            $arr = preg_split("/\|/", $req->input("noidung"));
            $req->merge([
                "service_id" => 4,
                "comment" => $comment,
                "amount" => count($arr),
            ]);

            if ($req->input("amount") < 10 || $req->input("amount") > 200) {
                return response()->json([
                    "success" => 0,
                    "message" => "số lượng tối thiểu là 5 comment và tối đa 200 comment",
                ], 400);
            }

            return $this->orderController->FacebookBuff($req, "comment", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffReviewPage(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "noidung" => "required"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));
            $req->input("comment") == $comment;
            $arr = preg_split("/\|/", $req->input("noidung"));
            $req->merge([
                "service_id" => 5,
                "comment" => $comment,
                "amount" => count($arr),
            ]);

            if ($req->input("amount") < 5 || $req->input("amount") > 10000) {
                return response()->json([
                    "success" => 0,
                    "message" => "số lượng tối thiểu là 5 đánh giá",
                ], 400);
            }

            return $this->orderController->FacebookBuff($req, "reviewpage", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLive(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer",
                "min" => "required|string"
            ]);

            if ($req->input("amount") < 50 || $req->input("amount") > 5000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 5000"
                ], 400);
            }

            $req->merge([
                "service_id" => 9
            ]);

            return $this->orderController->FacebookBuff($req, "live", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLive2(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer",
                "min" => "required|string"
            ]);

            if ($req->input("amount") < 50 || $req->input("amount") > 3000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 3000"
                ], 400);
            }

            $req->merge([
                "service_id" => 36
            ]);

            return $this->orderController->FacebookBuff($req, "live2", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function VipLike(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "package" => "required|string",
                "period" => "required|string",
                "posts" => "5",
                "service_id" => "15"
            ]);

            if ($req->input("package") % 50 != 0 || $req->input("package") > 2000 || $req->input("period") % 30 != 0) {
                return response()->json([
                    "success" => 0,
                    "message" => "Gói Like (package) bội của 50 và tối đa 2000, Ngày cài (period) bội của 30 và tối đa 360 ngày !"
                ], 400);
            }

            $req->merge([
                "service_id" => 15
            ]);

            return $this->orderController->FacebookVip($req, "like", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function VipComment(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "package" => "required|string",
                "period" => "required|string",
                "gender" => "required",
                //	"amount" => "required|string",
                "noidung" => "required",
                //	"note" => "required",
                "service_id" => "16"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));

            $price = $this->serviceModel->GetPrice("16", Auth::user()->role);
            $tongtien = $req->input("package") * $req->input("period") * ($price / 30);
            $total = Auth::user()->balance;


            if ($req->input("package") % 5 != 0 || $req->input("package") > 100 || $req->input("period") % 30 != 0) {
                return response()->json([
                    "success" => 0,
                    "message" => "Gói Comment (package) bội của 5 và tối đa 100, Ngày cài (period) bội của 30 và tối đa 360 ngày !"
                ], 400);
            }
            if ($total < $tongtien) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số dư không đủ",
                ], 400);
            }

            $req->merge([
                "service_id" => 16,
                "comment" => $comment,
            ]);

            return $this->orderController->FacebookVip($req, "comment", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffViewFacebook(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);

            if ($req->input("amount") < 500 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 500 đến 10000"
                ], 400);
            }
            $checkID = $this->orderController->CheckID($req);

            if (!$checkID->success) {
                return response()->json($checkID, 400);
            }
            $req->merge([
                "service_id" => 20
            ]);

            return $this->orderController->FacebookBuff($req, "view", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    //Instagram
    public function BuffSubInstagram(Request $req)
    {
        try {
            if ($req->input("amount") < 10 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 20 đến 10000"
                ], 400);
            }

            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:20"
            ]);

            $req->merge([
                "service_id" => 12
            ]);

            return $this->orderController->Instagram($req, "sub", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLikeInstagram(Request $req)
    {
        try {
            if ($req->input("amount") < 50 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 10000"
                ], 400);
            }

            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);

            $req->merge([
                "service_id" => 11
            ]);

            return $this->orderController->Instagram($req, "like", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffViewInstagram(Request $req)
    {
        try {
            if ($req->input("amount") < 500 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 500 đến 10000"
                ], 400);
            }

            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:500"
            ]);

            $req->merge([
                "service_id" => 20
            ]);

            return $this->orderController->Instagram($req, "view", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    //Tiktok
    public function BuffSubTikTok(Request $req)
    {
        try {
            if ($req->input("amount") < 50 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 10000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 14
            ]);

            return $this->orderController->TikTok($req, "sub", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLikeTikTok(Request $req)
    {
        try {
            if ($req->input("amount") < 50 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 10000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 13
            ]);

            return $this->orderController->TikTok($req, "like", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffViewTikTok(Request $req)
    {
        try {
            if ($req->input("amount") < 5000 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 5000 đến 1000000"
                ], 400);
            }
            // 		else if ($req->input("dichvu") != 24) {
            // 			return response()->json([
            // 				"success" => 0,
            // 				"message" => "Truyền sai dịch vụ"
            // 			], 400);
            // 		}
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 24
            ]);

            return $this->orderController->TikTok($req, "view", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffCommentTikTok(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "noidung" => "required"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));
            $req->input("comment") == $comment;
            $arr = preg_split("/\|/", $req->input("noidung"));
            $req->merge([
                "service_id" => 25,
                "comment" => $comment,
                "amount" => count($arr),
            ]);

            if ($req->input("amount") < 20 || $req->input("amount") > 1000) {
                return response()->json([
                    "success" => 0,
                    "message" => "số lượng tối thiểu là 20 comment",
                ], 400);
            }

            return $this->orderController->TikTok($req, "commenttiktok", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffShareTikTok(Request $req)
    {
        try {
            if ($req->input("amount") < 50 || $req->input("amount") > 10000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 10000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 26
            ]);

            return $this->orderController->TikTok($req, "share", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    //Youtube
    public function BuffSubYoutube(Request $req)
    {
        try {
            if ($req->input("amount") < 100 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 100 đến 10000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:100"
            ]);
            $req->merge([
                "service_id" => 27
            ]);

            return $this->orderController->Youtube($req, "sub", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffLikeYoutube(Request $req)
    {
        try {
            if ($req->input("amount") < 50 || $req->input("amount") > 100000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 50 đến 100000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 30
            ]);

            return $this->orderController->Youtube($req, "like", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffViewYoutube(Request $req)
    {
        try {
            if ($req->input("amount") < 500 || $req->input("amount") > 10000000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 500 đến 10000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer|min:50"
            ]);
            $req->merge([
                "service_id" => 29
            ]);

            return $this->orderController->Youtube($req, "view", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffCommentYoutube(Request $req)
    {
        try {
            $req->validate([
                "id" => "required|string|max:255",
                "noidung" => "required"
            ]);
            $comment = str_replace("|", "\n", $req->input("noidung"));
            $req->input("comment") == $comment;
            $arr = preg_split("/\|/", $req->input("noidung"));
            $req->merge([
                "service_id" => 32,
                "comment" => $comment,
                "amount" => count($arr),
            ]);

            if ($req->input("amount") < 20 || $req->input("amount") > 1000) {
                return response()->json([
                    "success" => 0,
                    "message" => "số lượng tối thiểu là 20 comment",
                ], 400);
            }

            return $this->orderController->Youtube($req, "commentytb", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function BuffGioXemYoutube(Request $req)
    {
        try {
            if ($req->input("amount") < 2000 || $req->input("amount") > 8000) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số lượng phải trong khoảng 2000 đến 8000"
                ], 400);
            }
            $req->validate([
                "id" => "required|string|max:255",
                "amount" => "required|integer"
            ]);
            $req->merge([
                "service_id" => 28
            ]);

            return $this->orderController->Youtube($req, "gioxem", true);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Bảo Trì Vui Lòng Thử Lại Sau Ít Phút...  !']);
        }
    }

    public function GetOrder(Request $req, $id)
    {
        $order = Order::find($id);

        if (!$order) {
            return response()->json([
                "success" => 0,
                "message" => "Đơn hàng có id = $id không tồn tại ! "
            ], 400);
        }

        if ($order->user_id != Auth::user()->id) {
            return response()->json([
                "success" => 0,
                "message" => "Bạn không có quyền truy cập đơn hàng này !"
            ], 403);
        }

        $result = [
            "order_id" => $order->id,
            "id" => $order->target,
            "status" => $order->status,
            "startcount" => $order->startcount,
            "quantity" => $order->quantity,
        ];

        return response()->json([
            "success" => 1,
            "order" => $result
        ], 200);
    }
}
