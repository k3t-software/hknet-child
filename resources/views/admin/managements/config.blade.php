@extends('admin.layouts.master')

@php
    $userRoles = ['<span class="badge badge-primary">Admin</span>', '<span class="badge badge-primary">Đại lý</span>', '<span class="badge badge-primary">Cộng Tác Viên</span>', '<span class="badge badge-primary">Khách</span>']
@endphp

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Cấu hình website</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Bổ sung</li>
            </ol>
        </div>
    </div>

    <div>
    </div>
    <style>
        .card.selected {
            opacity: 0.7;
        }
    </style>

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{{number_format($tokenInfo['balance'])}} đ</h2>
                                <h6>Số dư site chính</h6></div>
                            <div class="col-4 align-self-center text-right  p-l-0">
                                <div id="sparklinedash3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2 class="">{{$orderCount}}
                                    <i class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Đơn hàng</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{!! number_format($profit) !!} đ <i
                                            class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Lợi nhuận</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2> {{$userCount}} <i
                                            class="ti-angle-down font-14 text-danger"></i></h2>
                                <h6>Tổng thành viên</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">Thông tin cấu hình</h2>
                            @if(isset($tokenInfo['name']))
                                <ul class="list-group">
                                    <li class="list-group-item">Họ tên: <b>{{$tokenInfo['name']}}</b></li>
                                    <li class="list-group-item">Số dư: <b>{!!number_format($tokenInfo['balance']) !!}
                                            đ</b>
                                    </li>
                                    <li class="list-group-item">Chức vụ: <b>{!!$userRoles[$tokenInfo['role']] !!}</b>
                                    </li>
                                    <li class="list-group-item">Phone: <b>{{$tokenInfo['phone']}}</b></li>
                                    <li class="list-group-item">Email: <b>{{$tokenInfo['email']}}</b></li>
                                </ul>
                            @else
                                @if($tokenInfo['message'])
                                    <h3 class="text-warning">{{$tokenInfo['message']}}</h3>
                                @else
                                    <h3 class="text-warning">Xảy ra lỗi không xác định. Vui lòng liên hệ với quản trị
                                        viên</h3>
                                @endif
                            @endif
                        </div>
                </div>
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">Cấu hình website</h2>
                            <form id="rut_tien_vi_2" class="form_ajax" action="/config/save">
                                <div class="form-group">

                                    <div class="form-group">
                                        <label for="">Token <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="token"
                                               value="<?=$config->get('token')->value?>" required disabled="true">
                                    </div>

                                    <hr />

                                    <div class="form-group">
                                        <label for="">Tiêu đề <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="title"
                                               value="<?=$config->get('title')->value?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Logo <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="logo"
                                               value="<?=$config->get('logo')->value?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Favicon <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="favicon"
                                               value="<?=$config->get('favicon')->value?>" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="">ID Facebook <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="fanpage"
                                               value="<?=$config->get('fanpage')->value?>" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Tên Thương Hiệu <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="admin_name"
                                               value="<?=$config->get('admin_name')->value?>" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Địa chỉ liên hệ <span class="text-danger">(nếu có)</span>:</label>
                                        <input class="form-control" type="text" name="fb_admin"
                                               value="<?=$config->get('fb_admin')->value?>" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="">SĐT <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="sdt"
                                               value="<?=$config->get('sdt')->value?>" required>
                                    </div>


                                    <hr />

                                    <h3>Cấp bậc</h3>

                                    <div class="form-group">
                                        <label for="">Tên gọi cấp 1 <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="agency_name"
                                               value="<?=$config->get('agency_name')->value?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Tên gọi cấp 2 <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="collaborators_name"
                                               value="<?=$config->get('collaborators_name')->value?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Tên gọi cấp 3 <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="guest_name"
                                               value="<?=$config->get('guest_name')->value?>" required>
                                    </div>

                                    @php
                                        $defaultRole = $config->getValue('default_role');
                                    @endphp
                                    <div class="form-group">
                                        <label for="">Cấp mặc định khi tạo tài khoản: <span
                                                    class="text-danger">(*)</span>:</label>
                                        <select class="form-control" id="sel1" name="default_role">
                                            <option value="1" @if ($defaultRole === '1') selected @endif>Cấp 1</option>
                                            <option value="2" @if ($defaultRole === '1') selected @endif>Cấp 2</option>
                                            <option value="3" @if ($defaultRole === '3') selected @endif>Cấp 3</option>
                                        </select>
                                    </div>

                                    <div style="display: flex; justify-content: space-between;">
                                        <button type="submit" class="btn btn-primary">Lưu cấu hình</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">Thông tin ngân hàng</h2>
                            <form id="rut_tien_vi_2" class="form_ajax" action="/admin/config/bank/save">
                                <div class="form-group">

                                    <div class="form-group">
                                        <div class="form-group">

                                            <label>Ngân Hàng / Ví Điện Tử <span class="text-danger">(*)</span>:</label>
                                            <select class="form-control" name="bank">
                                                <option value="Vietcombank">Vietcombank</option>
                                                <option value="Momo">Momo</option>
                                                <option value="AGB">Agribank</option>
                                                <option value="BAB">Bac A Bank</option>
                                                <option value="BIDV">BIDV</option>
                                                <option value="DAB">DongA Bank</option>
                                                <option value="EXB">Eximbank</option>
                                                <option value="GPB">GPBank</option>
                                                <option value="HDB">HDBank</option>
                                                <option value="MSB">MariTimeBank</option>
                                                <option value="MB">Military JSC Bank (MBB)</option>
												<option value="MBBANK">Ngân hàng Quân đội(MBBANK)</option>
                                                <option value="NAB">Nam A Bank</option>
                                                <option value="NVB">Nam Viet Bank (NaviBank)</option>
                                                <option value="OJB">OceanBank</option>
                                                <option value="PGB">PGBank</option>
                                                <option value="STB">Sacombank</option>
                                                <option value="SGB">SaiGon Bank for Industry and Trade (Saigon Bank)
                                                </option>
                                                <option value="SHB">SHB</option>
                                                <option value="TCB">Techcombank</option>
                                                <option value="TPB">TienPhong Bank</option>
                                                <option value="VIB">VIB</option>
                                                <option value="VAB">VietA Bank</option>
                                                <option value="ACB">ACB</option>
                                                <option value="ICB">VietinBank</option>
                                                <option value="VPB">VPBank</option>
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Số Tài Khoản <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="stk"
                                               value="" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Chủ tài khoản: <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="ctk"
                                               value="" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Chi nhánh: <span class="text-danger">(*)</span>:</label>
                                        <input class="form-control" type="text" name="chi_nhanh"
                                               value="" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Link Ảnh QR (Nếu có):</label>
                                        <input class="form-control" type="text" name="image" target="_blank"
                                               value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Lưu ý:</label>
                                        <input class="form-control" type="text" name="note" target="_blank"
                                               value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Nội dung chuyển khoản : <span
                                                    class="text-danger">(*)</span>:</label>
                                        <textarea class="form-control" type="text" name="noidung"
                                                  value="" required></textarea>

                                        <div>
                                            <ul>
                                                <li><code>!!sdt!!</code> : Số điện thoại</li>
                                                <li><code>!!id!!</code> : ID</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div style="display: flex; justify-content: space-between;">
                                        <button type="submit" class="btn btn-primary">Thêm ngân hàng</button>
                                    </div>

                                </div>
                            </form>

                            <hr />

                            <ul class="list-group">
                                <div class="table-responsive">

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Ngân hàng</th>
                                            <th scope="col">STK</th>
                                            <th scope="col">CTK</th>
                                            <th scope="col">Chi nhánh</th>
                                            <th scope="col">Nội dung</th>
                                            <th scope="col">Lưu ý</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Hành động</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($banks as $index => $bank)
                                            @php
                                                $bankInfo = json_decode($bank->value, true);
                                                $configKey = 'bank_' . $bankInfo['bank']
                                            @endphp

                                            <tr>
                                                <th scope="row">{{$index + 1}}</th>
                                                <td>{{$bankInfo['bank']}}</td>
                                                <td>{{$bankInfo['stk']}}</td>
                                                <td>{{$bankInfo['ctk']}}</td>
                                                <td>{{$bankInfo['chi_nhanh']}}</td>
                                                <td>{{$bankInfo['noidung']}}</td>
                                                <td>{{$bankInfo['note']}}</td>
                                                <td><a href="{{$bankInfo['image']}}">
                                                        {{$bankInfo['image']}}
                                                    </a></td>
                                                <td>
                                                    <button class="btn btn-danger btn-xs"
                                                            onclick="deleteBank('{{$configKey}}')">Xóa
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </ul>


                        </div>


                </div>


                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">Giao diện</h2>


                            <h4 class="text-info">Chọn giao diện landing page</h4>

                            @php

                                $landings = array(
                                [
                                      "id" => null,
                                      "name" => "Không dùng",
                                      "preview" => "https://i.imgur.com/FkTLXb0.png"
                                ],
                                [
                                        "id" => "FlexStart",
                                        "name" => "FlexStart",
                                        "preview" => "https://i.imgur.com/sjNflu0.png"
                                ],
                                [
                                      "id" => "SeoDream",
                                      "name" => "SEO Dream",
                                      "preview" => "https://i.imgur.com/8p6KS3Y.png"
                                ],
                                 [
                                      "id" => "DigiMedia",
                                      "name" => "Digi Media",
                                      "preview" => "https://i.imgur.com/wC0OvKh.png"
                                ]
                                );

                                $logins = array(
                                [
                                        "id" => "default",
                                        "name" => "Mặc định",
                                        "preview" => "https://i.imgur.com/f7j3DrN.png"
                                ],
                                [
                                      "id" => "RedLogin",
                                      "name" => "Red Tone",
                                      "preview" => "https://i.imgur.com/425AKYD.png"
                                ]
                                );

                                $currentLanding = $config->get('landing_theme');
                                $currentLogin = $config->get('login_theme')
                            @endphp

                            <div class="row">
                                @foreach($landings as $landing)
                                    @php
                                        $isSelected = $currentLanding->value === $landing['id']
                                    @endphp
                                    <div class="col-md-3">
                                        <div class="card {!! $isSelected ? 'selected' : '' !!}">
                                            <img class="card-img-top" src="{{$landing['preview']}}" alt="Card image cap"
                                                 style="height: 200px;">
                                            <div class="card-body text-center">
                                                <h4>{{$landing['name']}}</h4>
                                                @if(!$isSelected)
                                                    <button class="btn btn-info"
                                                            onclick="selectTheme('{{$landing['id']}}', 'LANDING_PAGE')">
                                                        Chọn
                                                    </button>
                                                @else
                                                    <button class="btn btn-default">
                                                        Đã chọn
                                                    </button>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <h4 class="text-info">Chọn giao diện login</h4>

                            <div class="row">
                                @foreach($logins as $login):
                                @php
                                    $isSelected = $currentLogin->value === $login['id']
                                @endphp
                                <div class="col-md-3">
                                    <div class="card {!! $isSelected ? 'selected' : '' !!}">
                                        <img class="card-img-top" src="{{$login['preview']}}" alt="Card image cap">
                                        <div class="card-body text-center">
                                            <h4>{{$login['name']}}</h4>
                                            @if(!$isSelected)
                                                <button class="btn btn-info"
                                                        onclick="selectTheme('{{$login['id']}}', 'LOGIN_PAGE')">
                                                    Chọn
                                                </button>
                                            @else
                                                <button class="btn btn-default">
                                                    Đã chọn
                                                </button>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>

            </div>

        </div>
        @endsection

        @section('scripts')
            <script src="{{asset("admin/js/managements/config.js")}}"></script>
@endsection
