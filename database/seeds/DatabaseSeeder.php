<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\City;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => "Admin",
            'phone' => "123",
            'role' => "0",
            'facebookId' => "100005675358150",
            'password' => Hash::make('123'),
        ]);
    }
}
