<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FlashSale extends Model
{
    protected $table = "flash_sale_logs";
    protected $fillable = ['target_id', 'user_id', 'ip', 'feature'];


    private const FREE_PER_DAY = 1;
    private const TIME_LIMIT = 30; // m

    public static function insertLogs($targetId, $feature, $ip)
    {
        $user = Auth::user();
        return self::create([
            "target_id" => $targetId,
            "user_id" => $user->id,
            "ip" => $ip,
            "feature" => $feature
        ]);
    }

    public static function checkLogs($targetId, $feature, $ip)
    {

        $user = Auth::user();

        $day = 24 * 60 * 60;
        // Id chỉ được thêm một lần.
        $isSameId = self::where('target_id', $targetId)->where('feature', $feature)->count() >= 1;

        // Only add 2 ids per day
        $today = date('Y/m/d H:i:s', time());
        $yesterday = date('Y/m/d H:i:s', time() - $day);

        $isMaxPerDay = self::where('user_id', $user->id)->where('feature',
                $feature)->whereBetween('created_at',
                [$yesterday, $today])->count() >= 1;

        return (object)[
            "success" => !($isMaxPerDay || $isSameId),
            "message" => "Mỗi ngày 1 lượt sử dụng miễn phí và mỗi id/link chạy đc 1 lần, để sử dụng không giới hạn vui lòng nạp tiền sử dụng dịch vụ !"
        ];
    }

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

}
