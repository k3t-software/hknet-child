$(function () {
    Accounting();

    $("#getProfileBtn").on("click", function () {
        const profile = $("#profileLink").val();
        if (!profile) return;
        const button = $(this);
        button.attr("disabled", true);
        button.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

        $.ajax({
            url: `/admin/helper/GetProfile?profile=${profile}`,
            method: "GET",
            dataType: "json",
        }).always(function (data) {
            if (data.status == 1) {
                $(".profile-info [name=id]").val(data.items.id);
                $(".profile-info [name=name]").val(data.items.name);
                button.attr("disabled", false);
                button.html("Lấy thông tin");
            } else {
                button.attr("disabled", false);
                button.html("Lấy thông tin");
                swal(
                    {
                        title: "Failed!",
                        text: `Đã xảy ra lỗi ! Vui lòng thử lại !`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            }
        });
    });

    $(".order-info [name=package]").on("change", function () {
        Accounting();
    });

    $(".order-info [name=period]").on("change", function () {
        Accounting();
    });
    $(".order-info [name=posts]").on("change", function () {
        Accounting();
    });
    $("#orderBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const buffType = button.data("function");
        const backupBtn = button.html();
        const loadingBtn =
            '<i class="fa fa-spin fa-circle-o-notch"></i> Thực hiện';
        button.html(loadingBtn);

        let data = {};

        const form = $("#infoForm");

        let formData = form.serializeArray();
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        data.service_id = button.data("service");

        data.id = $(".profile-info [name=id]").val();

        $.ajax({
            url: `/admin/dichvu/facebook-vip/${buffType}`,
            method: "POST",
            dataType: "json",
            data,
        }).always(function (data) {
            if (data) {
                if (data.success == 1) {
                    swal(
                        {
                            title: "Thành công!",
                            text: `Thêm yêu cầu thành công !`,
                            type: "success",
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                } else {
                    button.attr("disabled", false);
                    button.html(backupBtn);
                    swal(
                        {
                            title: "Thất bại!",
                            text: data.message,
                            type: "error",
                        },
                        function () {
                            button.attr("disabled", false);
                        }
                    );
                }
                button.html(backupBtn);
            }
        });
    });

    function Accounting() {
        const price = $(".order-info [name=price]").val();
        const package = $(".order-info [name=package]").val();
        const period = $(".order-info [name=period]").val();
        const posts = $(".order-info [name=posts]").val();
        const total =  (price/30) * package * period;
        $(".order-info [name=total]").val(total);
    }
});