<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IpLog extends Model
{
    protected $table = "iplogs";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public function getIpCount($ip)
    {
        return self::where('ip', $ip)->count();
    }
}
