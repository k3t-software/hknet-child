<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Http;

use MessageBird;

class PaymentController extends Controller
{

    public $configModel;

    public function __construct()
    {
        $this->configModel = new Config;
    }

    public function index()
    {
        $data['user'] = Auth::user();
        $data['captcha'] = captcha_img();
        $data['config'] = $this->configModel;
        $data['banks'] = $this->configModel->getLike('bank_');
        return view("admin.features.payment", $data);
    }

    public function check()
    {
        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Captcha không chính xác'
            ]);
        }

        $this->sendVoice("Có khách nạp tiền");

        return response()->json([
            'success' => true,
            'message' => 'Đã gửi yêu cầu đến nhân viên ! Vui lòng đợi 2-5p để tiền vào tài khoản !'
        ]);
    }


    public function sendVoice($message)
    {
        $client = new MessageBird\Client(env('MB_KEY'));

        $call = new MessageBird\Objects\Voice\Call;
        // $call->source = $row->sim->number;
        $call->source = str_replace('.', '', '84777799999');
        $call->destination = '84812888896'; // Số nhận cuộc gọi

        $flow = new MessageBird\Objects\Voice\CallFlow;
        $flow->title = 'Đăng Ký Thành Công';

        $step = new MessageBird\Objects\Voice\Step;
        $step->action = 'say';
        $step->options = [
            // 'payload' => 'Mã xác nhận của quý khách là . 5 . 3 . 8 . 8 . 9 . xin nhắc lại, 5 . 3 . 8 . 8 . 9', // nội dung cuộc gọi thoại
            'payload' => $message,
            'language' => 'vi-VN', // ngôn ngữ
            'voice' => 'male'
        ];
        $flow->steps = [$step];
        $call->callFlow = $flow;
        return $client->voiceCalls->create($call);
    }
}
