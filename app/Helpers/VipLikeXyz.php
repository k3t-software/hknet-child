<?php

namespace App\Helpers;

use HTTP_Request2;
use HTTP_Request2_Exception;

class VipLikeXyz
{
    private $baseURL = 'https://viplike.xyz';
    public $cookie;
    public $partnerModel;
    public $account;

    public function __construct()
    {
        $this->getPageInfo();
    }

    public function getPageInfo($url = "/")
    {
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $url);
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setConfig(array(
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));

        /* Cookie */
        if (isset($this->cookie)) {
            $request->setHeader(array(
                'Cookie' => 'PHPSESSID=' . $this->cookie
            ));
        }
        $response = $request->send();
        $cookies = $response->getCookies();
        foreach ($cookies as $cookie) {
            if ($cookie['name'] === "PHPSESSID") {
                $this->cookie = $cookie['value'];
            }
        }

        /* Form Token */
        $regex = '/"form_token" value="(.*?)"/m';
        preg_match_all($regex, $response->getBody(), $matches, PREG_SET_ORDER, 0);
        $formToken = null;
        if ($matches && isset($matches[0][1])) {
            $formToken = $matches[0][1];
        }

        return [
            "cookie" => $this->cookie,
            "formToken" => $formToken
        ];
    }

    public function login($type = 'normal')
    {
        $username = "hknet";
        $password = "dhkytv0812";
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . "/?action=login");
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cookie' => 'PHPSESSID=' . $this->cookie
        ));
        $request->setConfig(array(
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));

        $request->addPostParameter(array(
            'username' => $username,
            'passwd' => $password
        ));
        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {

                $isWrongPassword = str_contains($response->getBody(), 'Sai tên tài khoản hoặc mật khẩu');
                if ($isWrongPassword) {
                    return false;
                }
                $cookies = $response->getCookies();
                foreach ($cookies as $cookie) {
                    if ($cookie['name'] === "PHPSESSID") {
                        $this->cookie = $cookie['value'];
                    }
                }
                return true;
            } else {
                echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                    $response->getReasonPhrase();
                return false;
            }
        } catch (HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return false;
        }
    }

    private function post($url, $data)
    {
        if (!isset($this->cookie)) {
            return (object)[
                'success' => 0,
                'message' => 'Đã xảy ra lỗi hệ thống !! [068]'
            ];
        }

        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $url);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cookie' => 'PHPSESSID=' . $this->cookie,
            'user-agent' => "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Mobile/15E148 Safari/604.1"
        ));
        $request->setConfig(array(
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));
        $request->addPostParameter($data);

        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {
                $isSuccess = false;
                $message = "Lỗi ko xác định";

                $re = '/<script> modal\(\'Thông báo\',"(.*)"\)<\/script>/m';
                preg_match_all($re, $response->getBody(), $matches, PREG_SET_ORDER, 0);
                if (isset($matches) && isset($matches[0][1])) {
                    $message = $matches[0][1];
                    $isSuccess = str_contains($message, 'thành công');
                } else {
                    $isSuccess = false;
                }

                return (object)[
                    'success' => $isSuccess ? 1 : 0,
                    'message' => $message
                ];
            } else {
                return (object)['success' => 0, 'message' => "Xảy ra lỗi khi thêm vào hệ thống [016]"];
            }
        } catch (HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return (object)['success' => 0, 'message' => 'Unknown error: ' . $e->getMessage()];
        }
    }


    public function buyComment($idPost, $amount, $delay, $commentStr)
    {

        $pageInfo = $this->getPageInfo('/');

        $comments = explode("\n", $commentStr);
        $isDuplicateComment = count($comments) > count(array_unique($comments));
        if ($isDuplicateComment) {
            return (object)[
                'success' => 0,
                'message' => "Nội dung comment ko được trùng."
            ];
        }

        return $this->post('/', array(
            'action' => "order_cmt",
            'form_token' => $pageInfo['formToken'],
            "id" => $idPost,
            'limit_cmt' => (int)$amount,
            "delay" => (int)$delay,
            'content_cmt' => $commentStr,
            'target_gender' => 'all',
            'target_avt' => 1,
            'submit' => true
        ));
    }
}
