<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
                <meta property="og:image" content="https://hknet.vn/admin/images/TIGVIET.png" />
                <meta property="og:description" content="Ưu đãi lên tới 1.500.000đ - Hỗ trợ data KH - Target nhiều tiêu chí" />
                <meta property="og:title" content="HKNet - Truyền Thông Mạng Xã Hội" />
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>CÔNG TY TRUYỀN THÔNG HKNET</title>
    <link rel="shortcut icon" href="https://hknet.vn/ico.ico">
    <meta property="fb:admins" content="100004185550959" />
    <meta name="description" />
    <meta name="keywords" />
    <meta name="author" />
    <meta property="og:title" content="Dịch vụ Facebook, Google, Instagram, Youtube, Tiktok Tư vấn hỗ trợ giải phải tiếp cận khách hàng" />
    <meta property="og:description" />
    <meta property="og:image" content="" />
    <meta property="og:image:secure_url" content="" />
    <meta property="og:image:width" content="" />
    <meta property="og:image:height" content="" />
    <meta property="og:type" content="Website" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta name="twitter:card" content="" />
    <meta name="twitter:title" content="Cung cấp dịch vụ trên nền tảng các mạng xã hội Facebook, Instagram, Youtube,... " />
    <meta name="twitter:description" />
    <meta name="twitter:image" content="" />
    <meta name="DC.language" content="scheme=utf-8 content=vi" />
    <link href="https://hknet.vn/" rel="canonical" />

    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/css/smooth-scroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery.lazy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/Themes/js/isotope.min.js') }}"></script>

    

    <link media="screen" rel="stylesheet" type="text/css" href="{{ asset('/template2/Content/1/v2/css/bootstrap.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/h.css?loadrsss') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/n.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/r.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/index.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/owl.carousel.css') }}" />
        <link media="screen" rel="stylesheet" type="text/css" href="https://hknet.vn/template2/Content/1/v2/css/cubeportfolieo.min.css" />



    <style>


        .client__item > a.btn-background {
            height: auto;
            border: none;
        }

        .bloghome__title {
            text-overflow: ellipsis;
            overflow: hidden;
            height: 1.2em;
            white-space: nowrap;
        }

        .client__body > p.btn-background {
            float: right;
        }

        .list-flex li {
            display: flex;
        }

        @media only screen and (max-width: 2400px) and (min-width: 1600px) {
            .footer:before {
                background-color: #F2F7FF;
                top: -332px;
            }
        }
    </style>




</head>

<body>


    





<section class="head">
    <div class="head__one">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head__one--inner">
                        <div class="head__one--left">
                            <div class="head__one--phone"><a href="tel:0812888896">HOTLINE: 0812 8888 96</a></div>
                        </div>
                        <div class="head__one--right">
                            <div class="head__one--social">
                                <a href="https://facebook.com/hknet"><i class="fa fa-facebook"></i></a>
                                <a href="https://plus.google.com/sales.hknet"><i class="fa fa-google-plus"></i></a>
                                <a href="https://www.youtube.com/channel/UCULxe8wylG3jYCnbjO3g73A"><i class="fa fa-youtube"></i></a>
                            </div>
                            <div class="head__one--language">
                                <div class="head__one--language--selected" id="currentLanguage">Langue</div>
                                <div class="head__one--language--list">
                                    <a onclick="changeLanguage('vi')">Việt Nam</a>
                                    <a onclick="changeLanguage('en')">English</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="head__two">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head__two--inner">
                        <div class="head__two--logo">
                            <h1 title="">
                                <a href="/">
                                    <img src="{{ asset('https://hknet.vn/admin/images/TIGVIET.png') }}" align="logo">
                                </a>
                            </h1>
                        </div>
                        <div class="head__two--nav">
                            <ul>
                                <li><a class="scroll" href="#dichvu">Dịch Vụ</a></li>
                                <li><a href="/chinh-sach-gia">Chính sách giá</a></li>
                                <li><a href="/api/docs">API</a></li>
                                	<!-- <li class="parent">
                                    <a href="/huong-dan">Dự Án</a>
                                    <ul>
                                        <li><a href="/huong-dan/huong-dan-su-dung" style="text-decoration:none;">Dịch Vụ Facebook & Instagram</a></li>
                                        <li><a href="/huong-dan/huong-dan-tich-hop" style="text-decoration:none;">sss</a></li>
                                        <li>
                                            <a href="/huong-dan/thu-thuat-marketing" style="text-decoration: none;">th&#7911; thu&#7853;t marketing</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li><a href="/dai-ly">Đại lý</a></li>
                                <li><a href="#contact">Liên hệ</a></li>
                            </ul>
                        </div>
                        <div class="head__two--user">
                            <a id="aSignin" class="btn-outline" href="/login">Đăng Nhập</a>
                            <a id="aSignup" class="btn-background" href="/register">Đăng Ký</a>
                            <div class="btn-group" role="group" id="divAccount" style="display: none;">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                        style="border: 1px solid #7eb142;color: #7eb142;border-radius: 23px;">
                                    <i class="fa fa-user-circle"></i> <span id="spanDisplayName"></span>
                                    <span class="caret"></span>
                                </button>
                                
                            </div>
                        </div>
                        <div class="head__two--nav-mobile">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" id="btn-menu-mobile">
                            <div class="sidebar-nav navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            
                        </div>

                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var currentLang = getCookie('lang');
    if (currentLang) {
        switch (currentLang) {
            case 'vi':
                document.getElementById('currentLanguage').innerHTML = "Việt Nam"; break;
            case 'en':
                document.getElementById('currentLanguage').innerHTML = "English"; break;
            default:
                break;
        }
    }
</script>

    
<section class="why-choose">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="headline__intro" style="margin-top: 100px;">
<h2 class="headline__intro--title">Phòng Trọ Bình Chánh</h2>
<div class="headline__intro--sub">Phòng Rộng, Thoáng Ma Giá Tốt
</div>
</div>
<div class="price__inner">
<div class="why-choose__left">
<figure class="lazy why-choose__image--large" style="display: block; background-image: url(https://cdn.chotot.com/CUnFFIuv1UoXARYqtSMEAES5QpZ2Wdmoz-Y3BzlrKb0/preset:view/plain/01ea4445f3bbce0fee5904c938504477-2720739227792114106.jpg);"></figure>
</div>
<div class="why-choose__left">
<figure class="lazy why-choose__image--large" style="display: block; background-image: url(https://cdn.chotot.com/EdYtM1cQ8quyCcm4zhsyOWH0_B32y3re3-fXpd9BLPU/preset:view/plain/dc833ca04be3062c13ba3f0f7bb6a96c-2720739244149816826.jpg);"></figure>
</div>
<div class="why-choose__left">
<figure class="lazy why-choose__image--large" style="display: block; background-image: url(https://cdn.chotot.com/Sx3FNOx0NuntNIBzVbpHCXf0-2F7wn5I_ylTvdxlwdU/preset:view/plain/5efd7e5820014ef35c8c863988ade7a9-2720739243706903716.jpg);"></figure>
</div>
</div>
</div>
</div>
</div>
<center>
<h4><font color="red"><b>Liên Hệ</b></font></h4>
<h4>Phone / Zalo: 0812 8888 96</h4>
<h4>Email: sales.hknet@gmail.com</h4>
<p><b>━Hoặc━</b></p>
<div class="head__two--user">
<a id="aSignin" class="btn-background" href="/login">Đăng Nhập Sử Dụng</a>
<div class="btn-group" role="group" id="divAccount" style="display: none;">
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border: 1px solid #7eb142;color: #7eb142;border-radius: 23px;">
<i class="fa fa-user-circle"></i> <span id="spanDisplayName"></span>
<span class="caret"></span>
</button>
</div>
</div>
</center>
</section>
    
    
<section class="contact contact_bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="headline__intro">
                        <h2 class="headline__intro--title">Tư vấn</h2>
                        <div class="headline__intro--sub">Vì đặc thù mỗi tính năng và nhóm đối tượng khác nhau, bạn hãy liên hệ với chúng tôi để có giá tốt nhất.</div>
                    </div>
                    <div class="contact__inner">
                        <div class="contact__left">
                            <figure class="contact__image lazy" style="display: block; background-image: url(https://esms.vn/Content/1/v2/img/contact-image@2x.png);"></figure>
                        </div>
                        <div class="contact__right">
                            <div class="contact__right--inner">
                                <h3 class="contact__right--headline">ĐĂNG KÝ NHẬN TƯ VẤN MIỄN PHÍ</h3>
                                
<form class="contact__from-right" data-testid="form-contact-chinh-sach-gia" action="save.php" method="post" onsubmit="" id="u_0_a">
        <div class="field-wrp">
            <div class="form-group">
                <input class="form-control" data-label="Name" required="" data-msg="Please enter name." type="text" name="name_u" id="name_u-chinh-sach-gia" placeholder="Nhập tên của bạn *">
                <label id="name_err" style="color:red" class="hidden">(*) Vui lòng nhập tên</label>
            </div>
            <div class="form-group">
                <input class="form-control" data-label="Email" required="" data-msg="Please enter email." type="email" name="email_u" id="email_u-chinh-sach-gia" placeholder="Nhập email của bạn *">
                <label id="email_err" style="color:red" class="hidden">(*) Vui lòng nhập email</label>
            </div>
            <div class="form-group">
                <input class="form-control" data-label="Phone" required="" data-msg="Please phone number." type="text" name="phone_u" id="phone_u-chinh-sach-gia" placeholder="Nhập số điện thoại của bạn *">
                <label id="phone_err" style="color:red" class="hidden">(*) Vui lòng nhập số điện thoại</label>
            </div>
            <div class="form-group">
                <textarea class="form-control" data-label="Message" name="note_u" id="note_u-chinh-sach-gia" placeholder="Mô tả thêm về dịch vụ muốn tư vấn" cols="30" rows="1"></textarea>
            </div>
        </div>
        <br>
        <div class="form-group">
            <button type="submit" id="btn_SendContact-chinh-sach-gia">Gửi</button>
        </div>
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<section class="why-choose">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="headline__intro">
                            <h2 class="headline__intro--title">TẠI SAO CHỌN <strong>HKNET ?</strong></h2>
                            <div class="headline__intro--sub">Định hướng là một doanh nghiệp quốc tế với các dịch vụ Mạng xã hội B2B, HKNET luôn xây dựng chất lượng dịch vụ khách hàng thật tốt nhằm đáp ứng các yêu cầu dù là cao nhất.</div>
                        </div>
                        <div class="why-choose__inner">
                            <div class="why-choose__left">
                                <figure class="lazy why-choose__image--large" style="display: block; background-image: url(admin/images//why-chosse1@2x.png);"></figure>
                            </div>
                            <div class="why-choose__right">
                                <div class="why-choose__list">
                                    <div class="why-choose__item">
                                        <figure class="lazy why-choose__image" style="display: block; background-image: url(admin/images/why-chosse-icon1@2x.png);"></figure>
                                        <h3>Chăm sóc khách hàng và hỗ trợ kỹ thuật 24/7.</h3>
                                    </div>
                                    <div class="why-choose__item">
                                        <figure class="lazy why-choose__image" style="display: block; background-image: url(admin/images/why-chosse2-icon2@2x.png);"></figure>
                                        <h3>Hệ thống Online nền tảng website, tự động hóa 100% không cần cài đặt.</h3>
                                    </div>
                                    <div class="why-choose__item">
                                        <figure class="lazy why-choose__image" style="display: block; background-image: url(admin/images/why-chosse-icon3@2x.png);"></figure>
                                        <h3>Tài nguyên dồi dào và được chọn lọc chất lượng để phục vụ quý khách</h3>
                                    </div>
                                    <div class="why-choose__item">
                                        <figure class="lazy why-choose__image" style="display: block; background-image: url(admin/images/why-chosse-icon4@2x.png);"></figure>
                                        <h3>Hỗ nâng chiết khấu khi trở thành đối tác kinh doanh dịch vụ</h3>
                                    </div>
                                    <div class="why-choose__item">
                                        <figure class="lazy why-choose__image" style="display: block; background-image: url(admin/images/why-chosse-icon5@2x.png);"></figure>
                                        <h3> HKNet là nhà bán hàng đầu trong lĩnh vực dịch vụ tiếp thị truyền thông xã hội</h3>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        


    <!-- Footer Columns region -->
    <section id="contact" class="center footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer__contact">
                        <div class="footer__logo2">
                            <img width="105" height="105" src="https://hknet.vn/ico.png" align="Logo">
                        </div>
                        <div class="footer__address">
                            <div class="footer__address--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_address@2x.png"></figure>
                            </div>
                            <div class="footer__address--right">
                                <div class="footer__address--item">CÔNG TY TNHH CÔNG NGHỆ VÀ THƯƠNG MẠI QUỐC TẾ HK NET</div>
                                <div class="footer__address--item"> 113 Trần Văn Dư, Bắc Mỹ An, Ngũ Hành Sơn, Đà Nẵng 550000</div>
                               <a href="https://goo.gl/maps/Sveinox6eUnvQuL78" target="_blank" class="map-link">
									<i class="fa fa-map-marker"></i>
									<span> Open in Google Maps</span>
								</a>
                            </div>
                        </div>
                        <div class="footer__phone">
                            <div class="footer__phone--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_phone@2x.png"></figure>
                            </div>
                            <div class="footer__phone--right">
                                <div class="footer__address--item">SDT: <a href="tel:0812888896">0812.8888.96</a></div>
                                <div class="footer__address--item">Mail: <a href="mail:Support@hknet.vn">Support@hknet.vn</a></div>
                            </div>
                        </div>
                        <div class="footer__site">
                            <div class="footer__site--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/icon-www@2x.png"></figure>
                            </div>
                            <div class="footer__site--right">
                <div data-id="27d1016" class="elementor-element elementor-element-27d1016 elementor-column elementor-col-25 elementor-top-column" data-element_type="column"><div class="elementor-column-wrap  elementor-element-populated"><div class="elementor-widget-wrap"><div data-id="3648821" class="elementor-element elementor-element-3648821 elementor-widget elementor-widget-heading" data-element_type="heading.default"><div class="elementor-widget-container"><h4 class="elementor-heading-title elementor-size-default">Kết nối chúng tôi</h4></div></div><div data-id="0318d71" class="elementor-element elementor-element-0318d71 elementor-widget elementor-widget-facebook-page" data-element_type="facebook-page.default"><div class="elementor-widget-container"><div class="elementor-facebook-widget fb-page fb_iframe_widget" data-href="https://www.facebook.com/hknet/" data-tabs="" data-height="100px" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-hide-cta="false" style="min-height: 1px;height:200px" data-width="290px" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=290&amp;height=100&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;width=290px"><span style="vertical-align: bottom; width: 290px; height: 180px;"><iframe name="f195248541b8c18" height="200px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.10/plugins/page.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dff709b40f7446%26domain%3Dvaway.vn%26origin%3Dhttps%253A%252F%252Fvaway.vn%252Ff3c2f0cb48b2298%26relation%3Dparent.parent&amp;container_width=290&amp;height=200&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;width=290px" style="border: none; visibility: visible; width: 290px; height: 180px;" class=""></iframe></span></div></div></div></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </section>



    



    

    <script type="text/javascript" src="{{ asset('/template2/Scripts/CustomValidate/eSMS.Uitl.js') }}" id="eSMSUtil') }}"></script>
    
        <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
            n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1273287776096843');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=1273287776096843&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- /JavaScripts -->
    <!-- Google Tag Manager (noscripts) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNMZ896" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="alert alert-success" id="divAlert" style="position: fixed;z-index: 9999999;top: 100px;right: 0px; display:none;">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong id="alertTitle">Thất Bại</strong>
        <hr class="message-inner-separator" />
        <p id="alertContent">
            <strong>Success! </strong> Product have added to your wishlist.
        </p>
    </div>

    <script type="text/javascript">
        $(function () {
        try {
                window.eSMSSDK.Util.init(1, function (s) { console.log(s); })
            } catch (e) {
                console.log(e);
            }
        });

        function showAlert(id) {
            $("#" + id).fadeTo(2000, 500).slideUp(1000, function () {
                $("#" + id).slideUp(1000);
            });
        }

        function ShowNotifyAlert(message, typealert) {
            var popup = $("#divAlert");
            var title = $("#alertTitle");
            var content = $("#alertContent");

            if (typealert === 'info') {
                popup.removeClass('alert-danger');
                popup.addClass('alert-success');
                title.html('Thành công');
                content.html(message);
                showAlert('divAlert');
                return;
            }

            if (typealert === 'error') {
                popup.removeClass('alert-success');
                popup.addClass('alert-danger');
                title.html('Thất bại');
                content.html(message);
                showAlert('divAlert');
                return;
            }
        }
    </script>
    <script type="text/javascript" src="{{ asset('/template2/Scripts/feedback.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/a.js') }}"></script>


    
        <!-- Messenger Plugin chat Code -->
    <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v10.0'
          });
        };

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>

      <!-- Your Plugin chat code -->
      <div class="fb-customerchat"
        attribution="page_inbox"
        page_id="914109848730081">
      </div>
    

</body>
</html>