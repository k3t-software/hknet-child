@php
    $role = Auth::user()->role;
    $title = App\Config::getValueStatic('title');
    $logo = App\Config::getValueStatic('logo');
    $favicon = App\Config::getValueStatic('favicon');

    $order = new App\Order;
    $refundOrderCount = $order->where('notify', 'like', '%Đợi hoàn tiền%')->count();

    $manualOrderRemain = App\Order::GetRemainingOrders(App\Constant::TYPE_SERVICE_MANUAL);
    $apiOrderRemain = App\Order::GetRemainingOrders(App\Constant::TYPE_SERVICE_API);
    $serviceMap = App\Service::GetPriceMap();

    $configs = new App\Config;
    $userRoles[0] = '<span class="badge badge-primary">Admin</span>';
    $userRoles[1] = "<span class='badge badge-primary'>{$configs->getValue('agency_name')}</span>";
    $userRoles[2] = "<span class='badge badge-primary'>{$configs->getValue('collaborators_name')}</span>";
    $userRoles[3] = "<span class='badge badge-primary'>{$configs->getValue('guest_name')}</span>";
@endphp
        <!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{$favicon}}" />

    <title>{{$title}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset("admin/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" />
@yield('styles')
<!-- Custom CSS -->
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet" />
    <link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
    <!-- You can change the theme colors from here -->
    <link href="{{asset("admin/css/colors/blue.css?s")}}" id="theme" rel="stylesheet" />
    <script src="{{asset("admin/plugins/jquery/jquery.min.js")}}"></script>

    <style>
        .sidebar-nav > ul > li > a.active {
            background: white !important;
        }

        .fix-sidebar .left-sidebar {
            position: absolute;
        }

        .nav-main-link-icon-custom {
            max-width: .9rem;
            margin-right: .625rem;
        }
    </style>

</head>

<body class="fix-header fix-sidebar card-no-border">

<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewbox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar hidden-md-up" style="">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                                            href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
                    <li class="nav-item m-l-10"><a
                                class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a></li>


                </ul>

            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar pt-0">
        <!-- Sidebar scrolls-->
        <div class="scroll-sidebars">
            <div class="navbar-header text-center">
                <a class="navbar-brand p-2" style="width: 195px; height: auto; background: transparent;"
                   href="/admin/dashboard">
                    <img src="{{$logo}}" class="img-fluid" alt="">
                </a>
            </div>
            <!-- User profile -->
            <div class="user-profile">
                <!-- User profile image https://graph.facebook.com/v8.0/4/picture-->
                <div class="profile-img"><img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSTVqR_sX8kPg_OJbYwRF290KoNhL56GJbNbw&usqp=CAU"
                            alt="user" />
                    <!-- this is blinking heartbit-->
                    <div class="notify setpos"><span class="heartbit"></span> <span class="point"></span></div>
                </div>
                <!-- User profile text-->
                <div class="profile-text">
                    <h5>{{Auth::user()->name}} <br>{!!$userRoles[Auth::user()->role]!!}</h5>
                    <h6 class="text-muted">Số dư: <span
                                class="text-success">{{number_format(Auth::user()->balance)}} đ</span></h6>

                    <a href="#" class="dropdown-toggle u-dropdown" role="button" data-toggle="modal"
                       data-target="#changePasswordModal" aria-haspopup="true" aria-expanded="true"><i
                                class="mdi mdi-key"></i></a>
                    <a href="/logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                </div>
            </div>
            <!-- End User profile text-->
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">

                    <li class="nav-devider"></li>

                    <li class="nav-small-cap">Quản lý</li>

                    <li><a class="waves-effect waves-dark" href="/admin/dashboard" aria-expanded="false"><i
                                    class="fa fa-dashboard"></i><span class="hide-menu">Dashboard</span></a>
                    <li><a class="waves-effect waves-dark" href="/admin/payments" aria-expanded="false"><i
                                    class="fa fa-money"></i><span class="hide-menu">Nạp tiền</span></a>
                    <li><a class="waves-effect waves-dark" href="/admin/transactions" aria-expanded="false"><i
                                    class="fa fa-credit-card"></i><span class="hide-menu">Lịch Sử Giao dịch</span></a>

                    <li><a class="waves-effect waves-dark" href="/admin/chat" aria-expanded="false"><i
                                    class="fa fa-envelope-o"></i><span class="hide-menu">Hỗ Trợ Trực Tuyến</span>
                            @php
                                $unReadMessage = \App\Chat::getUnreadMessage();
                                if(isset($unReadMessage) && count($unReadMessage) > 0) {
                                   echo '<span class="badge badge-primary">'.count($unReadMessage).'</span>';
                                }
                            @endphp
                        </a>
                    </li>


                    @if (Auth::user()->role == 0)
                        <li class="nav-devider"></li>

                        <li class="nav-small-cap">Quản trị viên</li>

                        <li><a class="waves-effect waves-dark" href="/admin/users" aria-expanded="false"><i
                                        class="fa fa-user-circle"></i><span class="hide-menu">Users</span></a>

                        </li>


                        <li><a class="waves-effect waves-dark" href="/admin/orders/api?limit=300" aria-expanded="false"><i
                                        class="fa fa-calendar"></i><span class="hide-menu">Hoàn tiền<span
                                            class="label label-rouded label-success pull-right">{{$refundOrderCount}}</span></span></a>
                        </li>

                        <li><a class="waves-effect waves-dark" href="/admin/admin-transactions" aria-expanded="false"><i
                                        class="fa fa-puzzle-piece"></i><span class="hide-menu">Thống kê hệ thống</span></a>
                        </li>

                        <li><a class="waves-effect waves-dark" href="/admin/services" aria-expanded="false"><i
                                        class="fa fa-puzzle-piece"></i><span class="hide-menu">Dịch vụ</span></a>
                        </li>

                        <li><a class="waves-effect waves-dark" href="/admin/setting" aria-expanded="false"><i
                                        class="fa fa-moon-o"></i><span class="hide-menu">Nâng cao</span></a>

                        </li>

                        <li><a class="waves-effect waves-dark" href="/admin/config" aria-expanded="false"><i
                                        class="fa fa-tags"></i><span class="hide-menu">Cấu hình</span></a>

                    @endif
                    <li class="nav-devider"></li>

                    <li class="nav-small-cap">Chức năng</li>

                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i><img
                                        src="https://i.imgur.com/EoRvO41.png" width="20px"></i><span class="hide-menu">Facebook</span></a>

                        <ul aria-expanded="false" class="collapse">
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-thumbs-up"></i> Tăng
                                    Like Bài Viết</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/facebook-buff/likepost"><i class="fa fa-thumbs-up"></i>
                                            Like Việt 1<span class="badge badge-primary pull-right">{{$serviceMap[39]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/likepost2"><i class="fa fa-thumbs-up"></i>
                                            Like Việt 2<span class="badge badge-primary pull-right">{{$serviceMap[44]}} đ</span></a>
                                    </li>
                                <!--<li><a href="/admin/dichvu/facebook-buff/likepost3"><i class="fa fa-thumbs-up"></i>
                                                Like Việt 3<span class="badge badge-primary pull-right">{{$serviceMap[54]}} đ</span></a></li>-->
                                </ul>
                            </li>

                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-comment"></i> Tăng
                                    Comment</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/facebook-buff/commentfb"><i class="fa fa-comment"></i>
                                            Server 1<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[38]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/comment"><i class="fa fa-comment"></i>
                                            Server 2<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[4]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>

                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-rss"></i> Tăng
                                    Follow</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/facebook-buff/sub"><i class="fa fa-rss"></i>
                                            Follow Thường<span class="badge badge-primary pull-right">{{$serviceMap[6]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/buffsubnhanh"><i class="fa fa-rss"></i>
                                            Follow Nhanh<span class="badge badge-primary pull-right">{{$serviceMap[19]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="/admin/dichvu/facebook-buff/likecomment"><i class="fa fa-thumbs-up"></i>
                                    Tăng Like Comment</a></li>
                            <li><a href="/admin/dichvu/facebook-buff/tangbanbe"><i class="fa far fa-users"></i>
                                    Tăng Bạn Bè - Sub</a></li>
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-eye"></i> Tăng
                                    Mắt</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/facebook-buff/live"><i class="fa fa-eye"></i>
                                            Server 1<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[9]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/live2"><i class="fa fa-eye"></i>
                                            Server 2<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[36]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-thumbs-up"></i> Tăng
                                    Like Page</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/facebook-buff/likepage"><i class="fa fa-thumbs-up"></i>
                                            Like Page<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[7]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/likepage2"><i class="fa fa-thumbs-up"></i>
                                            Like Page 2<span class="badge badge-primary pull-right">{{$serviceMap[43]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/facebook-buff/likepageglobal"><i
                                                    class="fa fa-thumbs-up"></i>
                                            Like Page Tây<span class="badge badge-primary pull-right">{{$serviceMap[53]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        <ul aria-expanded="false" class="collapse" style="">
                            <li><a href="/admin/dichvu/facebook-buff/reviewpage"><i class="fa far fa-star"></i>
                                    Tăng Đánh Giá Page</a></li>
                            <li><a href="/admin/dichvu/facebook-buff/view"><i class="fa fa-video-camera"></i>
                                    Tăng view</a></li>
                            <li><a href="/admin/dichvu/facebook-buff/sharelive"><i class="fa fa-share"></i> Tăng
                                    share group</a></li>
                            <li><a href="/admin/dichvu/facebook-buff/sharewall"><i class="fa fa-share"></i> Tăng
                                    share wall</a></li>
                            <li><a href="/admin/dichvu/facebook-buff/buffmember"><i class="fa fa-users"></i> Tăng Mem
                                    Nhóm</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i><img
                                        src="https://i.imgur.com/EoRvO41.png" width="20px"></i><span class="hide-menu">Facebook Vip</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/facebook-vip/like"><i class="fa fa-thumbs-up"></i> VIP Like</a>
                            </li>
                            <li><a href="/admin/dichvu/facebook-vip/comment"><i class="fa fa-comment"></i> VIP
                                    Comment</a></li>
                            <!--<li><a href="/admin/dichvu/facebook-vip/mat"><i class="fa fa-eye"></i> Vip Mắt</a></li>-->
                        </ul>
                    </li>
                    <li><a href="#" class="has-arrow waves-effect waves-dark"><img src="https://i.imgur.com/0rT4yUL.png"
                                                                                   width="20px"><span class="hide-menu"> Tiktok</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-rss"></i> Tăng
                                    Follow</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/tiktok/sub"><i class="fa fa-rss"></i>
                                            Follow Việt<span class="badge badge-primary pull-right">{{$serviceMap[14]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/tiktok/subglobal"><i class="fa fa-rss"></i>
                                            Follow Tây<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[52]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-thumbs-up"></i> Tăng
                                    Like</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/dichvu/tiktok/like"><i class="fa fa-thumbs-up"></i>
                                            Like Việt<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[13]}} đ</span></a>
                                    </li>
                                    <li><a href="/admin/dichvu/tiktok/likeglobal"><i class="fa fa-thumbs-up"></i>
                                            Like Tây<span
                                                    class="badge badge-primary pull-right">{{$serviceMap[51]}} đ</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="/admin/dichvu/tiktok/view"><i class="fa fa-eye"></i> Tăng View Video</a></li>
                            <li><a href="/admin/dichvu/tiktok/comment"><i class="fa fa-comment"></i> Tăng Comment</a>
                            </li>
                            <li><a href="/admin/dichvu/tiktok/share"><i class="fa fa-share"></i> Tăng Share</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                                    class="fa fa-youtube" style="color: rgb(255, 0, 0);"></i><span class="hide-menu">Youtube</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/youtube/sub"><i class="fa fa-bell"></i> Tăng Subscribe</a></li>
                            <li><a href="/admin/dichvu/youtube/like"><i class="fa fa-thumbs-up"></i> Tăng like video</a>
                            </li>
                            <li><a href="/admin/dichvu/youtube/view"><i class="fa fa-eye"></i> Tăng View Đề Xuất</a>
                            </li>
                            <li><a href="/admin/dichvu/youtube/worldview"><i class="fa fa-eye"></i> Tăng View Social</a>
                            </li>
                            <li><a href="/admin/dichvu/youtube/comment"><i class="fa fa-thumbs-up"></i> Tăng Comment</a>
                            </li>
                            <li><a href="/admin/dichvu/youtube/gioxem"><i class="fa fa-clock-o"></i> Tăng Giờ Xem</a>
                            </li>
                            <li><a href="/admin/dichvu/youtube/dislike"><i class="fa fa-thumbs-up"></i> Tăng lượt
                                    dislike</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i><img
                                        src="https://i.imgur.com/sRqpIt0.png" width="20px"></i><span class="hide-menu">Instagram</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/instagram/sub"><i class="fa fa-rss"></i> Tăng Theo Dõi</a></li>
                            <li><a href="/admin/dichvu/instagram/like"><i class="fa fa-thumbs-up"></i> Tăng like</a>
                            </li>
                            <li><a href="/admin/dichvu/instagram/view"><i class="fa fa-eye"></i> Tăng View</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                                    class="nav-main-link-icon fa fa-twitter" style="color: rgb(29,161,242);"></i><span
                                    class="hide-menu">Twitter</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/twitter/follow"><i class="fa fa-rss"></i> Tăng Follow</a></li>
                            <li><a href="/admin/dichvu/twitter/like"><i class="fa fa-thumbs-up"></i> Tăng Like</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                                    class="nav-main-link-icon fa fa-telegram" style="color: rgb(0, 136, 204);"></i><span
                                    class="hide-menu">Telegram</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/telegram/membergroup"><i class="fa fa-users"></i> Tăng Thành Viên</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><img
                                    src="/admin/images/shopee_logo.png" width="20px"><span
                                    class="hide-menu"> Shopee</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/shopee/sub"><i class="fa fa-rss"></i> Tăng Theo Dõi</a></li>
                            <li><a href="/admin/dichvu/shopee/like"><i class="fa fa-thumbs-up"></i> Tăng Like</a></li>
                        </ul>
                    </li>
                    <!--<li><a class="waves-effect waves-dark" href="/admin/VietBao" aria-expanded="false"><i
                                    class="fa fa-book" style="color: rgb(128,0,0);"></i><span class="hide-menu">Book Viết Báo</span></a>
                    </li>-->

                    <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                                    class="nav-main-link-icon fa fa-google" style="color: rgb(0, 136, 204);"></i><span
                                    class="hide-menu">Google</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="/admin/dichvu/google/gmail"><i class="fa fa-mail-reply-all"></i> Mua tài khoản
                                    Gmail</a>
                            </li>
                        </ul>
                    </li>

                    <!--<li class="active"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="true"><i class="nav-main-link-icon fa fa-puzzle-piece" style="color: rgb(128,0,0);"></i><span class="hide-menu">Dịch Vụ Khác</span></a>
                       <ul aria-expanded="false" class="collapse">
                           <li><a href="/admin/dichvu/other/voice"><i class="fa fa-phone"></i> Gọi Thoại</a></li>
                           <li><a href="/admin/dichvu/other/sms"><i class="fa fa-comment"></i> Nhắn tin</a></li>
                       </ul>
                   </li>-->
                    <br><br><br>.

                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
    @yield('content')
    <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <a id="backToTop"></a>
        <footer class="footer">
            Copyright HKNet © 2020. LTD
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>

<div id="changePasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="passwordChangeForm" method="post" novalidate>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" id="newPassword" class="form-control"
                               placeholder="Enter your password here" required="" data-validation-required-message=
                               "Please enter your password" />
                        <p class="help-block"></p>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="passwordChangeForm" id="changePasswordBtn"
                        class="btn btn-info waves-effect">Save
                </button>
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset("admin/plugins/bootstrap/js/popper.min.js")}}"></script>
{{--<script src="{{asset("admin/plugins/bootstrap/js/bootstrap.min.js")}}"></script>--}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset("admin/js/jquery.slimscroll.js")}}"></script>
<!--Wave Effects -->
<script src="{{asset("admin/js/waves.js")}}"></script>
<!--Menu sidebar -->
<script src="{{asset("admin/js/sidebarmenu.js")}}"></script>
<!--stickey kit -->
<script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
<script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
<!--stickey kit -->
<script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
<script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
<script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>


<!--Custom JavaScript -->
<script src="{{asset("admin/js/custom.min.js")}}"></script>
<script src="{{asset("admin/js/validation.js")}}"></script>
<script src="{{asset("admin/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
        $('.myTable').DataTable({
            order: [[0, 'desc']]
        });
        $('.myTableCustomCustom').DataTable({
            order: [[0, 'desc']]
        });
    });
</script>

<script src="{{asset("admin/js/features/changePassword.js")}}"></script>
<script src="{{asset("admin/js/features/order.js")}}?disable-cache=111111"></script>
<script src="{{asset("admin/js/helper.js")}}"></script>

@yield('scripts')

<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v10.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    const btn = $('#backToTop');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, '300');
    });


</script>

<!-- Your Plugin chat code -->
<div class="fb-customerchat"
     attribution="page_inbox"
     page_id="914109848730081">
</div>
</body>

</html>
