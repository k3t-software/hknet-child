<?php

namespace App\Helpers;

class Facebook extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function likePost($id, $amount, $reaction)
    {
        return $this->post('/likepost', [
            "id" => $id,
            "amount" => (int)$amount,
            "reaction" => $reaction
        ]);
    }

    public function likePost2($id, $amount, $reaction)
    {
        return $this->post('/likepost2', [
            "id" => $id,
            "amount" => (int)$amount,
            "reaction" => $reaction
        ]);
    }

    public function follow($id, $amount)
    {
        return $this->post('/subthuc', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function fastFollow($id, $amount)
    {
        return $this->post('/subnhanh', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }


    public function likePage($id, $amount)
    {
        return $this->post('/likepagethuc', [
            "id" => $id,
            "amount" => (int)$amount
        ]);
    }

    public function buffView($id, $amount)
    {
        return $this->post('/buffView', [
            "id" => $id,
            "amount" => (int)$amount
        ]);
    }

    public function buffMember($id, $amount)
    {
        return $this->post('/buffmember', [
            "id" => $id,
            "amount" => (int)$amount
        ]);
    }

    public function likePage2($id, $amount)
    {
        return $this->post('/likePage2', [
            "id" => $id,
            "amount" => (int)$amount
        ]);
    }

    public function likePageGlobal($id, $amount)
    {
        return $this->post('/likePageGlobal', [
            "id" => $id,
            "amount" => (int)$amount
        ]);
    }


    public function likeComment($id, $amount, $reaction)
    {
        return $this->post('/likeComment', [
            "id" => $id,
            "amount" => (int)$amount,
            "reaction" => $reaction
        ]);
    }


    public function reviewPage($id, $noidung)
    {
        return $this->post('/buffreviewpage', [
            "id" => $id,
            "noidung" => $noidung
        ]);
    }


    public function buffComment($id, $content)
    {
        $result = $this->post('/buffcomment', [
            "id" => $id,
            "content" => $content,
        ]);
        return (object)[
            "success" => isset($result->error) ? !$result->error : true,
            "message" => isset($result->message) ? $result->message : "Server hiện đang bảo trì, vui lòng thử lại sau ít phút."
        ];
    }

    public function buffComment2($id, $content)
    {
        $result = $this->post('/buffcomment2', [
            "id" => $id,
            "content" => $content,
        ]);
        return (object)[
            "success" => isset($result->error) ? !$result->error : true,
            "message" => isset($result->message) ? $result->message : "Server hiện đang bảo trì, vui lòng thử lại sau ít phút."
        ];
    }

    public function buffReviewPage($id, $content)
    {
        return $this->post('/buffreviewpage', [
            "id" => $id,
            "content" => $content,
        ]);
    }

    public function buffLive($id, $amount, $min)
    {
        return $this->post('/bufflive', [
            "id" => $id,
            "amount" => (int)$amount,
            "min" => (int)$min
        ]);
    }

    public function buffLive2($id, $amount, $min)
    {
        return $this->post('/bufflive2', [
            "id" => $id,
            "amount" => (int)$amount,
            "min" => (int)$min
        ]);
    }

    public function buffFriend($id, $amount)
    {
        return $this->post('/tangbanbe', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function shareLive($id, $amount)
    {
        return $this->post('/sharelive', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function shareWall($id, $amount)
    {
        return $this->post('/sharewall', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }


    public function vipLike($id, $package, $period)
    {
        return $this->post('/viplike', [
            "id" => $id,
            "package" => $package,
            "period" => (int)$period
        ]);
    }

    public function vipComment($id, $package, $period, $gender, $content)
    {
        return $this->post('/vipcomment', [
            "id" => $id,
            "package" => $package,
            "period" => (int)$period,
            "gender" => $gender,
            "noidung" => $content
        ]);
    }

    public function reOrderVip($id, $period)
    {
        return $this->post('/facebookVIP/reOrder', [
            "reOrderId" => $id,
            "period" => (int)$period
        ]);
    }

    public function deleteOrderVip($id)
    {
        return $this->post('/facebookVIP/delete', [
            "orderId" => $id,
        ]);
    }

    public function editVipComment($id, $comment)
    {
        return $this->post('/facebookVIP/comment/edit', [
            "orderId" => $id,
            "comment" => $comment
        ]);
    }

}

