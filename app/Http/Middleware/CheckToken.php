<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Service;
use Illuminate\Support\Facades\URL;
use App\Order;


use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    private $testToken = 'yJjbGFpbXMiOnsic3RhZmZfaWQiOiI';

    private function getServiceId($referer)
    {
        $i = 9;
        if ($referer == "likepost2") {
            $i = 44;
        }
        if ($referer == "likepost") {
            $i = 39;
        }
        if ($referer == "subnhanh") {
            $i = 19;
        }
        if ($referer == "viewfacebook") {
            $i = 8;
        }
        if ($referer == "sub") {
            $i = 6;
        }
        if ($referer == "likepagethuc") {
            $i = 7;
        }
        if ($referer == "buffcomment") {
            $i = 38;
        }
        if ($referer == "buffreviewpage") {
            $i = 5;
        }
        if ($referer == "bufflive") {
            $i = 9;
        }
        if ($referer == "bufflive2") {
            $i = 36;
        }
        if ($referer == "viplike") {
            $i = 15;
        }
        if ($referer == "vipcomment") {
            $i = 16;
        }
        //instagram
        if ($referer == "subinstagram") {
            $i = 12;
        }
        if ($referer == "likeinstagram") {
            $i = 11;
        }
        if ($referer == "viewinstagram") {
            $i = 20;
        }
        //tiktok
        if ($referer == "subtiktok") {
            $i = 14;
        }
        if ($referer == "liketiktok") {
            $i = 13;
        }
        if ($referer == "viewtiktok") {
            $i = 24;
        }
        if ($referer == "commenttiktok") {
            $i = 25;
        }
        if ($referer == "sharetiktok") {
            $i = 26;
        }
        //youtube
        if ($referer == "subyoutube") {
            $i = 27;
        }
        if ($referer == "gioxemyoutube") {
            $i = 28;
        }
        if ($referer == "viewyoutube") {
            $i = 29;
        }
        if ($referer == "likeyoutube") {
            $i = 30;
        }
        if ($referer == "commentyoutube") {
            $i = 32;
        }

        return $i;
    }

    public function handle($request, Closure $next)
    {
        $userModel = new User;
        $request->validate([
            "token" => "required|string",
            "amount" => "integer"
        ]);
        $token = $request->input("token");


        $data = [
            "amount" => $request->input("amount"),

        ];

        $referer = null;
        $paths = explode('/', $request->path());
        if (isset($paths[1])) {
            $referer = $paths[1];
        }

        $serviceId = $this->getServiceId($referer);

        // Test token
        if ($request->input('token') === $this->testToken) {
            $request->isBackTest = true;
            return response()->json((object)[
                "success" => 1,
                "order_id" => rand(5000, 9999),
                "rate" => Service::GetPrice($serviceId, 3)
            ]);
        }

        $user = $userModel->GetByToken($token);
        if (!$user) {
            return response()->json([
                "success" => 0,
                "message" => "Sai Token"
            ], 400);
        }
        Auth::login($user);

        $total = Service::GetPrice($serviceId, Auth::user()->role) * $data["amount"];
        if ($total > Auth::user()->balance) {
            return response()->json([
                "message" => "Số dư không đủ!",
            ]);
        }
        // Check Duplicate
        $should_block_duplicate = Order::CheckDuplicate($request->input('id'), $serviceId, Auth::user()->id);
        if ($should_block_duplicate) {
            return response()->json(["message" => "Các đơn đang xử lý & đang chạy sẽ không order mới được cho tới khi hoàn tất !", "success" => 0]);
        }

        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', '*')
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Headers', 'X-CSRF-Token');
    }
}
