/*
Template Name: Admin Press Admin
Author: Themedesigner
Email: niravjoshi87@gmail.com
File: js
*/
$(function () {
    "use strict";
    // ==============================================================
    // Newsletter
    // ==============================================================

    // ==============================================================
    // sparkline chart
    // ==============================================================
    var sparklineLogin = function () {
        $(".spark-count").sparkline(
            [4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9, 12, 4, 9],
            {
                type: "bar",
                width: "100%",
                height: "70",
                barWidth: "2",
                resize: true,
                barSpacing: "6",
                barColor: "rgba(255, 255, 255, 0.3)",
            }
        );

        $(".spark-count2").sparkline(
            [4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9, 12, 4, 9],
            {
                type: "bar",
                width: "100%",
                height: "70",
                barWidth: "2",
                resize: true,
                barSpacing: "6",
                barColor: "rgba(255, 255, 255, 0.3)",
            }
        );

        $("#spark8").sparkline([4, 5, 0, 10, 9, 12, 4, 9], {
            type: "bar",
            width: "100%",
            height: "40",
            barWidth: "4",
            resize: true,
            barSpacing: "5",
            barColor: "#26c6da",
        });
        $("#spark9").sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: "bar",
            width: "100%",
            height: "40",
            barWidth: "4",
            resize: true,
            barSpacing: "5",
            barColor: "#ef5350",
        });
        $("#spark10").sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: "bar",
            width: "100%",
            height: "40",
            barWidth: "4",
            resize: true,
            barSpacing: "5",
            barColor: "#7460ee",
        });
    };
    var sparkResize;

    $(window).resize(function (e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineLogin, 500);
    });
    sparklineLogin();
});
