<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdLogs extends Model
{
    protected $table = "idlogs";

    static function getCount($idPost = null)
    {
        return self::where('idPost', $idPost)->count();
    }
}
