@extends('admin.layouts.master')


@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Nạp Tiền Tự Động</h3>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">

            @foreach($banks as $bank)
                @php
                    $bankInfo = json_decode($bank->value, true);
                       $noidung = str_replace("!!sdt!!", $user->phone, $bankInfo['noidung']);
                       $noidung = str_replace("!!id!!", $user->id, $noidung)
                @endphp
                <div class="col-md-6">
                    <div class="alert alert-info">
                        <h3 class="text-info"><i class="fa fa-money"></i> {{$bankInfo['bank']}}</h3>
                        <p>
                        <ul>
                            <li>Số tài khoản: {{$bankInfo['stk']}}</li>
                            <li>Chủ Tài Khoản: {{$bankInfo['ctk']}}</li>
                            <li>Nội Dung: {{$noidung}}</li>
                            @if($bankInfo['note'] !== "")
                                <li>Lưu ý: {{$bankInfo['note']}}</li>
                            @endif
                        </ul>
                        @if($bankInfo['image'] !== "")
                            <center>
                                <img src="{{$bankInfo['image']}}" style="width: 50%;">
                            </center>
                            @endif

                            </p>
                    </div>

                </div>
            @endforeach


            <div class="col-md-12">

                <br>

                <div role="alert" class="alert alert-warning"><h6 class="font-bold">Chú ý:</h6><h6 class="font-bold"> -
                        Nạp tối thiểu 20.000đ, giao dịch sẽ được cộng từ 2-5p ! Gặp vấn đề trong quá trình thực
                        hiện vui lòng click <a href="https://www.facebook.com/{{$config->getValue('fanpage')}}"
                                               target="_blank"
                                               class="font-bold event-click">vào
                            đây</a> hoặc call {{$config->getValue('sdt')}}</h6></div>

            </div>
            <div class="col-md-12">
                <div class="card mb-0 overflow-hidden">
                    <div class="bg-holder bg-card">
                    </div>
                    <!--/.bg-holder-->
                    <div class="card-body position-relative">
                        <div class="alert alert-info" role="alert">
                            <h5 class="alert-heading font-weight-semi-bold">Thông tin</h5>
                            <p>- Nếu bạn là CTV, Đại lý dịch vụ để nâng cấp bậc chiết khấu tốt hơn vui lòng liên hệ <a href="https://www.facebook.com/{{$config->getValue('fanpage')}}"
                                               target="_blank"
                                               class="font-bold event-click">Admin</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  

    <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <div id="confirmPayment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Xác nhận nhanh</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="order-info pt-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-info" type="button">Thanh Toán</button>
                            <select class="form-control" name="reaction" id="reaction">
                                <option value="vcb">VietComBank</option>
                                <option value="vcb">Momo</option>
                            </select>
                        </div>
                    </div>

                    <div class="input-group pt-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-info" type="button">Mã Giao Dịch:</button>
                        </div>
                        <input data-price="10" value="" min="100" name="amount" type="text" class="form-control"
                               aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <br>
                    <label>Nhập mã xác thực:</label>
                    <form id="confirm_payment_form" method="post" novalidate>
                        {!! Captcha::img() !!}
                        <div class="form-group">
                            <input type="text" name="captcha" id="captcha" class="form-control" required=""
                                   data-validation-required-message=
                                   "Please enter your captcha" />
                            <p class="help-block"></p>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="confirm_payment_form" class="btn btn-info waves-effect">Xác nhận
                    </button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('scripts')

    <script>

        $('input[name="target_phone"]').change(e => {
            const phone = e.target.value;
            if (phone) {
                $.get('./helper/GetUserFromPhone', { phone }).then(res => {
                    $('#target_name').val(res.name);
                    $('#confirm-transfer-btn').attr('disabled', false);
                }).catch(() => {
                    $('#target_name').val('Không tìm thấy người dùng này !');
                    $('#confirm-transfer-btn').attr('disabled', true);
                });
            }
        });

        $('#moneyTranfForm').submit(e => {
            const form = $('#moneyTranfForm');
            let formData = form.serializeArray();
            let values = {};
            $(formData).each(function (index, obj) {
                values[obj.name] = obj.value;
            });
            $(form).find('button[type="submit"]').attr('disabled', true);
            $.ajax({
                type: 'POST',
                url: '../admin/moneyTransfer',
                data: values
            }).then(res => {
                const message = res && res.message;
                swal(
                    {
                        title: 'Thành công !!',
                        text: message,
                        type: 'success'
                    }, () => {
                        window.location = '/admin/transactions';
                    });
            }).catch(err => {
                const message = err.responseJSON && err.responseJSON.message;
                swal({
                    title: 'Lỗi !!',
                    text: message,
                    type: 'error'
                });
            }).always((e) => {
                $(form).find('button[type="submit"]').attr('disabled', false);
            });
            e.preventDefault();
        });

        $('#confirm_payment_form').submit(function (e) {
            e.preventDefault();
            const captcha = $('#captcha').val();
            $.ajax({
                url: '',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    captcha
                },
                dataType: 'JSON',
                success: function (response) {
                    alert(response.message);
                },
                error: function (e) {

                }
            });
        });
    </script>

@endsection
