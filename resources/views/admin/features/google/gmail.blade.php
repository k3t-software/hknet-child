@php
    $status = ['<span class="badge badge-info">Đợi duyệt</span>', '<span class="badge badge-primary">Thành Công</span>','<span class="badge badge-info">Đang chạy</span>', '<span class="badge badge-warning">Hoàn tiền</span>' ];
	$role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice']

@endphp
@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Mua Account Gmail</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
                <li class="breadcrumb-item active">Mua Gmail Account</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Buy Gmail Accounts</h4>
                        <h6 class="card-subtitle">Đây là tài khoản Gmail New</h6>
                        <div class="row pt-3">
                            <div class="col-md-6 b-r">
                                <form id="infoForm">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Số Lượng Mua:</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="amount" name="amount"
                                                   placeholder="Số Lượng">
                                        </div>
                                    </div>

                                    <div class="order-info pt-3">

                                        <input type="hidden" id="price" value="{{$service[$role[Auth::user()->role]]}}"
                                               name="price">
                                        <div class="row pt-3">
                                            <div class="input-group col-md-6">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-info" type="button">Tổng tiền</button>
                                                </div>
                                                <input disabled data-price="10000" id="total_price" value=0 name="total"
                                                       type="number"
                                                       class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" data-service="{{$service->id}}"
                                                        data-function="gmail" id="orderBtn"
                                                        class="btn waves-effect waves-light btn-danger btn-block"><i
                                                            class="fa fa-shopping-cart"></i> Thực hiện
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close"></button>
                                    <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
                                    - Định dạng: Mail|Mật khẩu|Mail Khôi Phục<br>
                                    - Mail bán xong là của người mua, bạn sẽ chịu trách nhiệm về các mục đích mà bạn sử dụng

                                </div>
                            </div>
                        </div>
                        <hr class="py-3">
                        <h4 class="card-title">Đơn hàng</h4>
                        <h6 class="card-subtitle">Danh sách các đơn hàng của bạn</h6>
                        <div class="table-responsive p-1">
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Số lượng</th>
                                    <th class="text-right">Tổng tiền</th>
                                    <th class="text-right">Trạng thái</th>
                                    <th class="text-right">Thời gian</th>
                                    <th class="text-right">Kết quả</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td class="text-center">{{$order->quantity}}</td>
                                        <td class="text-right">{{$order->total}}</td>
                                        <td class="text-right">{!!$status[$order->status]!!}</td>
                                        <td class="text-right">{{date('d/m/Y', strtotime($order->created_at))}}</td>
                                        <td class="text-right">
                                            <a href="/admin/resource/view/{{$order->id}}" class="btn btn-default"
                                               target="_blank">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@section('scripts')
    <script src="{{asset("admin/js/google/gmail.js")}}"></script>
@endsection
