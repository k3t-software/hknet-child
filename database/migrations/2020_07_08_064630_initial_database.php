<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('password');
            $table->integer('role')->default(3);
            $table->string('facebookId')->nullable();
            $table->bigInteger("balance")->default(0);
            $table->tinyInteger('accept_money_transfer')->default(0);
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('categories');
            $table->timestamps();
        });

        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->bigInteger("apiPrice")->default(50);
            $table->bigInteger("agencyPrice")->default(50);
            $table->bigInteger("collaboratorsPrice")->default(50);
            $table->bigInteger("guestPrice")->default(50);
            $table->integer("status")->default(0);
            $table->integer("unit_duration")->default(100);
            $table->integer("type")->default(0);
            $table->string('note')->nullable();
            $table->string('slug')->nullable();
            $table->tinyInteger('block_duplicate')->default(0);
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("service_id")->nullable();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->integer("quantity")->nullable();
            $table->string("target")->nullable();
            $table->integer("status")->default(0);
            $table->integer("duration")->default(100);
            $table->bigInteger("total")->nullable();
            $table->string('note')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('service_id')->references('id')->on('services');
            $table->timestamp('first_add_at')->useCurrent();
            $table->timestamps();
        });
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->integer("amount")->nullable();
            $table->integer("type")->nullable();
            $table->string('note')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('services');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('users');
        Schema::dropIfExists('settings');
    }
}


/*

ALTER TABLE `users` ADD `accept_money_transfer` TINYINT NOT NULL DEFAULT '1' AFTER `balance`;
ALTER TABLE `services` ADD `block_duplicate` TINYINT NOT NULL DEFAULT '0' AFTER `type`;
ALTER TABLE `orders` ADD `first_add_at` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;
UPDATE `orders` SET `first_add_at` = `created_at`

CREATE TABLE `giftcodes` (
  `id` int NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int NOT NULL,
  `quantity` int NOT NULL,
  `used` int NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `chats` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `target_id` bigint UNSIGNED DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(999) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
INSERT INTO `settings` (`id`, `name`, `value`, `note`, `created_at`, `updated_at`) VALUES
(1, 'banner', '{\"isShow\":\"on\",\"title\":\"Với tiêu chí Uy Tín đặt lên\",\"html\":\"<div class=\\\"hero__content\\\">\\r\\n                        <h2 class=\\\"hero__title \\\">Về chúng tôi sd<br></h2>\\r\\n                        <div class=\\\"hero__intro\\\">HKNet là doanh nghiệp được ra đời từ 2018 có mã số thuế 0401939005. <br>Hoạt động từ những năm 2017, với các tiền thân GetSub․Top, VipFb․Vn HuuKy9x․Com. Và Hiện tại phát triển trang web phi thương mại TraoDoiCheo․Com cùng\\r\\n                        website thương mại dich vụ là HKNet․Vn, Với tiêu chí Uy Tín đặt lên hàng đầu và tầm nhìn trong tương lai, HKNet là một trong những trang web dịch vụ mạng xã hội lớn Việt Nam</div>\\r\\n\\r\\n                        <div class=\\\"hero__button\\\"><a class=\\\"btn-background\\\" href=\\\"/login\\\">Sử Dụng Ngay</a> <a class=\\\"btn-white\\\" href=\\\"/register\\\">Đăng Ký Ngay</a></div>\\r\\n                    </div>\"}', '', '0000-00-00 00:00:00', '2021-07-12 16:48:10'),
(2, 'token', '[\"EAAAAZAw4FxQIBAML73WONaxmZB6D3ZBDgcYD5Ct6t7WqZB6uxta3ZCM7LavhTHkChY2ypCwQrFBZCQTiKYb1NFNqBcCgCJiANB6Xw4qh9AZArgKQo1ZB1QKkVxp5Cvki8mVcQlHnfBCdgpSuYxW9KRBnU9WZCh6UfYSZBnlC1lvM8L1NsafMN5eEQ7XRRUeqAaZADYZD\"]', '', '2021-07-12 17:00:00', '2021-07-27 16:42:04');
*/


/* Update 08/05

CREATE TABLE `hknet_vn`.`partner_accounts` ( `id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(255) NOT NULL , `password` VARCHAR(255) NOT NULL , `partner_name` VARCHAR(255) NOT NULL , `used` INT NOT NULL , `is_stop` BOOLEAN NOT NULL , `note` VARCHAR(255) NOT NULL , `created_at` DATE NOT NULL , `updated_at` DATE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `services` ADD `error_count` INT NOT NULL DEFAULT '0' AFTER `slug`;

 */
