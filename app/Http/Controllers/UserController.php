<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Config;
use App\GiftCode;
use App\IpLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\Constant;
use Illuminate\Support\Facades\Mail;
use MessageBird;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    private $model;
    private $transactionModel;


    public function __construct()
    {
        $this->model = new User;
        $this->transactionModel = new Transaction;
        $this->chatModel = new Chat;
        $this->giftCodeModel = new GiftCode;
        $this->ipLogModel = new IpLog;
        $this->configModel = new Config;
    }

    public function view()
    {
        $data["users"] = $this->model->get();
        return view("admin.managements.users", $data);
    }


    public function getLowerUser(Request $req)
    {
        $user = Auth::user();
        $upUser = $req->input('id') ? $req->input('id') : $user->id;

        $lowerUsers = $this->model->select('id', 'facebookId', 'name', 'phone', 'role', 'balance',
            'created_at')->where('refer_user',
            $upUser)->get()->toArray();

        $result = array_map(function ($lowerUser) {
            $totolCharge = $this->transactionModel->where('type', Constant::TRANSACTION_TYPE_CHARGE)->where('user_id',
                $lowerUser["id"])->get()->sum("amount");
            $lowerUserCount = $this->model->where('refer_user', $lowerUser["id"])->count();

            $lowerUser["total_charge"] = $totolCharge;
            $lowerUser["lower_count"] = $lowerUserCount;
            return $lowerUser;
        }, $lowerUsers);

        return response()->json($result);
    }

    public function referManager()
    {
        $data = [];
        $user = Auth::user();

        $queryTransaction = $this->transactionModel->where('type', Constant::TRANSACTION_TYPE_COMMISSION);

        if ($user->role === 0) {
            $data['transactions'] = $queryTransaction->get();
        } else {
            $data['transactions'] = $queryTransaction->where('user_id', $user->id)->get();
        }

        $data['referCode'] = strtoupper(substr($user->phone, -4) . "_" . $user->id);
        $data['user'] = $user;

        // Refer manager
        $level1Users = DB::table('users')->where([
            "refer_user" => $user->id
        ])->get()->toArray();

        $data['level1Count'] = count($level1Users);

        $data['level2Count'] = 0;
        foreach ($level1Users as $level1User) {
            $data['level2Count'] += DB::table('users')->where([
                "refer_user" => $level1User->id
            ])->get()->count();
        }
        $data['tongDoanhThu'] = 0;
        foreach ($data['transactions'] as $transaction) {
            $data['tongDoanhThu'] += $transaction['amount'];
        }

        return view("admin.managements.referManger", $data);
    }

    public function getById(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }
        $user = $this->model->get($req->input('id'));
        if ($user) {
            return response(["status" => "OK", "user" => $user], 200);
        }
    }

    public function sendSupportMessage($targetId)
    {
        $chat = new Chat();
        $chat->message = strip_tags("Chào bạn ! Bạn cần hỗ trợ gì không ? ");
        $chat->user_id = 1;
        $chat->target_id = $targetId;
        $chat->created_at = date('Y-m-d H:i:s');
        $chat->save();
    }

    public function verify(Request $req)
    {
        $otp = $req->input("otp");
        $user = Auth::user();
        if ($user->otp === $otp) {
            $user->otp = "";
            $user->verify = 1;
            $user->save();
            return response()->json([
                "success" => 1,
                "message" => "Email của bạn đã được xác thực thành công !"
            ]);
        } else {
            return response()->json([
                "success" => 0,
                "message" => "OTP ko hợp lệ hoặc hết han"
            ], 300);
        }

    }

    public function sendOtp(Request $req)
    {

        $validator = Validator::make($req->all(), [
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => 0,
                "message" => "Email ko hợp lệ"
            ], 300);
        }

        /* if ($req->session()->get('last_send_otp_at') > strtotime("-1 minute")) {
             return response()->json([
                 "success" => 0,
                 "message" => "Thử lại sau 1 phút"
             ], 300);
         } */
        $req->session()->put('last_send_otp_at', time());

        // Create new email
        $otp = rand(1000, 9999);
        $user = Auth::user();
        $user->email = $req->input("email");
        $user->otp = $otp;
        $user->save();

        $to_name = 'Guest';
        $to_email = $req->input("email");
        $data = array('otp' => $otp);

        Mail::send('verifyEmail', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Verify email');
            $message->from('support@hknet.vn', 'HKNET Support');
        });
        return response()->json([
            "success" => 1,
            "message" => "Email đã được gửi đi !"
        ]);
    }

    public function refundMoney($value)
    {
        $user = Auth::user();
        $user->balance += $value;
        $user->save();
    }

    public function create(Request $req)
    {
        if (!isset($req)) {
            return response("Request is invalid !", 400);
        }
        // Check coutnt ip
        $ipLogCount = $this->ipLogModel->getIpCount($req->ip());

        $user = new User();
        $user->phone = $req->input("phone");
        $user->password = Hash::make($req->input("password"));
        $user->name = $req->input("name");
        $user->role = $this->configModel->getValue('default_role');
        $user->facebookId = $req->input("facebookId");

        if ($req->input('giftCode') !== null && $req->input('giftCode') !== '') {
            $giftCode = $this->giftCodeModel->where('code', $req->input('giftCode'))->first();
            if (isset($giftCode) && $giftCode->used < $giftCode->quantity) {
                if ($ipLogCount === 0) {
                    $user->balance = $giftCode->value;
                    $giftCode->used += 1;
                    $giftCode->save();
                    $ipLog = new IpLog();
                    $ipLog->ip = $req->ip();
                    $ipLog->save();
                }

            } else {
                return response("GiftCode not avaiable", 400);
            }
        }

        $referCode = $req->input("referCode");
        if (isset($referCode) && $referCode !== "") {
            $referCodeParse = explode("_", $referCode);
            $fromId = (int)$referCodeParse[1];
            $fromUser = $this->model->get($fromId);
            if (isset($fromUser)) {
                $user->refer_user = $fromId;
                $user->role = $fromUser->role;
            }
        }

        $user->save();
        $this->sendSupportMessage($user->id);
        //$this->sendVoicee($req->input("phone"),
        //'Chào mừng quý khách đã đăng ký thành công tại hệ thống HK nét Chấm Vê en ! HK Nét, Hệ thống uy tín hàng đầu Việt Nam');

        return response("OK", 200);
    }

    public function sendVoicee($phone_recipient, $message)
    {
        $client = new MessageBird\Client(env('MB_KEY'));

        $call = new MessageBird\Objects\Voice\Call;
        // $call->source = $row->sim->number;
        $call->source = str_replace('.', '', '849999999');
        $call->destination = $this->convertPhoneVN($phone_recipient); // Số nhận cuộc gọi

        $flow = new MessageBird\Objects\Voice\CallFlow;
        $flow->title = 'Đăng Ký Thành Công';

        $step = new MessageBird\Objects\Voice\Step;
        $step->action = 'say';
        $step->options = [
            // 'payload' => 'Mã xác nhận của quý khách là . 5 . 3 . 8 . 8 . 9 . xin nhắc lại, 5 . 3 . 8 . 8 . 9', // nội dung cuộc gọi thoại
            'payload' => $message,
            'language' => 'vi-VN', // ngôn ngữ
            'voice' => 'male'
        ];
        $flow->steps = [$step];
        $call->callFlow = $flow;
        return $client->voiceCalls->create($call);
    }

    public function convertPhoneVN($number)
    {
        return preg_replace("/^0/", "84", $number);
    }

    public function update(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }
        $user = User::find($req->input('id'));
        $user->name = $req->input("name");
        $user->phone = $req->input("phone");
        $user->code = $req->input("code");
        // $user->email = $req->input("email");
        $user->role = $req->input("role");
        $user->save();
        return response("OK", 200);
    }

    private function getReferUserId($id)
    {
        $user = $this->model->where('id', $id);
        $referUserId = (isset($user->first()->refer_user) && $user->first()->refer_user !== 0) ? $user->first()->refer_user : null;

        return $referUserId;
    }

    public function getUpperUser($id)
    {
        $upperUsers = [];
        $referUserId = $id;

        while ($referUserId !== null) {
            $referUserId = $this->getReferUserId($referUserId);
            if ($referUserId !== null) {
                array_push($upperUsers, $referUserId);
            }
        }

        return $upperUsers;
    }

    public function addBalanceForReferUser($guest, $chargeAmount)
    {

        /*  Admin, Dai ly, CTV, Khach */
        $cap1 = [100, 5, 7, 20];
        $cap2 = [100, 2, 3, 10];

        $phanTramHoaHong = array($cap1, $cap2);

        $referUserIds = $this->getUpperUser($guest->id);

        foreach ($referUserIds as $index => $id) {
            $rank = $index;

            if ($rank >= 2) {
                break;
            }

            $user = $this->model->where("id", $id)->get()->first();
            $receivedPercent = $phanTramHoaHong[$rank][$guest->role];

            $commison = ($chargeAmount / 100) * $receivedPercent;
            $this->transactionModel->Create($user->id, Constant::TRANSACTION_TYPE_COMMISSION, $commison,
                "Hoa hồng từ {$guest->name} - {$guest->phone} [{$receivedPercent} %]");
            $user->balance_2 += $commison;
            $user->save();
        }
    }

    public function AddMoney(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $amount = $req->input('amount');

        $user = $this->model->find($req->input('id'));

        $this->addBalanceForReferUser($user, $amount);

        $user->balance += $amount;

        $type = 0;

        if ($amount < 0) {
            $type = 1;
        }

        $user->save();

        $this->transactionModel->Create($user->id, $type, $amount, 'Nạp tiền');

        return response(["status" => "OK", "user" => $user], 200);
    }

    public function ChangeRole(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $user = $this->model->find($req->input('id'));
        if ($req->input('role') && $req->input('role') !== '') {
            $user->role = $req->input("role");
        }

        $user->accept_money_transfer = $req->input('accept_money_transfer') === 'on' ? 1 : 0;

        if ($req->input('newPassword') && $req->input('newPassword') !== '') {
            $newPassword = $req->input('newPassword');
            $user->password = Hash::make($newPassword);
        }

        $user->save();
        return response(["status" => "OK"], 200);
    }

    public function changePassword(Request $req)
    {

        if (!Auth::check()) {
            return 0;
        }
        if ($req->input('password') == null) {
            return response("password is invalid !", 400);
        }
        $newPassword = $req->input('password');
        $user = User::find(Auth::user()->id);
        $user->password = Hash::make($newPassword);
        $user->save();
        return response("OK", 200);
    }


    public function delete(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }
        $user = User::find($req->input('id'));
        if ($user) {
            $user->delete();
        }
        return response("OK", 200);
    }

    public function AdvancedSettings(Request $request)
    {

        return view("admin.managements.advanced");
    }


    public function getInfoFromPhone(Request $req)
    {
        $userInfo = $this->model->where('phone', $req->input('phone'))->first();
        if (isset($userInfo->name)) {
            return response()->json([
                "success" => 1,
                "name" => $userInfo->name
            ]);
        } else {
            return response()->json([
                "success" => 0,
                "message" => "Can't not find user"
            ], 400);
        }
    }


    public function moneyTransfer(Request $req)
    {
        $fromUser = Auth::user();
        $targetUser = $this->model->where('phone', $req->input('target_phone'))->first();

        if ($fromUser->id === $targetUser->id) {
            return response()->json([
                "success" => 0,
                "message" => "Bạn không thể chuyển tiền cho bản thân. "
            ], 402);
        }

        if ($fromUser->role !== $targetUser->role && $fromUser->role !== 0) {
            return response()->json([
                "success" => 0,
                "message" => "Chỉ được chuyển tiền cho người dùng cùng cấp. "
            ], 402);
        }

        if (!isset($targetUser)) {
            return response()->json([
                "success" => 0,
                "message" => "Ko tìm thấy người dùng này"
            ], 402);
        }

        $amount = (int)$req->input('amount');
        $amountFormat = number_format($amount);

        if ($amount > $fromUser->balance) {
            return response()->json([
                "success" => 0,
                "message" => "Số tiền trong tài khoản của bạn hiện không đủ"
            ], 402);
        }
        DB::beginTransaction();

        try {
            // Target user update
            $targetUser->balance += $amount;
            $targetUser->save();

            // User update
            $fromUser->balance -= $amount;
            $fromUser->save();
            DB::commit();

            $transaction = new Transaction;
            $transaction->Create($fromUser->id, Constant::TRANSACTION_TYPE_TRANSFER, -$amount,
                "Chuyển tiền tới {$targetUser->phone} - [{$targetUser->name}].");
            $transaction->Create($targetUser->id, Constant::TRANSACTION_TYPE_RECEIVCE, +$amount,
                "Nhận tiền từ {$fromUser->phone} - [$fromUser->name].");

            return response()->json([
                'success' => 1,
                'message' => "Bạn đã chuyển thành công {$amountFormat}đ cho {$targetUser->name}."
            ]);
        } catch (Exception $err) {
            DB::rollBack();
            return response()->json([
                'success' => 1,
                'message' => "Xảy ra lỗi trong quá trình chuyển tiền"
            ]);
        }
    }
}
