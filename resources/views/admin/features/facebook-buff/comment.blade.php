@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];
@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Buff Comment</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">Buff Comment</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tạo mới Buff Comment</h4>
						<h6 class="card-subtitle">Chức năng buff Comment</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm" name="infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Facebook post</label>
										<div class="input-group">
											<input type="text" class="form-control" id = "profileLink" placeholder="Nhập link hoặc page id">
											<div class="input-group-append">
												<button class="btn btn-primary" id = "getID" type="button">Lấy thông tin</button>
											</div>
										</div>
									</div>

									<div class = "profile-info">

										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Post ID</button>
											</div>
											<input name ="id" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<div class="row">

											<div class="col-12">
												<label>Giới tính: </label>
												<input class = "radio-col-light-blue" value = "Nam" name="gender" type="radio" id="radio_1">
												<label for="radio_1">Nam</label>
												<input class = "radio-col-light-blue" value = "Nữ" name="gender" type="radio" id="radio_2">
												<label for="radio_2">Nữ</label>
												<input class = "radio-col-light-blue" value = "Tất cả" name="gender" type="radio" id="radio_3" checked="">
												<label for="radio_3">Tất cả</label>

											</div>

											<div class="col-12">

												<div class="form-group">
													<label>Comments (mỗi dòng 1 comment)</label>
													<textarea data-price = "{{$service[$role[Auth::user()->role]]}}" name = "comment" id="comment" class="form-control" rows="5"onkeyup="getVal();"></textarea>

														<script>
														function getVal()
														{
														document.getElementById('abc').innerHTML = countLine();
														}
														function countLine(){
														var comment=document.infoForm.comment.value;
														return (comment=='') ? '0' : (comment.split("\n").length);
														}
														</script>
														<div style="padding-bottom: 10px; font-weight: bold">Số lượng: <span id="abc">0</span> Comment</div>
												</div>
											</div>

											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Đơn giá</button>
												</div>
												<input name ="price" disabled type="text" class="form-control" value = "{{$service[$role[Auth::user()->role]]}} đ / {{$service['unit']}}" aria-label="Username" aria-describedby="basic-addon1">
											</div>

										</div>


										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value =0 name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "comment" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>

										</div>

									</div>



								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
									<p>+ Bình luận lên chậm đều,không trùng, tài khoản có avt lịch sự !</p>
									<p>+ Bình luận hoạt động tốt từ 7h - 23h</p>
									<p>+ Ghi thêm 10-20% nội dung comment so với số lượng để đảm bảo lên đủ.</p>
									<p>+ Không nhập ID không công khai hoặc chế độ tùy chỉnh (cố tình order sẽ không được hoàn tiền)</p>

								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-subtitle">Danh sách các đơn hàng buff comment của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Thông tin</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thời gian</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class="text-right"><span class="badge btn btn-outline-danger">{{$order->notify}}</span></td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class = "text-right">{{$order->created_at}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src="{{asset("admin/js/facebook-buff/comment.js")}}"></script>
@endsection