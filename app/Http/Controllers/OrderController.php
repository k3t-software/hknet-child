<?php

namespace App\Http\Controllers;


use App\Config;
use App\FlashSale;
use App\Helpers\Facebook;
use App\Helpers\Helper;
use App\Helpers\Instagram;
use App\Helpers\Profile;
use App\Helpers\Shopee;
use App\Helpers\Telegram;
use App\Helpers\TikTok;
use App\Helpers\TraoDoiCheo;
use App\Helpers\TuongTacCheo;
use App\Helpers\Twitter;
use App\Helpers\VipLikeXyz;
use App\Helpers\Youtube;
use App\IdLogs;
use App\Resources;
use App\Token;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Constant;
use App\Transaction;
use App\Service;
use App\Order;
use App\Setting;
use App\User;
use Datetime;

use App\Helpers\TraoDoiSub;
use App\Helpers\AutoFbPro;


use Illuminate\Support\Facades\DB;
use MessageBird;
use function response;


class OrderController extends Controller
{
    protected $PHPSESSID;
    protected $XSRFToken;
    protected $_ggwp;
    protected $session;
    protected $traoDoiSub;
    protected $traoDoiCheo;
    protected $tuongTacCheo;
    protected $autoFBPro;
    private $model;
    private $serviceModel;
    private $configModel;


    public function __construct()
    {
        $this->model = new Order;
        $this->serviceModel = new Service;
        $this->settingModel = new Setting;
        $this->traoDoiCheo = new TraoDoiCheo();
        $this->configModel = new Config;
        $this->token = $this->configModel->getValue('token');
        $this->youtube = new Youtube($this->token);
        $this->instagram = new Instagram($this->token);
        $this->twitter = new Twitter($this->token);
        $this->telegram = new Telegram($this->token);
        $this->shopee = new Shopee($this->token);
        $this->profile = new Profile($this->token);
    }

    public function getRandomToken()
    {
        $fiftyMinute = date('Y/m/d H:i:s', time() - 60 * 30);
        $tokenModel = new Token;
        $token = $tokenModel->where('last_use', '<', $fiftyMinute)->where('isExpired', '=',
            0)->limit(1)->get()->first();

        if ($token) {
            $token->last_use = date('Y/m/d H:i:s', time());
            $token->save();
            return $token;
        } else {
            return null;
        }
    }

    public function getById(Request $req, $id)
    {
        if ($id == null) {
            return response("ID is invalid !", 400);
        }
        $order = $this->model->get($id);
        if ($order) {
            return response(["status" => "OK", "order" => $order], 200);
        }
    }

    public function ChangeStatus(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $status = $req->input("status");
        $backMoney = $req->input("backMoney");

        $order = $this->model->get($req->input('id'));
        $notify = $req->input("notify");
        // Update new status

        if ($status === '3' && $order->status !== 3) {

            // Tru Tien
            $userModal = new User;
            $user = $userModal->get($order->user_id);
            $user->balance += (int)$backMoney;
            $user->save();

            // Get name of service
            $serviceModel = new Service;
            $service = $serviceModel->get($order->service_id);

            // Luu log
            $transaction = new Transaction;
            $transaction->Create($user->id, Constant::TRANSACTION_TYPE_REFUND, $backMoney,
                "Hoàn tiền dịch vụ $service->name [ $order->target ] <br /> $notify");
        }
        $order->status = (int)$status;
        $order->notify = $notify;
        $order->save();


        return response(["status" => "OK"], 200);
    }


    public function ChangeStartCount(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $order = $this->model->find($req->input('id'));
        $order->startcount = $req->input("startcount");

        $order->save();
        return response(["status" => "OK"], 200);
    }

    public function ChangeNotify(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $order = $this->model->find($req->input('id'));
        $order->notify = $req->input("notify");

        if ($order->service_id == 64) {
            $order->note = $req->input("notify");
            $order->status = 1;
        }

        $order->save();
        return response(["status" => "OK"], 200);
    }

    public function Changeviplike_status(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }

        $order = $this->model->find($req->input('id'));
        $order->viplike_status = $req->input("viplike_status");

        $order->save();
        return response(["status" => "OK"], 200);
    }

    public function view(Request $req, $type)
    {
        // header('content-type', 'application/json');
        $limit = $req->input("limit", 500);
        $types = ["api" => 0, "lentay" => 1];
        $data["orders"] = $this->model->orderBy('id', 'DESC')->where('notify', 'like', '%hoàn tiền%')->limit($limit)->get();
        //$data["orders"] = $this->model->orderBy('id', 'DESC')->get();
//        $this->model->CheckCompleted($data['orders']);
        // return response()->json($data["orders"]);
        // return json_encode($data["orders"]);
        $data["services"] = $this->serviceModel->get();
        return view("admin.managements.orders", $data);
    }

    public function CheapFacebook(Request $req, $feature)
    {
        $result = null;
        $data = null;
        $service_id = $req->input("service_id");

        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if ($feature == "likepost2") {
            $result = $this->CheapFacebookLikePost($req);
            $data = $result["data"];
            $result = $result["result"];
        } else {
            $this->LoginVietnamFB();

            $data = [
                "id" => $req->input("id"),
                "amount" => $req->input("amount"),
                "covid_type" => 3
            ];


            $result = $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=covid_$feature&site=add_new", $data);
        }

        if ($result->success == 1) {
            $this->CreateOrder($service_id, $data['id'], $data['amount']);
        }

        return response()->json($result);
    }

    public function CheapFacebookLikePost(Request $req)
    {
        $data = [
            "post_id" => $req->input("id"),
            "qty" => (int)$req->input("amount"),
        ];

        $result = $this->PostRequestTraoDoiCheo("https://traodoicheo.com/api/tangLike", $data);

        $status = strpos($result, "Mua like thành công!!");

        $data["amount"] = $data["qty"];
        $data["id"] = $data["post_id"];
        if (!$status) {
            return [
                "result" => (object)["success" => 1, "message" => "Thêm buff like thành công !"],
                "data" => $data
            ];
        }

        return ["result" => (object)["success" => 0, "message" => "Thêm buff like thành công !"], "data" => $data];
    }

    public function Instagram(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }
        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng "
            ], 200);
        }

        $service_id = $req->input("service_id");

        switch ($feature) {
            case "comment":
            {
                $amount = $req->input("amount");
                if ($amount < 10 || $amount > 10000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 10 và tối đa 10000",
                    ], 200);
                }
                $result = $this->BuffCommentIns($req);
                break;
            }
            case "view":
            {
                $amount = $req->input("amount");
                if ($amount < 500 || $amount > 10000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 500 và tối đa 10000",
                    ], 200);
                }
                $result = $this->BuffViewIns($req);
                break;
            }
            case "sub":
            {
                $amount = $req->input("amount");
                if ($amount < 50 || $amount > 10000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 50 và tối đa 10000",
                    ], 200);
                }
                $result = $this->BuffSubIns($req);
                break;
            }
            case "like":
            {
                $amount = $req->input("amount");
                if ($amount < 50 || $amount > 10000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 500 và tối đa 10000",
                    ], 200);
                }
                $result = $this->BuffLikeIns($req);
                break;
            }
        }

        if ($result['result']->success == 1) {
            $moreConfigs['result'] = $result['result'];

            $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                $data["note"] ?? null, null, null, $moreConfigs);
        }
        return response()->json($result['result']);
    }

    public function BuffSubIns(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel" => 3
        ];
        return [
            "result" => $this->instagram->buffSub($data['id'], $data['amount']),
            "data" => $data
        ];
    }

    public function BuffLikeIns(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel" => 3
        ];
        return [
            "result" => $this->instagram->buffLike($data['id'], $data['amount']),
            "data" => $data
        ];
    }

    public function BuffViewIns(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
            "channel" => 1
        ];
        //$this->sendVoice('Thông báo có đơn hàng mới');
        return [
            "result" => $this->instagram->buffView($data['id'], $amount),
            "data" => $data
        ];
    }

    public function TikTok(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng ! "
            ], 200);
        }

        $serviceId = $req->input("service_id");
        $id = $req->input("id");
        $amount = $req->input("amount");
        $reaction = $req->input("reaction");
        $content = $req->input('comment');
        $note = $req->input("note");

        $tiktok = new TikTok($this->token);
        $result = [];

        switch ($feature) {
            case "sub":
                $result = $tiktok->buffSub($id, $amount);
                break;
            case "subglobal":
                $result = $tiktok->buffSubGlobal($id, $amount);
                break;
            case "like":
                $result = $tiktok->buffLike($id, $amount);
                break;
            case "likeglobal":
                $result = $tiktok->buffLikeGlobal($id, $amount);
                break;
            case "view":
                $result = $tiktok->buffView($id, $amount);
                break;
            case "comment":
                $result = $tiktok->buffComment($id, $content);
                break;
            case "share":
                $result = $tiktok->buffShare($id, $amount);
                break;
            default:
                var_dump($feature);
                break;
        }

        if (isset($result->success) && $result->success) {
            $price = $this->serviceModel->GetPrice($serviceId, Auth::user()->role);
            $moreConfigs['result'] = $result;
            $this->CreateOrder($serviceId, $id, $amount,
                $note ?? null, null, $price * $amount, $moreConfigs);
        }

        return response()->json($result);
    }

    public function Youtube(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống ",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng ! "
            ], 200);
        }

        $service_id = $req->input("service_id");
        switch ($feature) {
            case "sub":
            {
                $amount = $req->input("amount");
                if ($amount < 100 || $amount > 15000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 100 và tối đa 15000",
                    ], 200);
                }
                if ($req->input("amount") % 100 != 0) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng phải bội của 100",
                    ], 200);
                }
                $result = $this->BuffSubYoutube($req);
                break;
            }
            case "gioxem":
            {
                $amount = $req->input("amount");
                if ($amount < 2000 || $amount > 8000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 2000 giờ và tối đa 8000 giờ",
                    ], 200);
                }
                if ($req->input("amount") % 1000 != 0) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng phải bội của 1000",
                    ], 200);
                }
                $result = $this->BuffGioXemYoutube($req);
                break;
            }
            case "view":
            {
                $amount = $req->input("amount");
                if ($amount < 500 || $amount > 30000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 500 và tối đa 30000, lớn hơn vui lòng chạy loại social",
                    ], 200);
                }
                if ($req->input("amount") % 100 != 0) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng phải bội của 100",
                    ], 200);
                }
                $result = $this->BuffViewYoutube($req);
                break;
            }
            case "worldview":
            {
                $amount = $req->input("amount");
                if ($amount < 10000 || $amount > 10000000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "View ADS số lượng tối thiểu 10000, số lượng trên 1 triệu view vui lòng liên hệ admin để có giá tốt",
                    ], 200);
                }

                $result = $this->BuffViewNhanhYoutube($req);
                break;
            }
            case "like":
            {
                $amount = $req->input("amount");
                if ($amount < 100 || $amount > 15000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 100 và tối đa 15000, mua lớn hơn liên hệ admin",
                    ], 200);
                }
                if ($req->input("amount") % 100 != 0) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng phải bội của 100",
                    ], 200);
                }
                $result = $this->BuffLikeYoutube($req);
                break;
            }
            case "dislike":
            {
                $amount = $req->input("amount");
                if ($amount < 100 || $amount > 30000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 100 và tối đa 30000",
                    ], 200);
                }
                $result = $this->BuffDisLikeYoutube($req);
                break;
            }
            case "share":
            {
                $amount = $req->input("amount");
                if ($amount < 50 || $amount > 100000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 50 và tối đa 100000",
                    ], 200);
                }
                $result = $this->BuffShareYoutube($req);
                break;
            }
            case "commentytb":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->BuffCommentYoutube($req);
                break;
            }
        }

        if ($result['result']->success == 1) {
            $moreConfigs['result'] = $result['result'];

            $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                $data["note"] ?? null, null, null, $moreConfigs);
        }

        return response()->json($result);
    }

    public function BuffSubYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        $result = $this->youtube->buffSub($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function BuffGioXemYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        //$this->sendVoice('Thông báo, có đơn hàng mới');
        $this->youtube->buffTimeView($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function BuffViewYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount") - ($req->input("amount") / 100 * 10);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->youtube->buffView($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function BuffLikeYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->youtube->buffLike($id, $amount);

        return ["result" => $result, "data" => $data];
    }

    public function BuffDisLikeYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->youtube->buffDislike($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function BuffCommentYoutube(Request $req)
    {
        $note = str_replace("\n", "<br/>", $req->input("note"));
        $id = $req->input("id");
        $amount = $req->input("amount");
        $comment = $req->input("comment");

        $data = [
            "server" => 3,
            "amount" => $req->input("amount"),
            "comments" => $req->input("comment"),
            "gender" => 'all',
            "cmt_delay_time" => '2',
            "set_price" => '500',
            "id" => $req->input("id"),
        ];

        $comment = str_replace("\n", "<br/>", $req->input("comment"));
        $data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Số Lượng:</strong> $amount<br/><strong>Nội dung:</strong>$comment<br/>";

        $result = $this->youtube->buffComment($id, $req->input("comment"));
        return [
            "result" => $result,
            "data" => $data
        ];
    }

    public function BuffShareYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        //$this->sendVoice('Thông báo, có đơn hàng mới');
        return ["result" => (object)["success" => 1, "message" => "Thêm buff dislike thành công !"], "data" => $data];
    }

    public function BuffViewNhanhYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        $result = $this->youtube->worldView($id, $amount);
        return [
            "result" => $result,
            "data" => $data
        ];
    }

    public function Twitter(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }
        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng !"
            ], 200);
        }
        $service_id = $req->input("service_id");

        switch ($feature) {
            case "follow":
            {

                $amount = $req->input("amount");
                if ($amount < 100 || $amount > 100000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 100 và tối đa 100000",
                    ], 200);
                }
                $result = $this->BuffFollow($req);
                break;
            }
            case "like":
            {
                $amount = $req->input("amount");
                if ($amount < 50 || $amount > 100000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 50 và tối đa 100000",
                    ], 200);
                }
                $result = $this->BuffLike($req);
                break;
            }
        }

        if ($result['result']->success == 1) {
            $moreConfigs['result'] = $result['result'];

            $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                $data["note"] ?? null, null, null, $moreConfigs);
        }

        return response()->json($result);
    }

    public function BuffFollow(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->twitter->buffFollow($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function BuffLike(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->twitter->buffLike($id, $amount);
        return ["result" => $result, "data" => $data];
    }

    public function Google(Request $req, $feature)
    {
        $service_id = $req->input("service_id");
        $amount = $req->input("amount");
        $user = Auth::user();
        $target = $user->id . "_" . time();
        $newOrder = $this->CreateOrder($service_id, $target, $amount);

        switch ($feature) {
            case "gmail":
                $data = Resources::buy($amount, "gmail", $newOrder->id, $user);
                if ($data === "") {
                    $newOrder->status = Constant::STATUS_ORDER_INCOMPLETE;
                } else {
                    $newOrder->status = Constant::STATUS_ORDER_COMPLETED;
                    $newOrder->note = $data;
                }
                $newOrder->save();
                break;
        }

        $successMessage = $data === "" ? "Chúng tôi đang xử lý đơn hàng của bạn. Bạn sẽ  nhận được account qua phần note" : "Thành công";

        return response()->json([
            "success" => 1,
            "message" => $successMessage
        ]);
    }

    public function Shopee(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng ! "
            ], 200);
        }

        $result = null;
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "sub":
            {
                $amount = $req->input("amount");
                if ($amount < 500 || $amount > 100000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 500 và tối đa 100000",
                    ], 200);
                }
                $result = $this->BuffSubShopee($req);
                break;
            }
            case "like":
            {
                $amount = $req->input("amount");
                if ($amount < 100 || $amount > 10000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 100 và tối đa 10000",
                    ], 200);
                }
                $result = $this->BuffLikeShopee($req);
                break;
            }
        }


        if ($result["result"]->success == 1) {
            $data = $result['data'];
            $moreConfigs['result'] = $result['result'];

            $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"), $data["note"] ?? null, null, null,
                $moreConfigs);
        }
        return response()->json($result);
    }

    public function BuffSubShopee(Request $req)
    {
        $data = [
            "link" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel" => 1
        ];

        $result = $this->shopee->buffSub($data['link'], $data['amount']);
        //$result = $this->autoFBPro->buySubShopee($data['id'], $data['amount']);
        //return [
        //    "result" => $result,
        //    "data" => $data
        //];
        return ["result" => $result, "data" => $data];
    }

    public function BuffLikeShopee(Request $req)
    {
        $data = [
            "link" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel" => 1
        ];
        $result = $this->shopee->buffLike($data['link'], $data['amount']);
        return ["result" => $result, "data" => $data];
    }


    public function Telegram(Request $req, $feature)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Độ dài ID ko được lớn hơn 200 kỹ tự ! "
            ], 200);
        }

        $result = null;
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "membergroup":
            {

                $amount = $req->input("amount");
                if ($amount < 1000 || $amount > 500000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 1000 và tối đa 500000",
                    ], 200);
                }
                $result = $this->BuffMemTelegram($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];

                    $moreConfigs['result'] = $result['result'];

                    $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"), $data["note"] ?? null,
                        null, null, $moreConfigs);
                }
                return response()->json($result['result']);
                break;
            }
        }
        if ($result->success == 1) {
            $this->CreateOrder($service_id, $data['link'] ?? $data['link_or_id'], $data['amount']);
        }
        return response()->json($result);
    }

    public function BuffMemTelegram(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];

        $result = $this->telegram->buffMember($id, $amount);
        return ["result" => $result, "data" => $data];
    }


    public function likePageSale($id, $amount = 10)
    {
        $result = $this->traoDoiCheo->buyPageLike($id, $amount);

        return [
            "result" => $result,
            "data" => [
                "note" => "Like free"
            ]
        ];
    }

    public function buffFollowSale($id, $amount)
    {
        $result = $this->traoDoiCheo->buyFollow($id, $amount);
        return [
            "result" => $result,
            "data" => [
                "note" => "Like free"
            ]
        ];
    }


    public function buyLikeFree($id, $amount)
    {
        $result = $this->traoDoiSub->buyLike($id, $amount);
        return [
            "result" => $result,
            "data" => [
                "note" => "Like free"
            ]
        ];
    }
    // {

    // $data = [
    // "reaction" => 'LIKE',
    // "amount" => 20,
    // "id" => $id,
    // "force_buff" => 0,
    // "channel_number" => 0,
    // "buff_speed" => 'false',
    // "buff_speed_second" => 5
    // ];

    // return [
    // "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=covid_likepost3&site=add_new", $data),
    // "data" => $data
    // ];
    // }
    public function Flashsale(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập đúng định dạng ! "
            ], 200);
        }
        if ($feature === 'likepost') {
            $this->traoDoiSub = new TraoDoiSub(true);
            $this->traoDoiSub->login();
        }


        $checkLogs = FlashSale::checkLogs($req->input("id"), 'fb-like', $req->ip());

        if ($checkLogs->success === false) {
            return response()->json($checkLogs);
        }

        $service_id = $req->input("service_id");
        switch ($feature) {
            case "likepost":
            {
                $amount = $req->input("amount");
                if ($amount < 50 || $amount > 50) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Miễn Phí Chỉ Hỗ Trợ Số Lượng 50 Like",
                    ], 200);
                }
                $result = $this->buyLikeFree($req->input("id"), 30);

                if ($result["result"]->success == 1) {
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());
                    $data = $result['data'];
                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);
                }
                return response()->json($result['result']);

                break;
            }
        }

        $service_id = $req->input("service_id");
        switch ($feature) {
            case "sub":
            {
                $amount = $req->input("amount");
                if ($amount > 10 || $amount === 0) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối đa 10",
                    ], 200);
                }
                $result = $this->buffFollowSale($req->input("id"), $req->input("amount"));

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());
                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "likepage":
            {
                $amount = $req->input("amount");
                if ($amount < 10 || $amount > 5000000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 10 và tối đa 5000000",
                    ], 200);
                }
                $result = $this->likePageSale($req->input("id"), $req->input("amount"));

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());
                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "liketiktok":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->LikeTiktok($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "subtiktok":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->SubTiktok($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];

                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "viewtiktok":
            {
                $amount = $req->input("amount");
                if ($amount < 1000 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Dịch vụ miễn phí chỉ hỗ trợ số lượng 1000",
                    ], 200);
                }
                $result = $this->ViewTiktok($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];

                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());


                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "likeyoutube":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->LikeYoutube($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "subyoutube":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->SubYoutube($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }
        $service_id = $req->input("service_id");
        switch ($feature) {
            case "viewyoutube":
            {
                $amount = $req->input("amount");
                if ($amount < 20 || $amount > 1000) {
                    return response()->json([
                        "success" => 0,
                        "message" => "Số lượng tối thiểu 20 và tối đa 1000",
                    ], 200);
                }
                $result = $this->ViewYoutube($req);

                if ($result["result"]->success == 1) {
                    $data = $result['data'];
                    FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

                    $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"),
                        $data["note"] ?? null);

                    if ($isAPI) {
                        $order->isAPI = true;
                        $order->save();
                    }
                    $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

                    $result["result"]->order_id = $order->id;
                }
                return response()->json($result['result']);
                break;
            }
        }

        if ($result["result"]->success == 1) {
            $data = $result['data'];
            $order = null;

            FlashSale::insertLogs($req->input("id"), 'fb-like', $req->ip());

            $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("amount"), $data["note"] ?? null);


            if ($isAPI) {
                $order->isAPI = true;
                $order->save();
            }
            $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

            $result["result"]->order_id = $order->id;
        }
        return response()->json($result);
    }

    public function LikePostSale(Request $req)
    {

        $id = $req->input("id");

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => 20,
            "id" => $id,
            "force_buff" => '1',
        ];

        FlashSale::insertLogs($id, "fb-like", $req->ip());


        $result = FlashSale::checkLogs($id, $req->ip(), "fb-like");

        $buyReaction = (object)(
        [
            "success" => 0,
            "message" => "Fail"
        ]
        );


        //        if ($data['reaction'] === 'LIKE') {
        //            $buyReaction = $this->traoDoiSub->buyLike($data['id'], $data['amount']);
        //        } else {
        //            $buyReaction = $this->traoDoiSub->buyReaction($data['id'], $data['amount'], $data['reaction']);
        //        }


        return ["result" => $buyReaction, "data" => $data];
    }

    public function Sub(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function LikePage(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function LikeTiktok(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function SubTiktok(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function ViewTiktok(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        $api = new ApiBL();

        $order = $api->order(array('service' => 4352, 'link' => $id, 'quantity' => 1000)); # Default
        return ["result" => (object)["success" => 1, "message" => "Thêm buff view thành công !"], "data" => $data];
    }

    public function LikeYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function SubYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function ViewYoutube(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function Other(Request $req, $feature, $isAPI = false)
    {
        // 	    $comment = $req->input("comment");
        // 		$list = $req->input("id");
        // 	    $DSso = explode("\n", $list);
        // 		$soluong = count($DSso);


        // 		$x = strpos ($DSso[0],"|");
        //         $sdt = substr($DSso[0],0,10);
        //         $ten = substr($DSso[0],$x+2);
        //         $noidung = str_replace("{name}",$ten,$comment);
        //         $sokytu = strlen($noidung);


        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "Vui lòng nhập ID",
            ], 200);
        }


        $this->LoginVietnamFB();

        $sdt = $req->input("sdt");
        if ($req->input("sdt") != 8481888888 && $req->input("sdt") != 8477779999 && $req->input("sdt") != 8489999999) {
            return response()->json([
                "success" => 0,
                "message" => "Nghiêm cấm bug, Sẽ khóa tài khoản nếu tiếp tục",
            ], 200);
        }

        $amount = $req->input("amount");
        if ($req->input("amount") > 255) {
            return response()->json([
                "success" => 0,
                "message" => "Nghiêm cấm bug, Sẽ khóa tài khoản nếu tiếp tục",
            ], 200);
        }

        $service_id = $req->input("service_id");

        switch ($feature) {
            case "voice":
            {
                $result = $this->Voices($req);
                return response()->json($result['result']);
                break;
            }
        }

        return response()->json($result);
    }

    public function Voices(Request $req)
    {
        $note = str_replace("\n", "<br/>", $req->input("note"));
        $comment = str_replace("\n", "<br/>", $req->input("comment"));
        $id = $req->input("id");
        $gender = $req->input("gender");
        $sdt = $req->input("sdt");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
        ];
        $service_id = $req->input("service_id");

        $DSso = explode("\n", $id);
        $soluong = count($DSso);


        for ($i = 0; $i < $soluong; $i++) {
            $sokytu = strlen($comment);
            if ($sokytu <= 125) {
                $sotinnhan = 125;
            } else {
                if ($sokytu > 125 && $sokytu <= 250) {
                    $sotinnhan = 250;
                }
            }
            $this->sendVoicee($DSso[$i], $comment, $req->input("gender"), $req->input("sdt"));
            $data["note"] = "<strong>Post ID</strong>: $DSso[$i]<br/><strong>Giới tính:</strong> $gender<br/><strong>Số comments:</strong> $sokytu <br/><strong>Nội dung:</strong><br/>$comment<br/><strong>Note:</strong><br/>$note";
            $this->CreateOrder($service_id, $DSso[$i], $sotinnhan, $data["note"] ?? null);
        }

        //         $this->sendVoicee($req->input("id"), $req->input("comment"),$req->input("gender"),$req->input("sdt"));
        // 		$data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Giới tính:</strong> $gender<br/><strong>Số comments:</strong> $amount<br/><strong>Nội dung:</strong><br/>$comment<br/><strong>Note:</strong><br/>$note";
        return ["result" => (object)["success" => 1, "message" => "Thực hiện cuộc gọi thành công!"], "data" => $data];
    }


    // 	public function Voices(Request $req)
    // 	{
    // 	    $note = str_replace("\n", "<br/>", $req->input("note"));
    // 		$comment = str_replace("\n", "<br/>", $req->input("comment"));
    // 		$id = $req->input("id");
    // 		$gender = $req->input("gender");
    // 		$sdt = $req->input("sdt");
    // 		$price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
    // 		$data = [
    // 			"id" => $req->input("id"),
    // 		];
    // 		$service_id = $req->input("service_id");

    // 	    $list = $req->input("id");
    // 	    $DSso = explode("\n", $list);
    // 		$soluong = count($DSso);

    // 	    for ($i=0; $i<$soluong; $i++)
    // 	    {
    // 	        $x = strpos ($DSso[$i],"|");
    //             $sdt = substr($DSso[$i],0,10);
    //             $ten = substr($DSso[$i],$x+1);
    //             $noidung = str_replace("{name}",$ten,$comment);
    //             $sokytu = strlen($noidung);

    //             if($sokytu<=125)
    //             {
    //                 $sotinnhan = 125;
    //             }else if ($sokytu >125 && $sokytu <=250)
    //             {
    //                 $sotinnhan = 250;
    //             }

    //   	        $this->sendVoicee($sdt, $noidung, $req->input("gender"), $req->input("sdt"));
    // 		    $data["note"] = "<strong>Post ID</strong>: $sdt<br/><strong>Giới tính:</strong> $gender<br/><strong>Số comments:</strong> $sokytu <br/><strong>Nội dung:</strong><br/>$noidung<br/><strong>Note:</strong><br/>$note";
    // 		    $this->CreateOrder($service_id, $DSso[$i],$sotinnhan, $data["note"] ?? null);
    // 	    }

    // //         $this->sendVoicee($req->input("id"), $req->input("comment"),$req->input("gender"),$req->input("sdt"));
    // // 		$data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Giới tính:</strong> $gender<br/><strong>Số comments:</strong> $amount<br/><strong>Nội dung:</strong><br/>$comment<br/><strong>Note:</strong><br/>$note";
    // 		return ["result" => (object)["success" => 1, "message" => "Thực hiện cuộc gọi thành công!"], "data" => $data];
    // 	}
    public function FacebookVip(Request $req, $feature, $isAPI = false)
    {
        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID và Gói không được để trống",
            ], 200);
        }

        $result = null;

        $duration = null;

        $service_id = $req->input("service_id");

        switch ($feature) {
            case "like":
            {
                $result = $this->FacebookVipLike($req);
                break;
            }
            case "mat":
            {
                $result = $this->FacebookVipMat($req);
                break;
            }
            case "comment":
            {
                $result = $this->FacebookVipComment($req);
                break;
            }
        }
        if ($result["result"]->success == 1) {
            $data = $result["data"];

            $duration = $data["duration"];
            $total = $data["total"];
            if ($total <= 0) {
                $result["result"] = (object)[
                    "success" => 0,
                    "message" => "Sẽ khóa tài khoản vĩnh viễn nếu tiếp tục tìm cách Bug hệ thống !"
                ];
                return response()->json($result['result']);
            }

            $moreConfigs = [];
            if (isset($result['result']->order_id)) {
                $moreConfigs['api_id'] = $result['result']->order_id;
            }

            $order = $this->CreateOrder($service_id, $req->input("id"), $req->input("package"), $data["note"] ?? null,
                $duration, $total ?? null, $moreConfigs);
            $result["result"]->rate = $this->serviceModel->GetPrice($service_id, Auth::user()->role);

            $result["result"]->order_id = $order->id;
        }
        //$this->sendVoice('Thông báo có đơn hàng Vip');
        return response()->json($result['result']);
    }


    public function FacebookVipDeleteOrder(Request $req)
    {
        $user = Auth::user();
        $orderId = (int)$req->input('orderId');
        $order = $this->model->where('id', $orderId)->where('user_id', $user->id)->first();
        if ($order) {
            $facebook = new Facebook($this->token);
            $result = $facebook->deleteOrderVip($order->api_id);

            if (!$result->success) {
                return response()->json($result);
            }

            $order->delete();
            return response()->json([
                "status" => 1,
                "message" => "Đã xóa thành công đơn hàng !!"
            ]);
        }
        return response()->json([
            "status" => 0,
            "message" => "Đơn hàng ko tồn "
        ], 402);
    }

    public function FacebookVipCommentEdit(Request $req)
    {
        $user = Auth::user();
        $orderId = $req->input('orderId');
        $order = $this->model->where('id', $orderId)->where('user_id', $user->id)->first();
        $comment = str_replace("\n", "<br/>", $req->input("comment"));

        if (isset($order)) {
            $facebook = new Facebook($this->token);

            $result = $facebook->editVipComment($order->api_id, $req->input("comment"));

            if (isset($result->success) && $result->success) {
                $order->note .= '<br /> <br />------------' . date('H:m - d/m/Y') . '--------------<br /> <b>Cập nhật nội dung comment: </b> <br /> ' . $comment;
                $order->status = Constant::STATUS_ORDER_INCOMPLETE;
                $order->save();
                return response()->json([
                    'status' => 1,
                    "message" => "Cập nhật nội dung comment thành công. Bạn sẽ nhận được thông báo khi đơn được duyệt hoàn tất."
                ]);
            } else {
                return response()->json([
                    "status" => 0,
                    "message" => "Server hiện đang bận. Vui lòng thử lại sau."
                ], 402);
            }
        } else {
            return response()->json([
                "status" => 0,
                "message" => "Hiện ko tìm thấy đơn hàng này."
            ], 402);
        }
    }

    public function FacebookVipGetPrice(Request $req)
    {
        $user = Auth::user();
        $orderId = $req->input('reOrderId');
        $period = $req->input('period');

        $order = $this->model->get($orderId);

        if (isset($order)) {
            $price = $this->serviceModel->GetPrice($order->service_id, $user->role);
            $total = $order->quantity * $period * ($price / 30);
            return response()->json([
                "success" => 1,
                "total" => $total,
            ]);
        }
        return response()->json([
            "success" => 0,
        ], 402);
    }

    public function FacebookVipReOrder(Request $req)
    {
        $user = Auth::user();

        $orderId = $req->input('reOrderId');
        $period = (int)$req->input('period');

        if ($period !== 30 && $period !== 60 && $period !== 90) {
            return response()->json([
                "success" => 0,
                "message" => "12 se bi khoa tai khoan :(  "
            ], 402);
        }

        $order = $this->model->get($orderId);

        if (isset($order)) {
            $price = $this->serviceModel->GetPrice($order->service_id, Auth::user()->role);
            $total = $order->quantity * $period * ($price / 30);
            if ($total > Auth::user()->balance) {
                return response()->json([
                    "success" => 0,
                    "message" => "Số dư không đủ",
                ], 402);
            }

            $facebook = new Facebook($this->token);
            $result = $facebook->reOrderVip($order->api_id, $period);

            if (!$result->success) {
                return response()->json($result);
            }

            try {
                $user->balance -= (int)$total;
                $user->save();

                $transaction = new Transaction;
                $transaction->Create($user->id, 1, -$total, 'Gia hạn dịch vụ');

                $isExpired = $order->status === 1;
                if ($isExpired) {
                    $order->created_at = strtotime('now');
                } else {
                    $order->created_at = strtotime($order->created_at) + $order->duration;
                }
                $order->duration = $period * 24 * 3600;
                $order->status = 0;
                $order->save();
                return response()->json(["success" => 1, "message" => "Đã gia hạn thành công !! "]);
            } catch (Exception $err) {
                return response()->json(["success" => 0, "message" => "Ko tìm thấy đơn hàng !"], 402);
            }

        } else {
            return response()->json(["success" => 0, "message" => "Ko tìm thấy đơn hàng !"], 402);
        }
    }

    public function FacebookVipLike(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("package");
        $package = $req->input("package");
        $period = $req->input("period");
        $posts = $req->input("posts");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
            "duration" => $period * 24 * 3600,
            "total" => $package * $period * ($price / 30),
        ];

        $data["note"] = json_encode((object)[
            "period" => $period,
            "posts" => $posts,
            "lastId" => null,
            "remaining" => $posts
        ]);

        $facebook = new Facebook($this->token);
        $result = ($facebook->vipLike($id, $package, $period));

        return [
            "result" => $result,
            "data" => $data
        ];
    }

    public function FacebookVipComment(Request $req)
    {
        $note = str_replace("\n", "<br/>", $req->input("note"));
        $id = $req->input("id");
        $gender = $req->input("gender");
        $amount = $req->input("amount");
        $package = $req->input("package");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $period = $req->input("period");
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "duration" => $period * 24 * 3600,
            "total" => $package * $period * ($price / 30),
        ];

        $facebook = new Facebook($this->token);
        $result = $facebook->vipComment($id, $package, $period, $gender, $req->input("note"));

        $comment = str_replace("\n", "<br/>", $req->input("comment"));
        $data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Giới tính:</strong> $gender<br/><strong>Gói VIP:</strong> $package comments/tháng<br/><strong>Thời hạn</strong>: $period tháng</br><strong>Nội dung:</strong><br/>$comment<br/><strong>Note:</strong><br/>$note";

        return ["result" => $result, "data" => $data];
    }

    public function FacebookVipMat(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("package");
        $package = $req->input("package");
        $period = $req->input("period");
        $posts = $req->input("posts");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
            "duration" => $period * 24 * 3600,
            "total" => $package * $period * ($price / 30),
        ];
        $comment = str_replace("\n", "<br/>", $req->input("comment"));

        $data["note"] = json_encode((object)[
            "period" => $period,
            "posts" => $posts,
            "lastId" => null,
            "remaining" => $posts
        ]);

        return ["result" => (object)["success" => 1, "message" => "Thêm vip like thành công !"], "data" => $data];
    }

    public function Info($token)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 0,
            CURLOPT_URL => 'https://graph.facebook.com/me?access_token=' . $token,
            CURLOPT_SSL_VERIFYPEER => false
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $resp = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $info = json_decode($resp, true);
        return [
            "httpcode" => $httpcode,
            "data" => $info
        ];
    }

    public function checkToken(Request $request)
    {
        $response = $this->Info($request->input('access_token'));
        return response()->json($response['data'], $response['httpcode']);
    }

    public function removeDieToken()
    {
        $removedCount = 0;
        $tokens = DB::table("tokens")->where('isExpired', "=", 0)->get();

        foreach ($tokens as $key => $token) {
            $response = $this->Info($token->token);
            if ($response['httpcode'] !== 200) {
                DB::table("tokens")->delete($token->id);
                $removedCount++;
            }
        }
        echo "<h1> Đã xóa $removedCount tokens còn lại  tokens</h1>";
    }

    public function BuffLikeVIP()
    {
        $currentTime = time();
        $fifteenMinutes = 60 * 20;
        $orders = $this->model->where('status', 0)->get();
        $this->model->CheckCompleted($orders);
        $orders = $this->model->where('status', 2)->where('quantity', '<=', 50);
        $orders = $orders->where('service_id', 15)->where('viplike_status', '=', 1)->where('lastScanAt', '<=',
            $currentTime)->inRandomOrder()->limit(1)->get();
        $orderController = new OrderController;
        foreach ($orders as $order) {
            $response = json_decode($orderController->GetFacebookPost($order->target));
            sleep(5);
            if (isset($response->error)) {
                echo "<h1 style='color: rgba(255,0,21,0.67)'>Token Error</h1>";
                var_dump($response->error);
                continue;
            }

            $result = $response->data;


            $info = json_decode($order->note);

            // check time to reset remaining

            $lastPost = $result[0];

            $lastDate = new Datetime($lastPost->created_time);
            $now = new DateTime();

            if ($lastDate->format('Y-m-d') == $now->format('Y-m-d')) {
                $info->remaining = $info->posts;
                $order->note = json_encode($info);
                $order->save();
            }

            $lastLink = $result[0]->actions[0]->link;

            $regex = '/(?:https?:\/\/(?:www|m|mbasic|business)\.(?:facebook|fb)\.com\/)(?:photo(?:\.php|s)|permalink\.php|video\.php|media|watch\/|questions|notes|[^\/]+\/(?:activity|posts|videos|photos))[\/?](?:fbid=|story_fbid=|id=|b=|v=|)(?|([0-9]+)|[^\/]+\/(\d+))/mi';
            preg_match_all($regex, $lastLink, $matches);

            $lastId = $matches[1][0];

            if (!isset($info) || $info->lastId == $lastId) {
                continue;
            }

            foreach ($result as $post) {
                if ($info->remaining <= 0) {
                    break;
                }
                $postDate = new Datetime($post->created_time);
                $now = new DateTime();
                if ($postDate->format('Y-m-d') == $now->format('Y-m-d')) {
                    $postLink = $post->actions[0]->link;
                    preg_match_all($regex, $postLink, $matches);

                    $postId = $matches[1][0];

                    if ($info->lastId == $postId) {
                        break;
                    }

                    $logCount = IdLogs::getCount($postId);
                    if ($logCount > 0) {
                        break;
                    }

                    $data = [
                        "post_id" => $postId,
                        "qty" => 50,
                    ];

                    $result = $this->traoDoiCheo->buyLike($data['post_id'], $data['qty']);
                    if ($result->status) {
                        echo "<h1>Liked $order->quantity likes for $postId </h1>";
                    } else {
                        echo '<h1 style="color: red;">Có lỗi xảy ra với TraoDoiCheo.</h1>';
                    }

                    // Save logs
                    $idLog = new IdLogs();
                    $idLog->idPost = $postId;
                    $idLog->save();

                    $info->remaining -= 1;
                }
            }

            $info->lastId = $lastId;
            $order->lastScanAt = $currentTime + $fifteenMinutes;
            $order->note = json_encode($info);
            $order->save();
        }
    }


    public function cronVipLike(Request $req)
    {
        $this->BuffLikeVIP();
    }

    // FacebookBuff Methods

    public function FacebookBuff(Request $req, $feature, $isAPI = false)
    {

        $facebook = new Facebook($this->token);
        $serviceId = $req->input('service_id');

        if ($req->input("id") == null) {
            return response()->json([
                "success" => 0,
                "message" => "ID không được để trống",
            ], 200);
        }

        if (strlen($req->input("id")) > 200) {
            return response()->json([
                "success" => 0,
                "message" => "Độ dài ID ko được lớn hơn 200 kỹ tự !"
            ], 200);
        }

        $id = $req->input("id");
        $amount = $req->input("amount");
        $reaction = $req->input("reaction");
        $content = $req->input('comment');
        $note = $req->input("note");

        $result = [];

        switch ($feature) {
            case 'likepost':
                $result = $facebook->likePost($id, $amount, strtoupper($reaction));
                break;
            case 'likepost2':
                $result = $facebook->likePost2($id, $amount, strtoupper($reaction));
                break;
            case 'commentfb':
                $content = $req->input('comment');
                $result = $facebook->buffComment($id, $content);
                break;
            case 'comment':
                $content = $req->input('comment');
                $result = $facebook->buffComment2($id, $content);
                break;
            case "likecomment":
                $result = $facebook->likeComment($id, $amount, strtoupper($reaction));
                break;
            case "likepage2":
                $result = $facebook->likePage2($id, $amount);
                break;
            case "likepageglobal":
                $result = $facebook->likePageGlobal($id, $amount);
                break;
            case 'sub':
                $result = $facebook->follow($id, $amount);
                break;
            case "buffsubnhanh":
                $result = $facebook->fastFollow($id, $amount);
                break;
            case "tangbanbe":
                $result = $facebook->buffFriend($id, $amount);
                break;
            case "live":
                $min = $req->input("min");
                $result = $facebook->buffLive($id, $amount, $min);
                break;
            case "live2":
                $min = $req->input("min");
                $result = $facebook->buffLive2($id, $amount, $min);
                break;
            case "likepage":
                $result = $facebook->likePage($id, $amount);
                break;
            case "view":
                $result = $facebook->buffView($id, $amount);
                break;
            case "reviewpage":
                $result = $facebook->reviewPage($id, $content);
                break;
            case "sharelive":
                $result = $facebook->shareLive($id, $amount);
                break;
            case "sharewall":
                $result = $facebook->shareWall($id, $amount);
                break;
            case "buffmember":
                $result = $facebook->buffMember($id, $amount);
                break;
            case "viplike":
                $package = $req->input('package');
                $period = $req->input('period');

                $result = $facebook->vipLike($id, $package, $period);
                break;
            default:
                var_dump($feature);
                break;
        }

        //        $result = (object)["success" => 1, "message" => "Success", "order_id" => 123123];

        if (isset($result->success) && $result->success === 1) {
            $price = $this->serviceModel->GetPrice($serviceId, Auth::user()->role);

            $moreConfigs['result'] = $result;
            $this->CreateOrder($serviceId, $id, $amount,
                $note ?? null, null, $price * $amount, $moreConfigs);
        }

        return response()->json($result);

    }

    public function BuffShareWall(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel_number" => 2,
            "buff_speed" => "slow",
        ];
        $buyShare = $this->traoDoiSub->buyShare($data['id'], $data['amount']);
        return ["result" => $buyShare, "data" => $data];
    }

    public function view3s(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        return ["result" => (object)["success" => 1, "message" => "Thêm buff like thành công !"], "data" => $data];
    }

    public function BuffSubNgon(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        return ["result" => (object)["success" => 1, "message" => "Thêm buff dislike thành công !"], "data" => $data];
    }

    public function BuffSub(Request $req)
    {

        $data = [
            "channel_number" => 2,
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),

        ];

        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=buffsubdexuat&site=add_new", $data),
            "data" => $data
        ];
    }

    public function BuffBanBe(Request $req)
    {

        $data = [
            "channel_number" => 1,
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "buff_speed" => 'false',
            "force_buff" => '1',

        ];

        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=covid_sub&site=add_new", $data),
            "data" => $data
        ];
    }

    public function BuffCommentVnfb(Request $req)
    {
        $note = str_replace("\n", "<br/>", $req->input("note"));
        $id = $req->input("id");
        $amount = $req->input("amount");
        $comment = $req->input("comment");

        $data = [
            "server" => 3,
            "amount" => $req->input("amount"),
            "comments" => $req->input("comment"),
            "gender" => 'all',
            "cmt_delay_time" => '2',
            "set_price" => '500',
            "id" => $req->input("id"),
        ];

        $comment = str_replace("\n", "<br/>", $req->input("comment"));
        $data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Số Lượng:</strong> $amount<br/><strong>Nội dung:</strong>$comment<br/>";
        $result = $this->autoFBPro->buyComment($data['id'], explode("\n", $data['comments']), $data['gender']);

        return ["result" => $result, "data" => $data];
    }

    public function BuffLikeVnfb(Request $req)
    {

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "channel_number" => '0',
            "buff_speed" => 'false',
            "force_buff" => '1',

        ];

        $reaction = strtoupper($req->input('reaction'));
        //        $result = $this->tuongTacCheo->buyR($data['id'], $data['amount'], $reaction);
        $amount = (int)$req->input('amount');
        if ($amount >= 500) {
            $amount = 500;
        }

        if ($reaction === 'LIKE') {
            $result = $this->tuongTacCheo->buyLike($data['id'], $amount);
        } else {
            $result = $this->tuongTacCheo->buyReaction($data['id'], $amount, $reaction);
        }
        return [
            "result" => $result,
            "data" => $data
        ];

        $data["note"] = $req->input("reaction");
        return ["result" => (object)["success" => 1, "message" => "Thêm buff like thành công !"], "data" => $data];

    }

    public function BuffLikeComment(Request $req)
    {

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "channel_number" => '0',
            "buff_speed" => 'false',
            "force_buff" => '1',

        ];

        $reaction = strtolower($req->input('reaction'));
        $result = $this->autoFBPro->buyLikeComment($data['id'], $data['amount'], $data['reaction']);
        return [
            "result" => $result,
            "data" => $data
        ];
    }

    public function BuffLikeVnfb2(Request $req)
    {

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "force_buff" => '1',
        ];

        if ($data['reaction'] === 'LIKE') {
            $buyReaction = $this->traoDoiSub->buyLike($data['id'], $data['amount']);
        } else {
            $buyReaction = $this->traoDoiSub->buyReaction($data['id'], $data['amount'], $data['reaction']);
        }

        return ["result" => $buyReaction, "data" => $data];
    }

    public function BuffLikeVnfb3(Request $req)
    {

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "channel_number" => '0',
            "buff_speed" => 'false',
            "force_buff" => '1',

        ];
        return ["result" => (object)["success" => 1, "message" => "Thêm buff follow thành công !"], "data" => $data];
        // $reaction = strtolower($req->input('reaction'));
        // $result = $this->autoFBPro->buyLike3($data['id'], $data['amount'], $data['reaction']);
        // return [
        //     "result" => $result,
        //     "data" => $data
        // ];
    }

    public function BuffLikePost(Request $req)
    {

        $data = [
            "reaction" => $req->input("reaction"),
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "force_buff" => '1',

        ];

        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=covid_likepost3&site=add_new", $data),
            "data" => $data
        ];
    }

    public function LocBanBe(Request $req)
    {

        $data = [
            "id" => $req->input("id"),
        ];

        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=poke_filter_friends&site=create", $data),
            "data" => $data
        ];
    }

    public function BuffMember(Request $req)
    {

        $data = [
            "type" => 'no_avatar',
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
        ];

        //return [
        //    "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=group_member&site=create", $data),
        //    "data" => $data
        //];
        return ["result" => (object)["success" => 1, "message" => "Thêm buff mem thành công !"], "data" => $data];

    }

    public function BuffView(Request $req)
    {

        $data = [
            "slmat" => $req->input("amount"),
            "link_or_id" => $req->input("id"),
            "server" => '10',
            "mins" => '30',
            "speed" => 0
        ];
        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=buffview2&site=create", $data),
            "data" => $data
        ];
    }

    public function BuffLikePage(Request $req)
    {
        $data = [
            "amount" => $req->input("amount"),
            "id" => $req->input("id"),
            "channel" => 1,
        ];
        //$result = $this->autoFBPro->buyLikePage($data['id'], $data['amount']);
        //return [
        //    "result" => $result,
        //    "data" => $data
        //];
        // return [
        //     "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=like_page2&site=add_new", $data),
        //     "data" => $data
        // ];
        return ["result" => (object)["success" => 1, "message" => "Thêm buff like thành công !"], "data" => $data];
    }

    public function BuffLikePageGlobal(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $price = $this->serviceModel->GetPrice($req->input("service_id"), Auth::user()->role);
        $data = [
            "id" => $req->input("id"),
            "amount" => $amount,
        ];
        $api = new ApiBL();

        $order = $api->order(array('service' => 4441, 'link' => $id, 'quantity' => $amount)); # Default
        return ["result" => (object)["success" => 1, "message" => "Thêm buff like thành công !"], "data" => $data];
    }

    public function ReviewPage(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "comments" => $req->input("comment"),
            "channel_number" => 2
        ];
        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=fanpage_review&site=add_new", $data),
            "data" => $data
        ];
    }

    public function BuffLive(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel_number" => 2,
            "mins" => $req->input("min"),
            "force_buff" => '1',
        ];
        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=bufflive&site=buff_live", $data),
            "data" => $data
        ];
    }

    public function BuffLive2(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel_number" => 5,
            "mins" => $req->input("min"),
            "force_buff" => '1',
        ];
        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=bufflive&site=buff_live", $data),
            "data" => $data
        ];
    }

    public function BuffShareLive(Request $req)
    {
        $data = [
            "id" => $req->input("id"),
            "link_or_id" => $req->input("id"),
            "amount" => $req->input("amount"),
            "channel_number" => 1,
            "buff_speed" => "slow"
        ];
        return [
            "result" => $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=sharelive&site=addnew", $data),
            "data" => $data
        ];
    }

    public function BuffComment(Request $req)
    {
        $vipLikeXyz = new VipLikeXyz();
        $vipLikeXyz->login();

        $note = str_replace("\n", "<br/>", $req->input("note"));
        $id = $req->input("id");
        $amount = $req->input("amount");
        $delay = 1;
        $comment = $req->input("comment");

        $data = [
            "server" => 3,
            "amount" => $req->input("amount"),
            "comments" => $req->input("comment"),
            "gender" => 'all',
            "cmt_delay_time" => '2',
            "set_price" => '500',
            "id" => $req->input("id"),
        ];

        $comment = str_replace("\n", "<br/>", $req->input("comment"));
        $data["note"] = "<strong>Post ID</strong>: $id<br/><strong>Số Lượng:</strong> $amount<br/><strong>Nội dung:</strong>$comment<br/>";

        return [
            "result" => $vipLikeXyz->buyComment($id, $amount, $amount, $req->input("comment")),
            "data" => $data
        ];
    }


    public function BuffSubNhanh(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo đại ca có đơn hàng sub đề xuất');
        return [
            "result" => (object)["success" => 1, "message" => "Thêm buff sub đề xuất thành công !"],
            "data" => $data
        ];
    }

    public function LikePage2(Request $req)
    {
        $id = $req->input("id");
        $amount = $req->input("amount");
        $data = [
            "id" => $id,
            "amount" => $amount
        ];
        //$this->sendVoice('Thông báo có đơn hàng like page');
        return ["result" => (object)["success" => 1, "message" => "Thêm đơn thành công !"], "data" => $data];
    }

    public function LoginVietnamFB()
    {
        $cookies = Helper::HTTP_POST("https://vietnamfb.com/?mc=pub&site=postLogin",
            ["username" => "donghuuky", "password" => "dhkytv0812hi"], true);

        foreach ($cookies as $cookie) {
            if ($cookie["name"] === "PHPSESSID") {
                $this->PHPSESSID = $cookie['value'];
            }
            if ($cookie["name"] === "_ggwp") {
                $this->_ggwp = $cookie['value'];
            }
        }
    }

    public function GetProfile(Request $req)
    {
        try {
            $this->LoginVietnamFB();

            $result = $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=facebook_check&site=get_facebook_id",
                ["link_or_id" => $req->input("profile")]);
            $data["status"] = Constant::RESPONSE_STATUS_SUCCESS;

            $items["id"] = $result->fb_id;

            $result = $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=facebook_check&site=get_facebook_name",
                ["fb_id" => $items["id"]]);

            $items["name"] = $result->name;

            $data["items"] = $items;

            return response()->json($data);
        } catch (Exception $e) {
            return response()->json(['error' => true]);
        }
    }

    public function CheckID(Request $req)
    {
        $this->LoginVietnamFB();


        return $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=facebook_check&site=get_facebook_name",
            ["fb_id" => $req->input("id")]);
    }

    public function GetInstagramProfile(Request $req)
    {
        $this->LoginVietnamFB();

        $result = $this->PostRequestVietnamFB("https://vietnamfb.com/?mc=instagram&site=convert_link_to_id_front",
            ["link" => $req->input("profile")]);
        $data["status"] = Constant::RESPONSE_STATUS_SUCCESS;
        $items["id"] = $result->user_id;

        $data["items"] = $items;

        return response()->json($data);
    }

    public function getProfit($serviceId, $salePrice, $quantity)
    {
        $entryPrices = $this->profile->getPrices();
        $entryPrice = $entryPrices[$serviceId];

        if (isset($entryPrice)) {
            return $salePrice - ($entryPrice * $quantity);
        } else {
            return 0;
        }
    }

    public function CreateOrder(
        $service_id,
        $target,
        $quantity,
        $note = null,
        $duration = null,
        $_total = null,
        $moreConfigs = []
    ) {
        if ($_total) {
            $total = $_total;
        } else {
            $total = $this->serviceModel->GetPrice($service_id, Auth::user()->role) * $quantity;
        }
        if ($total > Auth::user()->balance) {
            /* TODO: Fix response() func doesnt't working.
            *  It's a fucking code :)) but i'm too lazy to find solution :) Sorry about this :(((
             */
            header('Content-Type: application/json');
            exit(json_encode((object)["success" => 0, "message" => "Số dư ko đủ !"]));

            return response()->json([
                "success" => 0,
                "message" => "Số dư không đủ",
            ], 200);
        }
        $moreConfigs['profit'] = $this->getProfit($service_id, $total, $quantity);
        return $this->model->Create($service_id, $target, $total, $quantity, $note, $duration, $moreConfigs);
    }

    public function PostRequestVietnamFB($url, $data, $isGetHeader = 0)
    {
        $cookie = "PHPSESSID=$this->PHPSESSID; __cfduid=deb64e55b90ba208ef347f884fbf964961598023948; _ggwp=$this->_ggwp;";
        return (object)Helper::HTTP_POST($url, $data, $isGetHeader, $cookie);
    }


    public function PostRequestTraoDoiCheo($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => "post_id=" . $data['post_id'] . "&qty=" . $data["qty"],
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    // $this->sendVoice('sdt sim', 'so nhan', 'message');
    public function sendVoice($message)
    {
        $client = new MessageBird\Client(env('MB_KEY'));

        $call = new MessageBird\Objects\Voice\Call;
        // $call->source = $row->sim->number;
        $call->source = str_replace('.', '', '84777799999');
        $call->destination = '84812888896'; // Số nhận cuộc gọi

        $flow = new MessageBird\Objects\Voice\CallFlow;
        $flow->title = 'Đăng Ký Thành Công';

        $step = new MessageBird\Objects\Voice\Step;
        $step->action = 'say';
        $step->options = [
            // 'payload' => 'Mã xác nhận của quý khách là . 5 . 3 . 8 . 8 . 9 . xin nhắc lại, 5 . 3 . 8 . 8 . 9', // nội dung cuộc gọi thoại
            'payload' => $message,
            'language' => 'vi-VN', // ngôn ngữ
            'voice' => 'male'
        ];
        $flow->steps = [$step];
        $call->callFlow = $flow;
        return $client->voiceCalls->create($call);
    }

    public function sendVoicee($phone_recipient, $message, $gender, $myphone)
    {
        $client = new MessageBird\Client(env('MB_KEY'));

        $call = new MessageBird\Objects\Voice\Call;
        // $call->source = $row->sim->number;
        $call->source = str_replace('.', '', $myphone);
        $call->destination = $this->convertPhoneVN($phone_recipient); // Số nhận cuộc gọi

        $flow = new MessageBird\Objects\Voice\CallFlow;
        $flow->title = 'Đăng Ký Thành Công';

        $step = new MessageBird\Objects\Voice\Step;
        $step->action = 'say';
        $step->options = [
            // 'payload' => 'Mã xác nhận của quý khách là . 5 . 3 . 8 . 8 . 9 . xin nhắc lại, 5 . 3 . 8 . 8 . 9', // nội dung cuộc gọi thoại
            'payload' => $message,
            'language' => 'vi-VN', // ngôn ngữ
            'voice' => $gender
        ];
        $flow->steps = [$step];
        $call->callFlow = $flow;
        return $client->voiceCalls->create($call);
    }

    public function convertPhoneVN($number)
    {
        return preg_replace("/^0/", "84", $number);
    }

    public function GetRequestTraoDoiCheo($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HEADER => 1,
            CURLOPT_PORT => 443,
            CURLOPT_VERBOSE => true,
            CURLOPT_RESOLVE => ["traodoicheo.com:443:104.28.0.106"],
            CURLOPT_HTTPHEADER => array(
                "Cookie: XSRF-TOKEN={$this->XSRFToken}; he_thong_tang_like_tang_sub_session={$this->session}"
            ),

        ));


        $result = curl_exec($curl);

        return $result;
    }

    public function GetFacebookPost($id)
    {
        $curl = curl_init();

        $token = $this->getRandomToken();
        $accessToken = $token->token;
        echo "https://graph.facebook.com/$id/feed?limit=1&access_token=$accessToken";
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.facebook.com/$id/feed?limit=1&access_token=$accessToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 100,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cookie: sb=uCTpYMAKBpMwXtwo6u3rw18n; fr=19UcbOi37XB9Kc06S.AWVTAJhzNytq9XE0q6ReZmzl30k.BhE_lM._I.AAA.0.0.BhE_lM.AWXnyl8La-k"
            ),
        ));

        $result = curl_exec($curl);
        //
        //        $response = json_decode($result);
        //        if (isset($response->error)) {
        //            if ($response->error->code === 190) {
        //                $token->isExpired = 1;
        //                $token->save();
        //                var_dump("token het han ne");
        //            }
        //        }

        return $result;
    }

    public function DeleteById(Request $req)
    {
        $order = Order::find($req->input('id'));
        $order->delete();
        return response()->json("OK", 200);
    }
}

class ApiBL
{
    public $api_url = 'https://bulkfollows.com/api/v2'; // API URL
    public $api_key = '7eb48d9526290bc93473bc4c91dd7d66'; // Your API key

    public function order($data)
    { // add order
        $post = array_merge(array('key' => $this->api_key, 'action' => 'add'), $data);
        return json_decode($this->connect($post));
    }

    private function connect($post)
    {
        $_post = array();
        if (is_array($post)) {
            foreach ($post as $name => $value) {
                $_post[] = $name . '=' . urlencode($value);
            }
        }

        $ch = curl_init($this->api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if (is_array($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, join('&', $_post));
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        $result = curl_exec($ch);
        if (curl_errno($ch) != 0 && empty($result)) {
            $result = false;
        }
        curl_close($ch);
        return $result;
    }
}
