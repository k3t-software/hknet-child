<?php

namespace App\Helpers;

use App\Partner;

use HTTP_Request2;
use HTTP_Request2_Exception;
use Illuminate\Database\Eloquent\Model;

class TraoDoiSub
{
  private $baseURL = 'https://traodoisub.com';
  public $cookie;

  public $account;

  public function __construct($isFree = false)
  {
    $partnerModel = new Partner;
    $accountType = $isFree ? "traodoisub.com-sale" : "traodoisub.com";
    $account = $partnerModel->getAccountTraoDoiSub($accountType);
    $this->account = $account;
  }

  public function login()
  {

    if (!isset($this->account)) {
      return;
    }
    $username = $this->account->username;
    $password = $this->account->password;
    try {
      $request = new \HTTP_Request2();
      $request->setUrl($this->baseURL . "/scr/login.php");
      $request->setMethod(\HTTP_Request2::METHOD_POST);
      $request->setConfig(array(
        'follow_redirects' => true,
        'ssl_verify_peer' => false,
        'ssl_verify_host' => false
      ));
      $request->setHeader(array(
        'Content-Type' => 'application/x-www-form-urlencoded'
      ));
      $request->addPostParameter(array(
        'username' => $username,
        'password' => $password
      ));
      $response = $request->send();
      if ($response->getStatus() == 200) {
        $isWrongPassword = str_contains($response->getBody(), '1');
        if ($isWrongPassword) {
          $this->account->is_stop = 1;
          $this->account->note = "Wrong password !";
          $this->account->save();
          exit('Sai toàn khoản hoặc mật khẩu');
        }
        $cookies = $response->getCookies();
        foreach ($cookies as $key => $cookie) {
          if ($cookie['name'] === "PHPSESSID") {
            $this->cookie = $cookie['value'];
            return $cookie['value'];
          }
        }
        return null;
      } else {
        echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
          $response->getReasonPhrase();
        return false;
      }
    } catch (HTTP_Request2_Exception $e) {
      echo 'Error: ' . $e->getMessage();
      return 'loi me roi ?';
    }
  }

  private function post($url, $data)
  {
    if (!isset($this->cookie)) {
      return (object)[
        'success' => 0,
        'message' => 'Quả tải vui lòng thử lại hoặc đổi qua server khác !! [068]'
      ];
    }

    $request = new \HTTP_Request2();
    $request->setUrl($this->baseURL . $url);
    $request->setMethod(\HTTP_Request2::METHOD_POST);
    $request->setConfig(array(
      'follow_redirects' => true,
      'ssl_verify_peer' => false,
      'ssl_verify_host' => false
    ));
    $request->setHeader(array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'Cookie' => 'PHPSESSID=' . $this->cookie
    ));
    $request->addPostParameter($data);

    try {
      $response = $request->send();
      if ($response->getStatus() == 200) {
        $isSuccess = str_contains($response->getBody(), 'thành công');
        $isNotEnoughMoney = str_contains($response->getBody(), '1');
        $isBlockAccount = str_contains($response->getBody(), 'cấm sử dụng');
        if ($isSuccess) {
          $this->account->used += 1;
        }

        if ($isBlockAccount) {
          $this->account->is_stop = 1;
          $this->account->note = "Tài khoản vi phạm quá nhiều nên bị cấm sử dụng tính năng này vĩnh viễn!";
        }

        if ($isNotEnoughMoney) {
          $this->account->is_stop = 1;
          $this->account->note = "Tài khoản hết tiền";
        }

        $this->account->save();
        return (object)[
          'success' => $isSuccess ? 1 : 0,
          'message' => $response->getBody()
        ];
      } else {
        return (object)['success' => 0, 'message' => "Xảy ra lỗi khi thêm vào hệ thống [016]"];
      }
    } catch (\HTTP_Request2_Exception $e) {
      echo 'Error: ' . $e->getMessage();
      return (object)['success' => 0, 'message' => 'Unknown error: ' . $e->getMessage()];
    }
  }

  public function buyReaction($idPost, $amount, $reaction)
  {
    return $this->post('/mua/reaction/themid.php', array(
      'id' => $idPost,
      'sl' => (int)$amount,
      'dateTime' => date('Y-m-d H:i:s'),
      'loaicx' => $reaction
    ));
  }


    public function buyLike($idPost, $amount)
    {
        return $this->post('/mua/like/themid.php', array(
            'id' => $idPost,
            'sl' => (int)$amount,
            'dateTime' => date('Y-m-d H:i:s'),
        ));
    }

    public function buyShare($idPost, $amount)
    {
        return $this->post('/mua/share/themid.php', array(
            'id' => $idPost,
            'sl' => (int)$amount,
            'dateTime' => date('Y-m-d H:i:s'),
        ));
    }

    public function buyComment($idPost, $amount, $comments)
    {
        if ($amount < 10) {
            return (object)['success' => 0, 'message' => "Tối thiểu 10 comments."];
        }

        return $this->post('/mua/comment/themid.php', array(
            'id' => $idPost,
            'sl' => (int)$amount,
            'noidung' => $comments,
            'dateTime' => date('Y-m-d H:i:s'),
        ));
    }
}


