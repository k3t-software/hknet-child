<?php

namespace App;

class Constant
{

    public $SERVICE_ID;

    public static function SERVICEID($category, $serviceID)
    {
        switch ($category) {
            case "facebook-gia-re":
            {
                switch ($serviceID) {
                    case "sub":
                    {
                        return 1;
                    }
                    case "likepage":
                    {
                        return 2;
                    }
                    case "likepost":
                    {
                        return 3;
                    }
                }
                break;
            }
            case "instagram":
            {
                switch ($serviceID) {
                    case "like":
                    {
                        return 11;
                    }
                    case "sub":
                    {
                        return 12;
                    }
                    case "view":
                    {
                        return 20;
                    }
                }
            }
            case "tiktok":
            {
                switch ($serviceID) {
                    case "like":
                    {
                        return 13;
                    }
                    case "likeglobal":
                    {
                        return 51;
                    }
                    case "sub":
                    {
                        return 14;
                    }
                    case "subglobal":
                    {
                        return 52;
                    }
                    case "view":
                    {
                        return 24;
                    }
                    case "comment":
                    {
                        return 25;
                    }
                    case "share":
                    {
                        return 26;
                    }
                    case "live":
                    {
                        return 69;
                    }
                }
            }
            case "youtube":
            {
                switch ($serviceID) {
                    case "sub":
                    {
                        return 27;
                    }
                    case "gioxem":
                    {
                        return 28;
                    }
                    case "view":
                    {
                        return 29;
                    }
                    case "like":
                    {
                        return 30;
                    }
                    case "dislike":
                    {
                        return 31;
                    }
                    case "comment":
                    {
                        return 32;
                    }
                    case "share":
                    {
                        return 33;
                    }
                    case "worldview":
                    {
                        return 34;
                    }
                    case "subnhanh":
                    {
                        return 35;
                    }
                }
            }
            case "other":
            {
                switch ($serviceID) {
                    case "voice":
                    {
                        return 45;
                    }
                }
            }
            case "facebook-buff":
            {
                switch ($serviceID) {
                    case "comment":
                    {
                        return 4;
                    }
                    case "reviewpage":
                    {
                        return 5;
                    }
                    case "sub":
                    {
                        return 6;
                    }
                    case "likepage":
                    {
                        return 7;
                    }
                    case "likepageglobal":
                    {
                        return 53;
                    }
                    case "view":
                    {
                        return 8;
                    }
                    case "live":
                    {
                        return 9;
                    }
                    case "live2":
                    {
                        return 36;
                    }
                    case "sharewall":
                    {
                        return 10;
                    }
                    case "sharelive":
                    {
                        return 17;
                    }
                    case "buffsubnhanh":
                    {
                        return 19;
                    }
                    case "view3s":
                    {
                        return 37;
                    }
                    case "commentfb":
                    {
                        return 38;
                    }
                    case "likepost":
                    {
                        return 39;
                    }
                    case "likepost2":
                    {
                        return 44;
                    }
                    case "likepost3":
                    {
                        return 54;
                    }
                    case "locbanbe":
                    {
                        return 40;
                    }
                    case "buffmember":
                    {
                        return 41;
                    }
                    case "likepage2":
                    {
                        return 43;
                    }
                    case "tangbanbe":
                    {
                        return 50;
                    }
                    case "likecomment":
                    {
                        return 68;
                    }
                }
            }
            case "twitter":
            {
                switch ($serviceID) {
                    case "follow":
                    {
                        return 47;
                    }
                    case "like":
                    {
                        return 48;
                    }
                }
            }
            case "telegram":
            {
                switch ($serviceID) {
                    case "membergroup":
                    {
                        return 49;
                    }
                }
            }


            case "google":
            {
                switch ($serviceID) {
                    case "gmail":
                    {
                        return 64;
                    }
                }
            }
            case "shopee":
            {
                switch ($serviceID) {
                    case "sub":
                    {
                        return 67;
                    }
                    case "like":
                    {
                        return 66;
                    }
                }
            }
            case "flash-sale":
            {
                switch ($serviceID) {
                    case "likepost":
                    {
                        return 55;
                    }
                    case "sub":
                    {
                        return 56;
                    }
                    case "likepage":
                    {
                        return 57;
                    }
                    case "liketiktok":
                    {
                        return 58;
                    }
                    case "subtiktok":
                    {
                        return 59;
                    }
                    case "viewtiktok":
                    {
                        return 60;
                    }
                    case "likeyoutube":
                    {
                        return 61;
                    }
                    case "subyoutube":
                    {
                        return 62;
                    }
                    case "viewyoutube":
                    {
                        return 63;
                    }
                }
            }
            case "facebook-vip":
            {
                switch ($serviceID) {
                    case "comment":
                    {
                        return 16;
                    }
                    case "like":
                    {
                        return 15;
                    }
                    case "mat":
                    {
                        return 18;
                    }
                }
            }
        }
    }

    const RESPONSE_STATUS_SUCCESS = 1;

    const SERVICE_ID_CFACEBOOK_SUB = 1;
    const SERVICE_ID_CFACEBOOK_LIKEPAGE = 2;
    const SERVICE_ID_CFACEBOOK_LIKE2 = 3;

    const STATUS_ORDER_INCOMPLETE = 0;
    const STATUS_ORDER_COMPLETED = 1;
    const STATUS_ORDER_INCOMPLETED_2 = 2;
    const STATUS_ORDER_ERROR = 3;


    const TYPE_SERVICE_API = 0;
    const TYPE_SERVICE_MANUAL = 1;

    const STATUS_SERVICE_UNAVAILABLE = 1;

    const TRANSACTION_TYPE_CHARGE = 0;
    const TRANSACTION_TYPE_TRANSFER = 2;
    const TRANSACTION_TYPE_RECEIVCE = 3;
    const TRANSACTION_TYPE_REFUND = 4;
    const TRANSACTION_TYPE_COMMISSION = 5;


}

