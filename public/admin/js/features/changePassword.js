$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $("#passwordChangeForm")
        .find("input")
        .not("[type=submit]")
        .jqBootstrapValidation({
            submitSuccess: function ($form, event) {
                event.preventDefault();
                const password = $("#newPassword").val();
                $.ajax({
                    url: "/admin/changePassword",
                    method: "POST",
                    data: {
                        password,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed",
                                text: "Your password was successfully updated",
                                type: "success",
                            },
                            function () {
                                location.reload();
                            }
                        );
                    },
                    error: function () {
                        swal({
                            title: "Error!",
                            text: "Your password cannot be changed",
                            type: "error",
                        });
                    },
                });
            },
        });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
