$(function () {
    'use strict';
    $('#myTable').on('click', '.editBtn', function () {
        const modal = $('#infoModal');
        const form = $('#infoModal').find('#infoForm');
        const preloader = modal.find('.preloader');
        preloader.css('display', 'block');
        const id = $(this).data('id');
        modal.find('#saveBtn').attr('data-function', 'update');
        modal.find('#saveBtn').attr('data-id', id);

        $.ajax({
            method: 'GET',
            url: `/admin/services/get?id=${id}`,
            dataType: 'json',
            success: function (data) {
                form.find('[name=name]').val(data.service.name);
                form.find('[name=status]').val(data.service.status);
                form.find('[name=apiPrice]').val(data.service.apiPrice);
                form.find('[name=agencyPrice]').val(data.service.agencyPrice);
                form.find('[name=collaboratorsPrice]').val(
                    data.service.collaboratorsPrice
                );
                form.find('[name=block_duplicate]').attr('checked', data.service.block_duplicate === 1);
                form.find('[name=guestPrice]').val(data.service.guestPrice);
                form.find('[name=unit_duration]').val(
                    data.service.unit_duration
                );

                preloader.css('display', 'none');
            },
            error: function (data) {
                swal(
                    {
                        title: 'Failed!',
                        text: `Please check your info and try again !`,
                        type: 'error'
                    },
                    function () {
                        button.attr('disabled', false);
                    }
                );
            }
        });
    });
    $('#saveBtn').on('click', function () {
        const button = $(this);
        button.attr('disabled', true);
        const form = $('#infoForm');

        let formdata = form.serializeArray();
        let data = {};
        $(formdata).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.id = $(this).data('id');
        $.ajax({
            url: `/admin/services/update`,
            method: 'POST',
            data,
            success: function (data) {
                swal(
                    {
                        title: 'Completed!',
                        text: `Cập nhật dịch vụ thành công !`,
                        type: 'success'
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
            error: function (data) {
                swal(
                    {
                        title: 'Error!',
                        text: `Cập nhật thất bại !`,
                        type: 'error'
                    },
                    function () {
                        location.reload(true);
                    }
                );
            }
        });
    });


});


const updatePricePercent = () => {
    const pricePercent = $('#price_percent').val();

    $.ajax({
        url: `/admin/services/pricePercent`,
        method: 'POST',
        data: {
            percent: pricePercent
        },
        success: function (data) {
            swal(
                {
                    title: 'Completed!',
                    text: `Cập nhật dịch vụ thành công !`,
                    type: 'success'
                },
                function () {
                    location.reload(true);
                }
            );
        },
        error: function (data) {
            swal(
                {
                    title: 'Error!',
                    text: `Cập nhật thất bại !`,
                    type: 'error'
                },
                function () {
                    location.reload(true);
                }
            );
        }
    });
};
