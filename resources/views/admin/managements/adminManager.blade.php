@php
    $status = ['<span class="badge badge-success">Đang hoạt động</span>', '<span class="badge badge-warning">Đang bảo trì</span>']
@endphp

@extends("admin.layouts.master")
@section("content")


    <style>
        .box-body {
            padding: 0px;
            border-radius: 0 0 3px 3px;

            margin-bottom: 15px;
            padding-top: 10px;
        }

        .box-service img {
            height: 50px;
        }

        .box-service font.text-service {
            font-size: 15px;
        }

        .box-service-panel {
            border: 2px solid #098dff;
            border-radius: 8px;
        }

        .box-service-panel:hover {
            cursor: pointer;
            box-shadow: 0 0 0 1.5px rgba(11, 142, 255, 1);
        }

        .box-service-panel .panel-body a {
            text-decoration: none;
            color: inherit;
        }

        }
    </style>

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Thống kê quản trị viên</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{{number_format($tokenInfo['balance'])}} đ</h2>
                                <h6>Số dư site chính</h6></div>
                            <div class="col-4 align-self-center text-right  p-l-0">
                                <div id="sparklinedash3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2 class="">{{$orderCount}}
                                    <i class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Đơn hàng</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{!! number_format($revenue) !!} <i
                                            class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Doanh thu</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2> {{$userCount}} <i
                                            class="ti-angle-down font-14 text-danger"></i></h2>
                                <h6>Tổng thành viên</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Bảng giá
                                    @if($user->role === 3)
                                        Khách
                                    @endif
                                    @if($user->role === 2)
                                        Cộng Tác Viên
                                    @endif

                                    @if($user->role === 1)
                                        Đại Lý
                                    @endif
                                    @if($user->role === 0)
                                        đầy đủ cho Quản Trị Viên
                                    @endif
                                </h3>

                                <div class="table-responsive p-1">
                                    <table id="myTable" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Tên</th>
                                            @if($user->role === 3 || $user->role === 0)
                                                <th class="text">Khách</th>
                                            @endif
                                            @if($user->role === 2 || $user->role === 0)
                                                <th class="text">Cộng Tác Viên</th>
                                            @endif

                                            @if($user->role === 1 || $user->role === 0)
                                                <th class="text">Đại Lý</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($services as $service)
                                            <tr>
                                                <td class="text-center">{{$loop->index + 1}}</td>
                                                <td>
                                                    {{$service->name}}
                                                </td>
                                                @if($user->role === 3 || $user->role === 0)
                                                    <td class="text">
                                                        {{$service->guestPrice}}đ / {{$service['unit']}}
                                                    </td>
                                                @endif

                                                @if($user->role === 2 || $user->role === 0)
                                                    <td class="text">
                                                        {{$service->collaboratorsPrice}}đ / {{$service['unit']}}
                                                    </td>
                                                @endif

                                                @if($user->role === 1 || $user->role === 0)
                                                    <td class="text">
                                                        {{$service->agencyPrice}}đ / {{$service['unit']}}
                                                    </td>

                                                @endif

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @php
                    $bannerValues = json_decode($banner->value, true)
                @endphp
                @if(isset($bannerValues['isShow']) && $bannerValues['isShow'] === 'on')
                    <div class="modal fade" id="dashboardBanner" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 10%;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{$bannerValues['title']}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! $bannerValues['html'] !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif



            <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

        @endsection

        @section('scripts')
            <!-- ============================================================== -->
                <!-- This page plugins -->
                <!-- ============================================================== -->

                <script>
                    @if(isset($bannerValues) && isset($bannerValues['isShow']))
                    const banner = {
                        id: '{{strtotime($banner->updated_at)}}',
                        isShow: {{$bannerValues['isShow'] === 'on' ? 'true' : 'false'}},
                        title: `{{$bannerValues['title']}}`,
                        html: `{{$bannerValues['html']}}`
                    };

                    if (banner.isShow && !localStorage.getItem('isShowedModal_' + banner.id)) {
                        $('#dashboardBanner').modal('show');
                    }

                    $('#dashboardBanner').on('hidden.bs.modal', function () {
                        localStorage.setItem('isShowedModal_' + banner.id, true);
                    });
                    @endif


                </script>


                <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

                <script src="{{asset("admin/js/dashboard4.js")}}"></script>



    {{-- <script src="{{asset("admin/js/dashboard2.js")}}"></script> --}}
@endsection
