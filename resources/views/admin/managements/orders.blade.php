@php
    $status = ['<span class="badge badge-warning">Đang xử lý</span>', '<span class="badge badge-info">Đã hoàn thành</span>','<span class="badge badge-warning">Đang chạy</span>', '<span class="badge badge-danger">Lỗi ID Sub</span>'];
    $vipStatus = ['<span class="badge badge-warning">Đang xử lý</span>', '<span class="badge badge-success">Đã Xong</span>','<span class="badge badge-info">Đang chạy</span>', '<span class="badge badge-danger">Lỗi ID</span>'];

@endphp
@extends('admin.layouts.master')


@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Orders Management</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
                <li class="breadcrumb-item active">Orders</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        {{--        <nav>--}}
        {{--            <div class="nav nav-tabs" id="nav-tab" role="tablist">--}}
        {{--                @foreach ($services as $service)--}}
        {{--                    <a class="nav-item nav-link" id="nav-{{$service->id}}-tab" data-toggle="tab"--}}
        {{--                       href="#nav-{{$service->id}}" role="tab"--}}
        {{--                       aria-controls="nav-{{$service->id}}" aria-selected="false">{{$service->name}}</a>--}}
        {{--                @endforeach--}}
        {{--            </div>--}}
        {{--        </nav>--}}
        <div class="tab-content" id="nav-tabContent">

            @foreach ($services as $service)
                {{--                <div class="tab-pane fade" id="nav-{{$service->id}}" role="tabpanel"--}}
                {{--                     aria-labelledby="nav-{{$service->id}}-tab">--}}
                @php
                    $hasOrder = false;
                    foreach ($orders as $order) {
                        if ($order->service_id == $service->id) {
                            $hasOrder = true;
                            break;
                        }
                    }
                @endphp

                @if($hasOrder):
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-3">
                                    <div class="col-md-8">
                                        <h3 class="card-title">{{$service->name}}</h3>
                                    </div>

                                </div>

                                <div class="table-responsive p-1">
                                    <table class=" myTable table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Link hoặc ID</th>
                                            <th>Người dùng</th>
                                            <th>Tên người dùng</th>
                                            @if ($service->id == 6 || $service->id == 7 || $service->id == 11 || $service->id == 12 || $service->id == 13 || $service->id == 14 || $service->id == 19 || $service->id == 24 || $service->id == 43 || $service->id == 27 || $service->id == 29 || $service->id == 30 || $service->id == 31 || $service->id == 32 || $service->id == 33 || $service->id == 34|| $service->id == 41|| $service->id == 51|| $service->id == 52|| $service->id == 53)
                                                <th><font color="red">Start Count</font></th>

                                            @endif
                                            @if ($service->id == 15)
                                                <th><font color="red">TDC_Status</font></th>

                                            @endif
                                            <th class="text-right">Số lượng</th>
                                            <th class="text-right">Tổng tiền</th>
                                            <th class="text-right">Lý do</th>
                                            <th class="text-right">Trạng thái</th>
                                            @if ($service->type == 1)
                                                <th class="text-right">Hành động</th>
                                            @endif
                                            <th class="text-center">Ngày hết hạn</th>
                                            <th class="text-right">Thời gian</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($orders as $order)
                                            @if ($order->service_id == $service->id)
                                                @php
                                                    $notify = isset($order->notify) ? $order->notify : "";
                                                    $isWaitRefund = str_contains($notify, 'Đợi hoàn tiền');
                                                @endphp
                                                @if($isWaitRefund)
                                                    <tr style="background: wheat;">
                                                @else
                                                    <tr>
                                                        @endif
                                                        <td>
                                                            {{$order->id}}
                                                        </td>
                                                        <td>
                                                            {{$order->target}}
                                                        </td>
                                                        <td>
                                                            {{$order->user->phone}}
                                                        </td>
                                                        <td>
                                                            {{$order->user->name}}
                                                        </td>
                                                        @if ($service->id == 6 || $service->id == 7 || $service->id == 11 || $service->id == 12 || $service->id == 13 || $service->id == 14 || $service->id == 19 || $service->id == 24 || $service->id == 43 || $service->id == 27 || $service->id == 29 || $service->id == 30 || $service->id == 31 || $service->id == 32 || $service->id == 33 || $service->id == 34|| $service->id == 41|| $service->id == 51|| $service->id == 52|| $service->id == 53)
                                                            <td>
                                                                {{$order->startcount}}
                                                            </td>
                                                        @endif
                                                        @if ($service->id == 15)
                                                            <td>
                                                                {{$order->viplike_status}}
                                                            </td>
                                                        @endif
                                                        <td class="text-right">
                                                            {!!number_format($order->quantity)!!}
                                                        </td>

                                                        <td class="text-right">
                                                            {!!number_format($order->total)!!}
                                                        </td>

                                                        <td class="text-right" style="max-width: 200px;">
                                                            <span class="badge badge-danger">{{$order->note}}</span>
                                                        </td>

                                                        <td class="text-right">
                                                            @if($isWaitRefund)
                                                                <span class="badge badge-danger">{{$order->notify}}</span>
                                                            @else
                                                                @if ($service->type == 1)
                                                                    {!!$vipStatus[$order->status]!!}
                                                                @else
                                                                    {!!$status[$order->status]!!}
                                                                @endif
                                                                @if ($service->id == 6 || $service->id == 7 || $service->id == 11 || $service->id == 12 || $service->id == 13 || $service->id == 14|| $service->id == 24|| $service->id == 29|| $service->id == 30 )
                                                                    Active✔️
                                                                @endif
                                                            @endif
                                                        </td>
                                                        <td class="text-center product-action">
                                                            <a href=""
                                                               data-id={{$order->id}} data-backMoney={{$order->total}} class="changeStatusBtn"
                                                               data-toggle="modal"
                                                               data-target="#changeStatusModal"> <i
                                                                        class="fa  fa-check-square-o text-inverse m-r-10"></i>
                                                            </a>

                                                            <a href=""
                                                               data-id={{$order->id}} class="changeStartCountBtn"
                                                               data-toggle="modal"
                                                               data-target="#changeStartCountModal"><i
                                                                        class="fa fa-calendar-o text-inverse m-r-10"></i>
                                                            </a>
                                                            <a href="" data-id={{$order->id}} class="changeNotifyBtn"
                                                               data-toggle="modal"
                                                               data-target="#changeNotifyModal"><i
                                                                        class="fa fa-comment-o text-inverse m-r-10"></i>
                                                            </a>
                                                            <a href=""
                                                               data-id={{$order->id}} class="changeviplike_statusBtn"
                                                               data-toggle="modal"
                                                               data-target="#changeviplike_statusModal"><i
                                                                        class="fa fa-like-o text-inverse m-r-10"></i>
                                                            </a>
                                                            <a href="" data-id={{$order->id}} class="deleteBtn"
                                                               data-toggle="tooltip"
                                                               data-original-title="Delete"> <i
                                                                        class="fa fa-close text-danger"></i> </a>
                                                        </td>
                                                        @if ($service->type == 1)
                                                            <td class="text-right">
                                                                {{date('d/m/Y', strtotime($order->created_at) + $order->duration)}}
                                                            </td>
                                                        @endif
                                                        <td class="text-right">
                                                            {{date('H:i:s d/m/Y', strtotime($order->created_at))}}
                                                        </td>


                                                    </tr>
                                                @endif
                                                @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    </div>--}}
                </div>
                @endif
            @endforeach
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                    </circle>
                </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Thay đổi trạng thái</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="role" class="control-label">Trạng thái:</label>

                        <select name="status" class="form-control" id="statusChange">
                            <option value="0">Đang xử lý</option>
                            <option value="2">Đang chạy</option>
                            <option value="1">Đã Xong</option>
                            <option value="3">Lỗi ID</option>
                        </select>
                    </div>
                    <div id="backMoneyContainer" style="display: none">
                        <div class="form-group">
                            <label class="control-label">Hoàn tiền: </label>
                            <input class="form-control" name="backMoney" id="backMoney" />
                        </div>

                        <div class="form-group">
                            <label class="control-label">Thông báo: </label>
                            <textarea name="notify" class="form-control" id="notify"></textarea>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="changeStatusBtn" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changeStartCountModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                    </circle>
                </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Thay đổi trạng thái</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="role" class="control-label">Số Lượng Gốc:</label>

                        <div class="input-group col-md-12">
                            <div class="input-group-prepend">
                                <button class="btn btn-info" type="button">Số lượng</button>
                            </div>
                            <input value="1" min="0" name="startcount" type="number" class="form-control"
                                   aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="changeStartCountBtn" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changeNotifyModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                    </circle>
                </svg>
            </div>

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Thay đổi trạng thái</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="role" class="control-label">Thông báo:</label>

                        <div class="input-group col-md-12">
                            <div class="input-group-prepend">
                                <button class="btn btn-info" type="button">Nhập thông báo</button>
                            </div>
                            <textarea rows="3" value="" min="0" name="notify" class="form-control" aria-label="Username"
                                      aria-describedby="basic-addon1"> </textarea>
                        </div>
                        <br><a>Vui lòng công khai theo dõi & nút kết bạn</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="changeNotifyBtn" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changeviplike_statusModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                    </circle>
                </svg>
            </div>

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Thay đổi trạng thái</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="role" class="control-label">Thông báo:</label>

                        <div class="input-group col-md-12">
                            <div class="input-group-prepend">
                                <button class="btn btn-info" type="button">Nhập thông báo</button>
                            </div>
                            <input value="" min="0" name="viplike_status" type="text" class="form-control"
                                   aria-label="Username"
                                   aria-describedby="basic-addon1">
                        </div>
                        <br><a>Vui lòng công khai theo dõi & nút kết bạn</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="changeviplike_statusBtn" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="commentDetailModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10">
                    </circle>
                </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Chi tiết comment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="commentDetails" class="alert alert-info"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $('.myTableCustom').DataTable({
        //         order: [[0, 'desc']]
        //     });
        // });
    </script>

@endsection
