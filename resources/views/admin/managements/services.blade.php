@php
    $status = ['<span class="badge badge-success">Đang hoạt động</span>', '<span class="badge badge-warning">Đang bảo trì</span>']
@endphp

@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Quản lý dịch vụ</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Dịch vụ</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Danh sách dịch vụ</h3>
                            </div>

                        </div>
                        <span class="text-info">Thiết lập nhanh % lợi nhuận: ( {{$configs->getValue('agency_name')}} | {{$configs->getValue('collaborators_name')}} | {{$configs->getValue('guest_name')}} ) </span> <br />
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Cài đặt nhanh % lợi nhuận." id="price_percent" value="30|50|60">
                            <div class="input-group-append">
                                <button class="btn btn-success" onclick="updatePricePercent()">Cài đặt</button>
                            </div>
                        </div>


                        <div class="table-responsive p-1">
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Tên</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-right">Giá gốc</th>
                                    <th class="text-right">{{$configs->getValue('agency_name')}}</th>
                                    <th class="text-right">{{$configs->getValue('collaborators_name')}}</th>
                                    <th class="text-right">{{$configs->getValue('guest_name')}}</th>
                                    <th class="text-right">Đơn vị</th>
                                    <th class="text-right">Thời gian / đơn vị</th>
                                    <th class="text-right">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($services as $service)
                                    <tr>
                                        <td class="text-center">{{$loop->index + 1}}</td>
                                        <td>
                                            {{ $service->category->name."-".$service->name}}
                                        </td>
                                        <td class="text-center">
                                            {!!$status[$service->status]!!}
                                        </td>
                                        <td class="text-right">
                                            @php
                                                $price = $entryPrices[$service->id];
                                                    if ($price) {
                                                        echo $price;
                                                    }else{
                                                        echo '<span class="badge badge-danger">Không hỗ trợ dịch vụ</span>';
                                                    }
                                            @endphp
                                        </td>
                                        <td class="text-right">
                                            {{$service->agencyPrice}}
                                        </td>
                                        <td class="text-right">
                                            {{$service->collaboratorsPrice}}
                                        </td>
                                        <td class="text-right">
                                            {{$service->guestPrice}}
                                        </td>
                                        <td class="text-right">
                                            {{$service->unit}}
                                        </td>
                                        <td class="text-right">
                                            {{$service->unit_duration}}
                                        </td>
                                        <td class="text-right service-action">
                                            <a href="" data-id={{$service->id}} class = "editBtn" data-toggle="modal"
                                            data-target="#infoModal"> <i
                                                    class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="" data-id={{$service->id}} class = "deleteBtn" data-toggle="tooltip
                                            " data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2"
                            stroke-miterlimit="10"></circle>
                </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Thông tin dịch vụ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form class="form-material p-1" id="infoForm">
                        <div class="form-group">
                            <label for="name" class="control-label">Tên dịch vụ:</label>
                            <input type="text" class="form-control" name="name" />
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Trạng thái:</label>
                            <select name="status" class="form-control">
                                <option value="0">Đang hoạt động</option>
                                <option value="1">Đang bảo trì</option>
                            </select>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label for="apiPrice" class="control-label">Giá API:</label>
                            <input type="number" class="form-control" name="apiPrice" />
                        </div>
                        <div class="form-group">
                            <label for="apiPrice" class="control-label">Giá {{$configs->getValue('agency_name')}}</label>
                            <input type="number" class="form-control" name="agencyPrice" />
                        </div>
                        <div class="form-group">
                            <label for="apiPrice" class="control-label">Giá  {{$configs->getValue('collaborators_name')}}</label>
                            <input type="number" class="form-control" name="collaboratorsPrice" />
                        </div>
                        <div class="form-group">
                            <label for="apiPrice" class="control-label">Giá {{$configs->getValue('guest_name')}}:</label>
                            <input type="number" class="form-control" name="guestPrice" />
                        </div>
                        <div class="form-group">
                            <label for="apiPrice" class="control-label">Thời gian / đơn vị:</label>
                            <input type="number" class="form-control" name="unit_duration" />
                        </div>

                        <div style="padding-left: 0;">
                            <input type="checkbox" id="block_duplicate" name="block_duplicate">
                            <label for="block_duplicate">Chặn trùng đơn khi đang chạy</label>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" data-function="create" id="saveBtn" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset("admin/js/features/services.js")}}"></script>
@endsection

