$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#loginBtn").on("click", function (e) {
        e.preventDefault();
        const formData = $("#loginForm").serializeArray();
        let data = {};
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        $.ajax({
            url: "/login",
            method: "POST",
            dataType: "json",
            data,
            success: function (dataResponse) {
                if (dataResponse) {
                    location.href = "/admin/users";
                } else
                    swal(
                        {
                            title: "Error!",
                            text: "Wrong username or password",
                            type: "error",
                        },
                        function () {}
                    );
            },
        });
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
