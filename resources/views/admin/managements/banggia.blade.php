@php
	$status = ['<span class="badge badge-success">Đang hoạt động</span>', '<span class="badge badge-warning">Đang bảo trì</span>'];
@endphp

@extends("admin.layouts.master")
@section("content")


<style>
    .box-body {
      padding: 0px;
      border-radius: 0 0 3px 3px;
    
      margin-bottom: 15px;
      padding-top: 10px;
    }
	.box-service img {
		height: 50px;
	}
	.box-service font.text-service {
		font-size: 15px;
	}
	.box-service-panel {
		border: 2px solid #098dff;
		border-radius: 8px;
	}
	.box-service-panel:hover {
	    cursor: pointer;
		box-shadow: 0 0 0 1.5px rgba(11, 142, 255, 1);
	}
	.box-service-panel .panel-body a {
	    text-decoration: none;
		color: inherit;
	}
}
</style>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Book Báo Điện Tử</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div>

</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Row -->
	<!-- Row -->

	<div class="card">
<div class="row">
		<div class="col-12">
	<div class="row">
		<div class="col-12">
            <div class="card">
            <div class="card-body">
				<h3 class="card-title">Báo Giá Đăng Bài Báo Điện Tử Hàng Đầu</h3>
					<div class="table-responsive p-1">
						<table id = "myTable" class="table table-bordered">
							<thead>
							<ul>
                                <li>Liên hệ chúng tôi sau khi chọn được tờ báo phía dưới mà khách hàng mong muốn !</li>
                                <li>Hotline / Zalo:<a href="tel:0812888896"><strong> 0812.8888.96</strong></a></li>
                                <li>Email: sales.hknet@gmail.com</li>
                                </ul>
                        <tr role="row"><th class="text-center sorting_asc" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 33.6094px;">#</th><th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Tên: activate to sort column ascending" style="width: 83.797px;">Kênh Truyền Thông</th>
                        <th class="text sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Khách: activate to sort column ascending" style="width: 82.094px;">Đơn Giá / Bài</th></tr>
                        </thead>
                        <tbody>
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">1</td>
                            <td>
                            https://vnexpress.vn
                            </td>
                            <td class="text">
                            10.500.000đ
                            </td>
                            </tr>

                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">2</td>
                            <td>
                            https://dantri.com.vn
                            </td>
                            <td class="text">
                            4.500.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">3</td>
                            <td>
                            https://thanhnien.vn
                            </td>
                            <td class="text">
                            6.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">4</td>
                            <td>
                            https://tuoitre.vn
                            </td>
                            <td class="text">
                            10.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">5</td>
                            <td>
                            https://tienphong.vn
                            </td>
                            <td class="text">
                            3.500.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">6</td>
                            <td>
                            https://doanhnhan.vn
                            </td>
                            <td class="text">
                            3.000.000đ
                            </td>
                            </tr>
                            
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">7</td>
                            <td>
                            https://kenh14.vn
                            </td>
                            <td class="text">
                            7.500.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">8</td>
                            <td>
                            https://danviet.vn
                            </td>
                            <td class="text">
                            5.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">9</td>
                            <td>
                            https://yeah1.com
                            </td>
                            <td class="text">
                            4.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">10</td>
                            <td>
                            https://vietnamnet.vn
                            </td>
                            <td class="text">
                            5.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">11</td>
                            <td>
                            https://nld.com.vn
                            </td>
                            <td class="text">
                            4.000.000đ
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">12</td>
                            <td>
                            https://www.yan.vn
                            </td>
                            <td class="text">
                            6.000.000đ
                            </td>
                            </tr>

                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">13</td>
                            <td>
                            https://baomoi.com
                            </td>
                            <td class="text">
                            4.200.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">14</td>
                            <td>
                            https://cafef.vn - https://cafebiz.vn
                            </td>
                            <td class="text">
                            5.000.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">15</td>
                            <td>
                            https://zing.vn
                            </td>
                            <td class="text">
                            6.000.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">16</td>
                            <td>
                            https://nguoinoitieng.net
                            </td>
                            <td class="text">
                            3.200.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">17</td>
                            <td>
                            https://www.xaluan.com
                            </td>
                            <td class="text">
                            3.200.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">18</td>
                            <td>
                            https://eva.vn
                            </td>
                            <td class="text">
                            4.200.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">19</td>
                            <td>
                            https://vtc.vn
                            </td>
                            <td class="text">
                            6.000.000đ 
                            </td>
                            </tr>
                            
                            <tr role="row" class="odd">
                            <td class="text-center sorting_1">20</td>
                            <td>
                            https://saostar.vn
                            </td>
                            <td class="text">
                            5.200.000đ 
                            </td>
                            </tr>
                            
            </tbody>
            

            </table><div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><span></div></div>
            </div>
            </div>
            </div>
            </div>
            
	</div>
	
	<!-- Row -->
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@section('scripts')
<!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->

    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

    <script src="{{asset("admin/js/dashboard4.js")}}"></script>


	
    {{-- <script src="{{asset("admin/js/dashboard2.js")}}"></script> --}}
@endsection