let lastestMessageId = 0;

function scrollBottom() {
    $("#msg_history").scrollTop($("#msg_history")[0].scrollHeight);
}

async function viewChat(id) {
    axios.get('/admin/chat/getMessages?from=' + id).then(res => {
        $('div[class*="active_chat"]').removeClass('active_chat');
        $('div[data-senderid="' + id + '"]').addClass('active_chat');
        const messages = res.data || [];
        const lastMessage = messages[messages.length - 1].id;
        if(lastMessage === lastestMessageId) return
        const messageHtml = messages.map(message => {
            const isOutGoingMessage = message.user_id === userId;
            const msg = message.message;
            let more = ''
            if(message.id < lastestMessageId && lastestMessageId !== 0) {
               more = '<hr />'
            }
            if (isOutGoingMessage) {
                return `${more}<div class="outgoing_msg">
                            <div class="sent_msg">
                                <p>${msg}</p>
                                <span class="time_date"> ${moment().format('H:m DD/MM/YYYY')}</span></div>
                        </div>`;
            } else {
                return `${more}<div class="incoming_msg">
                            <div class="incoming_msg_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>${msg}</p>
                                    <span class="time_date"> ${moment(message.created_at).format('H:m DD/MM/YYYY')}     </span></div>
                            </div>
                        </div>`;
            }
        });

        $('#msg_history').html(messageHtml);
        $('#target_id').val(id);
        scrollBottom();
        lastestMessageId = lastMessage;
        axios.post('/admin/chat/seenMessage', { user_id: id }).then(res => {
            $('div[data-senderid="' + id + '"] h5 .badge').remove();
        }).catch(err => {

        });

    });
}

const mySenders = new Set();
setInterval(() => {
    viewChat($('#target_id').val().trim());
    getUnReadMessage();
}, 2000);

function pingPong(){
    const audio = new Audio('/admin/sounds/new_message.mp3');
    audio.play();
}

function getUnReadMessage() {
    axios.get('/admin/chat/getUnReadMessage').then(res => {
        const unSeenMessages = res.data;
        if (unSeenMessages.length > 0){
            pingPong();
        }

        unSeenMessages.forEach(({ user_id, message }) => {
            $('div[data-senderid="' + user_id + '"] #new_message').html(message);
            if ($('div[data-senderid="4"] h5 span').length <= 1) {
                $('div[data-senderid="' + user_id + '"] h5').append('<span class="badge badge-primary">Mới</span>');
            }
        });
    }).catch(err => {
        console.log(err);
    });
};

$(function () {
    async function getSenders() {
        axios.get('/admin/chat/getSenders').then(res => {
            // Group by
            const senders = res.data;
            if (senders.length ===0)return
            const inboxChatHTML = senders.map(sender => {
                if (!mySenders.has(sender.from_id)) {
                    mySenders.add(sender.from_id);
                    return (`<div class="chat_list" onclick="viewChat(${sender.from_id})" data-senderId="${sender.from_id}">
                            <div class="chat_people">
                                <div class="chat_img">
                                <div><i class="fa fa-3x fa-user-circle" /> </div>
                                    </div>
                                <div class="chat_ib">
                                    <h5>${sender.from_name} <span class="chat_date"></span></h5>
                                    <p  id="new_message">${sender.message}</p>
                                </div>
                            </div>
                        </div>`);
                }
            });
            mySenders.clear();
            $('#inbox_chat').html(inboxChatHTML);
        }).catch(err => {
            console.log(err.message);
        });
    }

    function sendMessage(messsage, target) {
        axios.post('/admin/chat/sendMessage', { messsage, target }).then(res => {
            $('#msg_history').append(`<div class="outgoing_msg">
                            <div class="sent_msg">
                                <p>${messsage}</p>
                                <span class="time_date"> ${moment().format('H:m DD/MM/YYYY')}</span></div>
                        </div>`);
            scrollBottom();
        }).catch(err => {
            console.log(err.messsage);
        });
    }

    $('#sendMessageForm').submit(e => {
        e.preventDefault();
        const target = $('#target_id').val().trim();
        const message = $('#message').val();
        if (!message && message === '') {
            return alert('Điền tin nhắn ');
        }
        $('#message').val('');
        sendMessage(message, target);

    });

    getSenders();
    setInterval(() => {
       getSenders();
    }, 20000)
});
