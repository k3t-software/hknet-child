<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = "configs";
    protected $primaryKey = 'config_key';
    public $timestamps = true;

    public function get($configKey = null)
    {
        if ($configKey) {
            return self::where('config_key', $configKey)->first();
        } else {
            return self::all();
        }
    }

    public function getLike($configLikeKey = "")
    {
        return self::where('config_key', 'like', "%${configLikeKey}%")->get();
    }

    public function getValue($configKey = null)
    {
        $config = $this->get($configKey);
        if (isset($config->value)) {
            return $config->value;
        } else {
            return null;
        }
    }

    public static function getValueStatic($configKey = null)
    {
        $config = self::where('config_key', $configKey)->first();
        if (isset($config->value)) {
            return $config->value;
        } else {
            return null;
        }
    }

    public function setConfig($configKey, $value = null)
    {
        $config = $this->get($configKey);
        if ($config) {
            $config->value = $value;
            return $config->save();
        } else {
            return self::insert([
                "config_key" => $configKey,
                "value" => $value
            ]);
        }
    }

}
