<?php

namespace App\Helpers;

class TikTok extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffSub($id, $amount)
    {
        return $this->post('/subtiktok', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffSubGlobal($id, $amount)
    {
        return $this->post('/subtiktokTay', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLike($id, $amount)
    {
        return $this->post('/liketiktok', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLikeGlobal($id, $amount)
    {
        return $this->post('/liketiktokTay', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffView($id, $amount)
    {
        return $this->post('/viewtiktok', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffComment($id, $noidung)
    {
        return $this->post('/commenttiktok', [
            "id" => $id,
            "noidung" => $noidung,
        ]);
    }

    public function buffShare($id, $amount)
    {

        return $this->post('/sharetiktok', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }
}
