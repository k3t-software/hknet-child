

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from templatemo.com/templates/templatemo_563_seo_dream/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Nov 2021 08:40:19 GMT -->
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="../../../fonts.gstatic.com/index.html">
    <link href="../../../fonts.googleapis.com/css258f5.css?family=Open+Sans:wght@300;400;600;700;800&amp;display=swap" rel="stylesheet">

    <title><?=$config->getValue('title')?></title>

    <!-- Bootstrap core CSS -->
    <link href="https://templatemo.com/templates/templatemo_563_seo_dream/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_563_seo_dream/assets/css/fontawesome.css">
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_563_seo_dream/assets/css/templatemo-seo-dream.css">
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_563_seo_dream/assets/css/animated.css">
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_563_seo_dream/assets/css/owl.css">
    <!--

    TemplateMo 563 SEO Dream

    https://templatemo.com/tm-563-seo-dream

    -->

</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!-- ***** Preloader End ***** -->

<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="https://templatemo.com/templates/templatemo_563_seo_dream/index.html" class="logo">
                        <h4><?=$config->getValue('admin_name')?> <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/logo-icon.png" alt=""></h4>
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li class="scroll-to-section"><a href="#top" class="active">Home</a></li>
                        <li class="scroll-to-section"><a href="#about">Thông Tin</a></li>
                        <li class="scroll-to-section"><a href="#services">Dịch Vụ</a></li>
                        <li class="scroll-to-section"><a href="#contact">Liên Hệ</a></li>
                        <li class="scroll-to-section"><div class="main-blue-button"><a href="/login">Đăng Nhập</a></div></li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                        <div class="left-content header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                            <div class="row">
                                <div class="col-lg-4 col-sm-4">
                                    <div class="info-stat">
                                        <h6>Khách Hàng Hài Lòng:</h6>
                                        <h4>1.232</h4>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4">
                                    <div class="info-stat">
                                        <h6>Đơn Hàng Đã Tạo:</h6>
                                        <h4>10.521</h4>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4">
                                    <div class="info-stat">
                                        <h6>Khách Hàng Lâu Năm:</h6>
                                        <h4>1.537</h4>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <h2>Dịch vụ mạng xã hội chất lượng với chi phí tối ưu nhất</h2>
                                </div>
                                <div class="col-lg-12">
                                    <div class="main-green-button scroll-to-section">
                                        <a href="/login">Đăng Nhập / Đăng Ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/banner-right-image.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="services" class="our-services section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <h6>Dịch Vụ</h6>
                    <h2>Các Dịch Vụ <?=$config->getValue('admin_name')?> Cung Cấp</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://img.icons8.com/ios-filled/100/000000/facebook-new.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Facebook</h4>
                                <p><?=$config->getValue('admin_name')?> tự hào là đơn vị đối tác cung cấp các dịch vụ facebook một cách nhanh chóng và toàn diện đáp ứng đầy đủ yêu cầu của khách hàng khi kinh doanh phát triển trên internet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://img.icons8.com/ios-filled/100/000000/youtube-play.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Youtube</h4>
                                <p>Chúng tối cung cấp các dịch vụ tăng lượt like, sub, lượt xem nhằm tăng độ uy tín và tin tưởng cho khách hàng và ngoài ra cũng có thể dùng để đạt điều kiện bật kiếm tiền cho kênh.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://img.icons8.com/ios-filled/100/000000/tiktok--v1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Tiktok</h4>
                                <p>Hiện nay mạng xã hội Tiktok đang phát triển rát thần tốc và để đáp ứng được nhu cầu người sử dụng nhằm tăng độ uy tín và tiếp cận cho thương hiệu, đó chính là lý do chúng tôi ra đời và phục vụ !</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://img.icons8.com/ios-filled/100/000000/instagram-new.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Instagram</h4>
                                <p>Dịch vụ Instagram hiện đang là cách giúp tài khoản Instagram của bạn tăng khả năng hiển thị khi người dùng tìm kiếm, tiếp cận thêm nhiều khách hàng, tăng khả năng bán hàng.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://img.icons8.com/ios-filled/100/000000/shopee.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Shopee</h4>
                                <p>Để bán hàng trên thương mại điện tử có sản phẩm là chưa đủ, phải có các dịch vụ đi kèm như tăng like follow để tăng lượt tìm kiếm và hiển thị.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.8s">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon">
                                <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/service-icon-02.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="right-content">
                                <h4>Khác</h4>
                                <p>Ngoài ra chúng tôi còn cung cấp nhiều dịch vụ khác nhằm đáp ứng thị trường số hiện nay. Giải pháp toàn diện cho Khách hàng cập nhật các dịch vụ thịnh hành nhất.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="about" class="about-us section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="left-image wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/about-left-image.png" alt="">
                </div>
            </div>
            <div class="col-lg-6 align-self-center wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <div class="section-heading">
                    <h6><?=$config->getValue('admin_name')?></h6>
                    <h2>Nền Tảng Hoàn Hảo Cho Tiếp Thị <span>Truyền Thông Xã Hội</span></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="about-item">
                            <h4>70+</h4>
                            <h6>Dịch vụ</h6>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="about-item">
                            <h4>10.000+</h4>
                            <h6>Khách hàng hài lòng</h6>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="about-item">
                            <h4>7+</h4>
                            <h6>Năm phát triển</h6>
                        </div>
                    </div>
                </div>
                <p><?=$config->getValue('admin_name')?> Cung cấp các dịch vụ truyền thông xã hội như Instagram, Facebook, Youtube, TikTok, Shopee và nhiều nền tảng khác để quảng bá bản thân hoặc công ty của bạn. Nếu bạn đang tìm cách tăng cường sự hiện diện trực tuyến của mình, thì lựa chọn tốt nhất của bạn, nơi chúng tôi cung cấp các dịch vụ giúp bạn tăng cường sự hiện diện trực tuyến của mình trên TẤT CẢ các nền tảng truyền thông xã hội với mức giá rẻ nhất.</p>
                <div class="main-green-button"><a href="#">Đăng Ký Ngay</a></div>
            </div>
        </div>
    </div>
</div>

<div id="features" class="features section">
					<center><h2>Chúng tôi mang lại </h2><br><br></center>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="features-content">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="features-item first-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                                <div class="first-number number">
                                    <h6>01</h6>
                                </div>
                                <div class="icon"></div>
                                <h4>Dịch Vụ Chất Lượng</h4>
                                <div class="line-dec"></div>
                                <p>Dựa trên quá trình phát triển bền vững và được chứng thực bởi hàng ngàn khách hàng.</p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="features-item second-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                                <div class="second-number number">
                                    <h6>02</h6>
                                </div>
                                <div class="icon"></div>
                                <h4>Bảo Mật Thông Tin</h4>
                                <div class="line-dec"></div>
                                <p>Chúng tôi hoàn toàn sử dụng tài nguyên trao đổi like, follow người thật tự nhiên không nắm bất cứ thông tin gì từ khách hàng</p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="features-item first-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                                <div class="third-number number">
                                    <h6>03</h6>
                                </div>
                                <div class="icon"></div>
                                <h4>Chính Sách Bảo Hành</h4>
                                <div class="line-dec"></div>
                                <p>Các dịch vụ của chúng tôi cam kết có các chính sách bảo hành được liệt kê cụ thể từng dịch vụ.</p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="features-item second-feature last-features-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                                <div class="fourth-number number">
                                    <h6>04</h6>
                                </div>
                                <div class="icon"></div>
                                <h4>Hỗ Trợ Nhanh Chóng</h4>
                                <div class="line-dec"></div>
                                <p>Quy trình làm việc chuyên nghiệp, đảm bảo bàn giao mẫu logo đến cho khách hàng đúng thời hạn..</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<div id="contact" class="contact-us section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
                <form id="contact" action="#" method="post">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            <div class="section-heading">
                                <h6>Liên Hệ</h6>
                                <h2>Hãy liên hệ chúng tôi tư vấn !</h2>
                            </div>
                        </div>
<center>
                        <div class="col-lg-6">
                            <div class="contact-info">
                                <ul>
                                    <li>
                                        <div class="icon">
                                            <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/contact-icon-01.png" alt="email icon">
                                        </div>
                                        <a class="main-blue-button" href="https://www.facebook.com/<?=$config->getValue('fanpage')?>">Contact Facebook</a>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/contact-icon-02.png" alt="phone">
                                        </div>
                                        <a href="#"><?=$config->getValue('sdt')?></a>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <img src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/images/contact-icon-03.png" alt="location">
                                        </div>
                                        <a href="#"><?=$config->getValue('fb_admin')?></a>
                                    </li>
                                </ul>
                            </div>
                        </div></center>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright © 2021 <?=$config->getValue('admin_name')?>. All Rights Reserved.

                    <br>Web Designed by <a rel="nofollow" href="/" title="free CSS templates"><?=$config->getValue('admin_name')?></a></p>
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/vendor/jquery/jquery.min.js"></script>
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/js/owl-carousel.js"></script>
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/js/animation.js"></script>
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/js/imagesloaded.js"></script>
<script src="https://templatemo.com/templates/templatemo_563_seo_dream/assets/js/custom.js"></script>

</body>

<!-- Mirrored from templatemo.com/templates/templatemo_563_seo_dream/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Nov 2021 08:40:22 GMT -->
</html>
