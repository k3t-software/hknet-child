<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $configModel;

    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->configModel = new Config;
    }

    public function view(Request $req)
    {
        if (Auth::check()) {
            return redirect()->intended('/admin/dashboard');
        }
        if ($req->input('refer')) {
            return response()->redirectTo("/register?refer=" . $req->input("refer"));
            //                    return response()->view('admin.auth.login')->withCookie('refer', $req->input('refer'), 60 * 24);
        }

        $theme = $this->configModel->getValue('login_theme');

        $data['config']  = $this->configModel;
        $loginView = $theme === 'default' ? 'admin.auth.login' : 'template.login.RedLogin/signIn';
        return view($loginView, $data);
    }

    public function register(Request $req)
    {
        $data = [
            'refer' => ''
        ];

        if ($req->input('refer')) {
            $referCode = $req->input("refer");
            if (isset($referCode) && $referCode !== "") {
                $referCodeParse = explode("_", $referCode);
                $fromId = (int)(isset($referCodeParse[1]) ? $referCodeParse[1] : "0");

                $fromUser = DB::table("users")->where("id", $fromId)->limit(1)->get()->first();
                if (isset($fromUser)) {
                    $data['refer'] = $referCode;
                } else {
                    return response()->redirectTo("/register");
                }
            }

            $data['refer'] = $req->input('refer');
        }

        $theme = $this->configModel->getValue('login_theme');
        $data['config']  = $this->configModel;
        $loginView = $theme === 'default' ? 'admin.auth.register' : 'template.login.RedLogin.register';
        return view($loginView, $data);
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $req)
    {
        $arr = [
            'phone' => $req->input("phone"),
            'password' => $req->input("password"),
        ];
        if (Auth::attempt($arr)) {
            return response()->json(1, 200);
        } else {
            return response()->json(0, 200);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
