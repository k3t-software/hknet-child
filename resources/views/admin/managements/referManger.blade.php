@php

    $userRoles = ['<span class="badge badge-primary">1%</span>', '<span class="badge badge-primary">3%</span>', '<span class="badge badge-primary">7%</span>', '<span class="badge badge-primary">20%</span>'];
    $type = ['<span class="badge badge-success">Nạp tiền</span>', '<span class="badge badge-warning">Sử dụng dịch vụ</span>', '<span class="badge badge-danger">Chuyển tiền</span>', '<span class="badge badge-info">Nhận tiền</span>', '<span class="badge badge-primary">Hoàn tiền dịch vụ</span>', '<span class="badge badge-primary">[Ví VNĐ] Hoa hồng</span>']

@endphp

@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Tiếp Thị Liên Kết</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Tài khoản</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">


                <div class="row">
                    <!-- Column -->

                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- Row -->
                                <div class="row">
                                    <div class="col-8"><h2 class="text-warning">{!! number_format($user->balance_2) !!}
                                            đ
                                            <i class="ti-angle-up font-14 text-success"></i></h2>
                                        <h6>Số dư ví VNĐ</h6></div>
                                    <div class="col-4 align-self-center text-right p-l-0">
                                        <div id="sparklinedash">
                                            <canvas width="51" height="50"
                                                    style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- Row -->
                                <div class="row">
                                    <div class="col-8"><h2>{{$level1Count}}<i
                                                    class="ti-angle-up font-14 text-success"></i></h2>
                                        <h6>Khách hàng của bạn</h6></div>
                                    <div class="col-4 align-self-center text-right p-l-0">
                                        <div id="sparklinedash2">
                                            <canvas width="51" height="50"
                                                    style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->

                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- Row -->
                                <div class="row">
                                    <div class="col-8"><h2>{{$level2Count}}<i
                                                    class="ti-angle-up font-14 text-success"></i></h2>
                                        <h6>Tổng khách hàng con</h6></div>
                                    <div class="col-4 align-self-center text-right p-l-0">
                                        <div id="sparklinedash2">
                                            <canvas width="51" height="50"
                                                    style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->

                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- Row -->
                                <div class="row">
                                    <div class="col-8"><h2> {!! number_format($tongDoanhThu) !!} đ<i
                                                    class="ti-angle-down font-14 text-danger"></i></h2>
                                        <h6>Tổng doanh thu</h6></div>
                                    <div class="col-4 align-self-center text-right p-l-0">
                                        <div id="sparklinedash4">
                                            <canvas width="51" height="50"
                                                    style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">

                        <div class="table-responsive p-1">
                            <table class="table service-tablwa service-well">
                                <thead>
                                <tr>
                                    <th>Link kiếm tiền</th>
                                    <th>Hoa hồng khi nạp</th>
                                    <th>100 khách hàng đầu tiên</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="https://hknet.vn/login?refer={!! $referCode !!}">https://hknet.vn/login?refer={!! $referCode !!}</a>
                                    </td>
                                    <td>{!!$userRoles[Auth::user()->role]!!}</td>
                                    <td>200.000 vnd</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h4 class="card-title">Quản lý khách hàng</h4>
                            </div>
                        </div>
                        <div class="table-responsive p-1">
                            <table id="refer_user_table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Phone</th>
                                    <th class="text-center">Role</th>
                                    <th class="text-center">Số dư</th>
                                    <th class="text-center">Tổng nạp</th>
                                    <th class="text-center">Thời gian tạo</th>
                                    <th class="text-center">Khách Hàng</th>
                                </tr>
                                </thead>
                                <tbody id="refer_user_table">
                                </tbody>

                                <img style="margin: 0 auto; display: block; display: none; " class="loading-image"
                                     src="https://mir-s3-cdn-cf.behance.net/project_modules/max_632/04de2e31234507.564a1d23645bf.gif" />
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h4 class="card-title">Hoa hồng nhận được</h4>
                            </div>
                        </div>
                        <div class="table-responsive p-1">
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Loại</th>
                                    <th class="text-center">Số tiền</th>
                                    <th class="text-center" style="max-width: 200px;">Ghi chú</th>
                                    <th class="text-center">Thời gian</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($transactions as $transaction)
                                    <tr>
                                        <td class="text-center">{{$loop->index + 1}}</td>
                                        <td class="text-center">
                                            {!!$type[$transaction->type]!!}
                                        </td>

                                        <td class="text-center @if ($transaction->type == 0 || $transaction->type === 5)
                                                text-success
@else
                                                text-danger
@endif">
                                            @if ($transaction->type == 0 || $transaction->type === 5)
                                                +
                                            @endif
                                            {!!number_format($transaction->amount)!!}
                                        </td>

                                        <td class="text-center">
                                            {!!$transaction->note!!}
                                        </td>

                                        <td class="text-center">
                                            {{date('H:i:s d/m/Y', strtotime($transaction->created_at))}}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-container">

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
            integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.22.0/axios.min.js"
            integrity="sha512-m2ssMAtdCEYGWXQ8hXVG4Q39uKYtbfaJL5QMTbhl2kc6vYyubrKHhr6aLLXW4ITeXSywQLn1AhsAaqrJl8Acfg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="{{asset("admin/js/managements/referManger.js")}}"></script>

    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

    <script src="{{asset("admin/js/dashboard4.js")}}"></script>
@endsection
