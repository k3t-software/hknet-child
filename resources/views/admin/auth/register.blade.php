<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Original URL: http://themedesigner.in/demo/admin-press/main/pages-register2.html
    Date Downloaded: 11/30/2018 3:41:57 PM !-->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("admin/images/favicon.png")}}" />
    <title>HKNet - Đăng ký</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset("admin/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet" />
    <!-- You can change the theme colors from here -->
    <link href="{{asset("admin/css/colors/blue.css")}}" id="theme" rel="stylesheet" />
    <link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewbox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register"
         style="min-height: 100vh; background-image:url({{asset("admin/images/auth-bg.jpg")}});">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-material" id="registerForm">
                {{csrf_field() }}
                <h3 class="box-title m-t-10 m-b-0">HKNet.Vn</h3><small>Tạo tài khoản</small>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input class="form-control" name="name" type="text" required="" placeholder="Họ tên" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="phone" type="text" required="" placeholder="Số điện thoại" />
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="password" type="password" required=""
                               placeholder="Mật khẩu" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="passwordConfirm" type="password" required=""
                               placeholder="Xác nhận mật khẩu" />
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="giftCode" type="text" required=""
                               placeholder="Giftcode (nếu có)" />
                    </div>
                </div>


                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="referCode" type="text" readonly=""
                               placeholder="Mã giới thiệu" value="{!!$refer!!}" required=""/>
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                                id="registerBtn" type="button">Đăng ký
                        </button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Đã có tài khoản ? <a href="/login" class="text-info m-l-5"><b>Đăng nhập</b></a></p>
                    </div>

                </div>
            </form>
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset("admin/plugins/jquery/jquery.min.js")}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset("admin/plugins/bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("admin/plugins/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset("admin/js/jquery.slimscroll.js")}}"></script>
<!--Wave Effects -->
<script src="{{asset("admin/js/waves.js")}}"></script>
<!--Menu sidebar -->
<script src="{{asset("admin/js/sidebarmenu.js")}}"></script>
<!--stickey kit -->
<script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
<script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
<!--Custom JavaScript -->
<script src="{{asset("admin/js/custom.min.js")}}"></script>
<script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>

<script src="{{asset("admin/js/features/register.js")}}"></script>


<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{asset("admin/plugins/styleswitcher/jQuery.style.switcher.js")}}"></script>
</body>

</html>
