@php
    $status = ['<span class="badge badge-success">Đang hoạt động</span>', '<span class="badge badge-warning">Đang bảo trì</span>']
@endphp

@extends("admin.layouts.master")
@section("content")


    <style>
        .box-body {
            padding: 0px;
            border-radius: 0 0 3px 3px;

            margin-bottom: 15px;
            padding-top: 10px;
        }

        .box-service img {
            height: 50px;
        }

        .box-service font.text-service {
            font-size: 15px;
        }

        .box-service-panel {
            border: 2px solid #098dff;
            border-radius: 8px;
        }

        .box-service-panel:hover {
            cursor: pointer;
            box-shadow: 0 0 0 1.5px rgba(11, 142, 255, 1);
        }

        .box-service-panel .panel-body a {
            text-decoration: none;
            color: inherit;
        }

        body {
            margin-top: 1px;
        }

        /* Social feed */
        .social-feed-separated .social-feed-box {
            margin-left: 62px;
        }

        .social-feed-separated .social-avatar {
            float: left;
            padding: 0;
        }

        .social-feed-separated .social-avatar img {
            width: 52px;
            height: 52px;
            border: 1px solid #e7eaec;
        }

        .social-feed-separated .social-feed-box .social-avatar {
            padding: 15px 15px 0 15px;
            float: none;
        }

        .social-feed-box {
            /*padding: 15px;*/
            border: 1px solid #e7eaec;
            background: #fff;
            margin-bottom: 15px;
        }

        .article .social-feed-box {
            margin-bottom: 0;
            border-bottom: none;
        }

        .article .social-feed-box:last-child {
            margin-bottom: 0;
            border-bottom: 1px solid #e7eaec;
        }

        .article .social-feed-box p {
            font-size: 13px;
            line-height: 18px;
        }

        .social-action {
            margin: 15px;
        }

        .social-avatar {
            padding: 15px 15px 0 15px;
        }

        .social-comment .social-comment {
            margin-left: 45px;
        }

        .social-avatar img {
            height: 40px;
            width: 40px;
            margin-right: 10px;
        }

        .social-avatar .media-body a {
            font-size: 14px;
            display: block;
        }

        .social-body {
            padding: 15px;
        }

        .social-body img {
            margin-bottom: 10px;
        }

        .social-footer {
            border-top: 1px solid #e7eaec;
            padding: 10px 15px;
            background: #f9f9f9;
        }

        .social-footer .social-comment img {
            width: 32px;
            margin-right: 10px;
        }

        .social-comment:first-child {
            margin-top: 0;
        }

        .social-comment {
            margin-top: 15px;
        }

        .social-comment textarea {
            font-size: 12px;
        }


        .form-control, .single-line {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 6px 12px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 100%;
            font-size: 14px;
        }


        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }

        .ibox.collapsed .ibox-content {
            display: none;
        }

        .ibox.collapsed .fa.fa-chevron-up:before {
            content: "\f078";
        }

        .ibox.collapsed .fa.fa-chevron-down:before {
            content: "\f077";
        }

        .ibox:after,
        .ibox:before {
            display: table;
        }

        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }

        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }

        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }
    </style>

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{{number_format(Auth::user()->balance)}} đ</h2>
                                <h6>Số dư</h6></div>
                            <div class="col-4 align-self-center text-right  p-l-0">
                                <div id="sparklinedash3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2 class="">{{Auth::user()->CountOrderByUserId(Auth::user()->id, -1)}}
                                    <i class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Đơn hàng</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{{Auth::user()->CountOrderByUserId(Auth::user()->id, 2)}} <i
                                            class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Đơn hoàn thành</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2> {{number_format(-Auth::user()->GetTotal(Auth::user()->id, 1))}} <i
                                            class="ti-angle-down font-14 text-danger"></i></h2>
                                <h6>Tổng chi</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="flex-direction: row-reverse;">
            <div class="col-6">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
                      rel="stylesheet">

            </div>

            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                        @foreach($newfeeds as $newfeed)
                            <div class="social-feed-box">
                                <div class="social-avatar">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="https://bootdey.com/img/Content/avatar/avatar1.png">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Admin
                                        </a>
                                        <small class="text-muted">
                                            {{date('H:i:s d/m/Y', strtotime($newfeed->created_at))}}
                                        </small>
                                    </div>
                                </div>
                                <div class="social-body">
                                    <h3>{{$newfeed['title']}}</h3>
                                    {!! $newfeed['content'] !!}
                                    <br />
                                    <br />
<div class="" style="white-space: pre-wrap; border-top: 1px solid rgb(224, 224, 224); padding: 15px 20px;"><img class="j1lvzwm4" height="24" src="data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e" width="24"><img class="j1lvzwm4" height="24" src="data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='10.25%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FEEA70'/%3e%3cstop offset='100%25' stop-color='%23F69B30'/%3e%3c/linearGradient%3e%3clinearGradient id='d' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23472315'/%3e%3cstop offset='100%25' stop-color='%238B3A0E'/%3e%3c/linearGradient%3e%3clinearGradient id='e' x1='50%25' x2='50%25' y1='0%25' y2='81.902%25'%3e%3cstop offset='0%25' stop-color='%23FC607C'/%3e%3cstop offset='100%25' stop-color='%23D91F3A'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.921365489 0 0 0 0 0.460682745 0 0 0 0 0 0 0 0 0.35 0'/%3e%3c/filter%3e%3cpath id='b' d='M16 8A8 8 0 110 8a8 8 0 0116 0'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='url(%23d)' d='M3 8.008C3 10.023 4.006 14 8 14c3.993 0 5-3.977 5-5.992C13 7.849 11.39 7 8 7c-3.39 0-5 .849-5 1.008'/%3e%3cpath fill='url(%23e)' d='M4.541 12.5c.804.995 1.907 1.5 3.469 1.5 1.563 0 2.655-.505 3.459-1.5-.551-.588-1.599-1.5-3.459-1.5s-2.917.912-3.469 1.5'/%3e%3cpath fill='%232A3755' d='M6.213 4.144c.263.188.502.455.41.788-.071.254-.194.369-.422.371-.78.011-1.708.255-2.506.612-.065.029-.197.088-.332.085-.124-.003-.251-.058-.327-.237-.067-.157-.073-.388.276-.598.545-.33 1.257-.48 1.909-.604a7.077 7.077 0 00-1.315-.768c-.427-.194-.38-.457-.323-.6.127-.317.609-.196 1.078.026a9 9 0 011.552.925zm3.577 0a8.953 8.953 0 011.55-.925c.47-.222.95-.343 1.078-.026.057.143.104.406-.323.6a7.029 7.029 0 00-1.313.768c.65.123 1.363.274 1.907.604.349.21.342.44.276.598-.077.18-.203.234-.327.237-.135.003-.267-.056-.332-.085-.797-.357-1.725-.6-2.504-.612-.228-.002-.351-.117-.422-.37-.091-.333.147-.6.41-.788z'/%3e%3c/g%3e%3c/svg%3e" width="24"><img class="j1lvzwm4" height="24" src="data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%23FF6680'/%3e%3cstop offset='100%25' stop-color='%23E61739'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0.710144928 0 0 0 0 0 0 0 0 0 0.117780134 0 0 0 0.349786932 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 100 16A8 8 0 008 0z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M10.473 4C8.275 4 8 5.824 8 5.824S7.726 4 5.528 4c-2.114 0-2.73 2.222-2.472 3.41C3.736 10.55 8 12.75 8 12.75s4.265-2.2 4.945-5.34c.257-1.188-.36-3.41-2.472-3.41'/%3e%3c/g%3e%3c/svg%3e" width="24"><span class="text-secondary" style="display: inline-block; width: 40%;"> 467K</span><br><div style="position: relative; border-top: 1px solid rgb(224, 224, 224); border-bottom: 1px solid rgb(224, 224, 224); margin-top: 10px;"><div class="font-bold text-secondary" style="display: inline-block; width: 33%; text-align: center; padding: 10px;"><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 1024 1024" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg" style="font-size: 24px; vertical-align: -4px;"></svg> Thích</div><div class="font-bold text-secondary" style="display: inline-block; width: 34%; text-align: center; padding: 10px;"><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg" style="font-size: 24px;"></svg> Bình luận</div><div class="font-bold text-secondary" style="display: inline-block; width: 33%; text-align: center; padding: 10px;"><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg" style="font-size: 24px; vertical-align: -4px;"></svg> Chia sẻ</div></div><div style="position: relative;"><img src="https://w7.pngwing.com/pngs/219/345/png-transparent-computer-software-computer-icons-team-members-photography-team-logo.png" width="40" alt="user" class="img-avatars rounded-circle border mr-2" style="display: inline-block; margin-top: 10px;"><input class="inputwritecomment" placeholder="Viết bình luận..."  style="display: inline-block; padding-left: 10px; border-radius: 18px; background-color: rgb(230, 235, 244); border: 1px solid rgb(230, 235, 244); width: 95%; height: 40px; margin-top: 15px; position: absolute;"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg" style="position: absolute; right: 14px; top: 20px; font-size: 28px;"><circle cx="12" cy="12" r="10"></circle><path d="M8 14s1.5 2 4 2 4-2 4-2"></path><line x1="9" y1="9" x2="9.01" y2="9"></line><line x1="15" y1="9" x2="15.01" y2="9"></line></svg><span class="" style="position: absolute; top: 60px;">Nhấn Enter để đăng.</span></div></div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>


                @php
                    $bannerValues = json_decode($banner->value, true);
                @endphp
                @if(isset($bannerValues['isShow']) && $bannerValues['isShow'] === 'on')
                    <div class="modal fade" id="dashboardBanner" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 10%;">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{$bannerValues['title']}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! $bannerValues['html'] !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif



            <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

        @endsection

        @section('scripts')
            <!-- ============================================================== -->
                <!-- This page plugins -->
                <!-- ============================================================== -->

                <script>
                    @if(isset($bannerValues) && isset($bannerValues['isShow']))
                    const banner = {
                        id: '{{strtotime($banner->updated_at)}}',
                        isShow: {{$bannerValues['isShow'] === 'on' ? 'true' : 'false'}},
                        title: `{{$bannerValues['title']}}`,
                        html: `{{$bannerValues['html']}}`
                    };

                    if (banner.isShow && !localStorage.getItem('isShowedModal_' + banner.id)) {
                        $('#dashboardBanner').modal('show');
                    }

                    $('#dashboardBanner').on('hidden.bs.modal', function () {
                        localStorage.setItem('isShowedModal_' + banner.id, true);
                    });
                    @endif


                </script>


                <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

                <script src="{{asset("admin/js/dashboard4.js")}}"></script>



    {{-- <script src="{{asset("admin/js/dashboard2.js")}}"></script> --}}
@endsection
