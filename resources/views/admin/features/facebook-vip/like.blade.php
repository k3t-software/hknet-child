@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
	$role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];

@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">VIP Like</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">VIP Like</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tạo mới VIP Like</h4>
						<h6 class="card-subtitle">Chức năng VIP Like cho tài khoản facebook</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Facebook profile</label>
										<div class="input-group">
											<input type="text" class="form-control" id = "profileLink" placeholder="Nhập link hoặc profile id">
											<div class="input-group-append">
												<button class="btn btn-info" id = "getProfileBtn" type="button">Lấy thông tin</button>
											</div>
										</div>
									</div>

									<div class = "profile-info">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile Name</button>
											</div>
											<input name = "name" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Profile ID</button>
											</div>
											<input name ="id" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<input type="hidden" value ="{{$service[$role[Auth::user()->role]]}}" name="price">

										<div class="row">
											<div class="col-12 mb-3">
												<label>Gói VIP</label>
												<select class="form-control" name = "package">
													<option value="50"> 50 LIKE</option>
													<option value="100"> 100 LIKE</option>
													<option value="150"> 150 LIKE</option>
													<option value="200"> 200 LIKE</option>
													<option value="300"> 300 LIKE</option>
													<option value="350"> 350 LIKE</option>
													<option value="400"> 400 LIKE</option>
													<option value="450"> 450 LIKE</option>
													<option value="500"> 500 LIKE</option>
													<option value="600"> 600 LIKE</option>
													<option value="700"> 700 LIKE</option>
													<option value="800"> 800 LIKE</option>
													<option value="900"> 900 LIKE</option>
													<option value="1000"> 1000 LIKE</option>
												</select>

											</div>


											<div class="col-12 mb-3">
													<label>Thời hạn</label>
													<select class="form-control" name = "period">
														<option value="30"> 30 Ngày</option>
														<option value="60"> 60 Ngày</option>
														<option value="90"> 90 Ngày</option>
													</select>

											</div>

											<div class="col-12 mb-3">
												<label>Số bài trong ngày</label>
												<select class="form-control" name = "posts">
													<option value="5">5</option>
													<option value="10">10</option>

												</select>

											</div>



										</div>

										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value =0 name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "like" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>

										</div>

									</div>



								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
									- Like Clone Có Avatar<br><br>
									- Mỗi bài đăng tối thiểu cách nhau 1 giờ<br><br>

								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-subtitle">Danh sách các đơn hàng của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thông Báo</th>
											@if ($service->id == '15')
											<th class = "text-right">Ngày hết hạn</th>
											@endif
											<th class = "text-right">Thời gian cài</th>
                                            <th class = "text-right">Hành động</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class="text-right"><span class="badge btn btn-outline-danger">{{$order->notify}}</span></td>
												@if ($service->id == '15')
												<td class = "text-right">
													{{date('d/m/Y', strtotime($order->created_at) + $order->duration)}}
                                                    <br />
                                                    @php
                                                    $start = strtotime('now');
                                                    $end = strtotime($order->created_at) + $order->duration;
                                                    $days_between = ceil(abs($end - $start) / 86400);
                                                    echo '( '. $days_between . ' day )';
                                                     @endphp
												</td>
												@endif
												<td class = "text-right">{{date('d/m/Y', strtotime($order->first_add_at))}}</td>
                                                <td class = "text-right">
                                                    <button type="button" class="btn btn-primary btn-sm" data-action="reOrder" data-orderId="{{$order->id}}">Gia hạn</button>

                                                        <button type="button" class="btn btn-danger btn-sm" data-action="delete" data-orderId="{{$order->id}}">Xoá</button>
                                                </td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
    <div class="modal" id="reOrderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="reOrderForm">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Gia hạn thêm</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <input type="hidden" value="" id="reOrderId"/>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="col-12 mb-3">
                            <label>Thời hạn</label>
                            <select class="form-control" name="period" id="reOrderPeriod">
                                <option value=""></option>
                                <option value="30"> 30 Ngày</option>
                                <option value="60"> 60 Ngày</option>
                                <option value="90"> 90 Ngày</option>
                            </select>
                        </div>


                        <hr />

                        <h3>Tổng tiền : <span id="totalReOrder"></span></h3>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary" style="float: left;">Gia hạn</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{asset("admin/js/facebook-vip/like.js")}}"></script>
@endsection