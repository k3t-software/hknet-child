<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from templatemo.com/templates/templatemo_568_digimedia/homepage_3.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Nov 2021 08:51:57 GMT -->
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;display=swap"
          rel="stylesheet">

    <title><?=$title?></title>


    <!-- Bootstrap core CSS -->
    <link href="https://templatemo.com/templates/templatemo_568_digimedia/vendor/bootstrap/css/bootstrap.min.css"
          rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_568_digimedia/assets/css/fontawesome.css">
    <link rel="stylesheet"
          href="https://templatemo.com/templates/templatemo_568_digimedia/assets/css/templatemo-digimedia-v3.css">
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_568_digimedia/assets/css/animated.css">
    <link rel="stylesheet" href="https://templatemo.com/templates/templatemo_568_digimedia/assets/css/owl.css">
    <!--

    TemplateMo 568 DigiMedia

    https://templatemo.com/tm-568-digimedia

    -->
</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!-- ***** Preloader End ***** -->


<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="/" class="logo">
                        <img src="<?=$config->getValue('logo')?>" alt="<?=$config->getValue('title')?>" height="70px">
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li class="scroll-to-section"><a href="#top" class="active">Home</a></li>
                        <li class="scroll-to-section"><a href="#about">Thông tin</a></li>
                        <li class="scroll-to-section"><a href="#services">Dịch Vụ</a></li>
                        <li class="scroll-to-section"><a href="#contact">Liên Hệ</a></li>
                        <li class="scroll-to-section">
                            <div class="border-first-button"><a href="/login">Đăng nhập</a></div>
                        </li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 align-self-center">
                        <div class="left-content show-up header-text wow fadeInLeft" data-wow-duration="1s"
                             data-wow-delay="1s">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6><?=$config->getValue('admin_name')?></h6>
                                    <h2>Trang web truyền thông xã hội tốt nhất</h2>
                                    <p>Chúng tôi có các dịch vụ tốt nhất và rẻ nhất như lượt thích trên instagram, lượt theo dõi, lượt thích và người theo dõi trên facebook, lượt xem và lượt thích trên youtube và bất kỳ dịch vụ truyền thông xã hội nào bạn cần.</p>
                                </div>
                                <div class="col-lg-12">
                                    <div class="border-first-button scroll-to-section">
                                        <a href="/login">Đăng Nhập / Đăng Ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/slider-dec-v3.png"
                                 alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="about" class="about section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="about-left-image  wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/about-dec-v3.png"
                                 alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 align-self-center  wow fadeInRight" data-wow-duration="1s"
                         data-wow-delay="0.5s">
                        <div class="about-right-content">
                            <div class="section-heading">
                                <h6><?=$config->getValue('admin_name')?></h6>
                                <h4>Nền Tảng Hoàn Hảo Cho Tiếp Thị<em> Truyền Thông Xã Hội</em></h4>
                                <div class="line-dec"></div>
                            </div>
                            <p>Cung cấp các dịch vụ truyền thông xã hội như Instagram, Facebook, Youtube, TikTok, Shopee và nhiều nền tảng khác để quảng bá bản thân hoặc công ty của bạn. Nếu bạn đang tìm cách tăng cường sự hiện diện trực tuyến của mình, thì lựa chọn tốt nhất của bạn, nơi chúng tôi cung cấp các dịch vụ giúp bạn tăng cường sự hiện diện trực tuyến của mình trên TẤT CẢ các nền tảng truyền thông xã hội với mức giá rẻ nhất.</p>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4">
                                    <div class="skill-item first-skill-item wow fadeIn" data-wow-duration="1s"
                                         data-wow-delay="0s">
                                        <div class="progress" data-percentage="90">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                                            <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                                            <div class="progress-value">
                                                <div>
                                                    99%<br>
                                                    <span>Tài nguyên thực</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4">
                                    <div class="skill-item second-skill-item wow fadeIn" data-wow-duration="1s"
                                         data-wow-delay="0s">
                                        <div class="progress" data-percentage="80">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                                            <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                                            <div class="progress-value">
                                                <div>
                                                    80%<br>
                                                    <span>Khách hàng trở lại</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4">
                                    <div class="skill-item third-skill-item wow fadeIn" data-wow-duration="1s"
                                         data-wow-delay="0s">
                                        <div class="progress" data-percentage="90">
                        <span class="progress-left">
                          <span class="progress-bar"></span>
                        </span>
                                            <span class="progress-right">
                          <span class="progress-bar"></span>
                        </span>
                                            <div class="progress-value">
                                                <div>
                                                    90%<br>
                                                    <span>Khách hàng hài lòng</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="services" class="services section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
                    <h6>Dịch Vụ</h6>
                    <h4>Các Dịch Vụ <em><?=$config->getValue('admin_name')?></em> Cung Cấp</h4>
                    <div class="line-dec"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="naccs">
                    <div class="grid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu">
                                    <div class="first-thumb active">
                                        <div class="thumb">
                                            <span class="icon"><img
                                                        src="https://hknet.vn/admin/images/dichvu/facebook_vip.png"
                                                        alt=""></span>
                                            Facebook
                                        </div>
                                    </div>
                                    <div>
                                        <div class="thumb">
                                            <span class="icon"><img
                                                        src="https://hknet.vn/admin/images/dichvu/tiktok.png"
                                                        alt=""></span>
                                            Tiktok
                                        </div>
                                    </div>
                                    <div>
                                        <div class="thumb">
                                            <span class="icon"><img
                                                        src="https://hknet.vn/admin/images/dichvu/youtubeq.PNG"
                                                        alt=""></span>
                                            Youtube
                                        </div>
                                    </div>
                                    <div>
                                        <div class="thumb">
                                            <span class="icon"><img
                                                        src="https://hknet.vn/admin/images/dichvu/instagram.png"
                                                        alt=""></span>
                                            Instagram
                                        </div>
                                    </div>
                                    <div class="last-thumb">
                                        <div class="thumb">
                                            <span class="icon"><img
                                                        src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/service-icon-01.png"
                                                        alt=""></span>
                                            Dịch Vụ Khác
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <ul class="nacc">
                                    <li class="active">
                                        <div>
                                            <div class="thumb">
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="left-text">
                                                            <h4>Dịch Vụ FaceBook</h4>
                                                            <p> <?=$config->getValue('admin_name')?> tự hào là đơn vị đối tác cung cấp các dịch vụ facebook một cách nhanh chóng và toàn diện đáp ứng đầy đủ yêu cầu của khách hàng khi kinh doanh phát triển trên internet.</p>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="right-image">
                                                            <img src="https://1.bp.blogspot.com/-d6KGiCYxIxA/YVhf3yAnyuI/AAAAAAAAW-c/ASNq6utURu04fMbZL3EUZcQEe5_Km8ARgCLcBGAsYHQ/s0/tanglike.png"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <div class="thumb">
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="left-text">
                                                            <h4>Dịch Vụ Tiktok</h4>
                                                            <p>Hiện nay mạng xã hội Tiktok đang phát triển rát thần tốc và để đáp ứng được nhu cầu người sử dụng nhằm tăng độ uy tín và tiếp cận cho thương hiệu, đó chính là lý do chúng tôi ra đời và phục vụ !  </p>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="right-image">
                                                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/services-image-02.jpg"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <div class="thumb">
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="left-text">
                                                            <h4>Dịch Vụ Youtube</h4>
                                                            <p>Chúng tối cung cấp các dịch vụ tăng lượt like, sub, lượt xem nhằm tăng độ uy tín và tin tưởng cho khách hàng và ngoài ra cũng có thể dùng để đạt điều kiện bật kiếm tiền cho kênh ! </p>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="right-image">
                                                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/services-image-03.jpg"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <div class="thumb">
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="left-text">
                                                            <h4>Dịch Vụ Instagram</h4>
                                                            <p>Dịch vụ Instagram hiện đang là cách giúp tài khoản Instagram của bạn tăng khả năng hiển thị khi người dùng tìm kiếm, tiếp cận thêm nhiều khách hàng, tăng khả năng bán hàng.</p>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="right-image">
                                                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/services-image-04.jpg"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <div class="thumb">
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="left-text">
                                                            <h4>Nhiều Dịch Vụ Khác</h4>
                                                            <p>Ngoài ra chúng tôi còn cung cấp nhiều dịch vụ khác nhằm đáp ứng thị trường số hiện nay. Giải pháp toàn diện cho Khách hàng cập nhật các dịch vụ thịnh hành nhất</p>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 align-self-center">
                                                        <div class="right-image">
                                                            <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/services-image.jpg"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="contact" class="contact-us section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <h6>Liên Hệ</h6>
                    <h4>Hãy liên hệ chúng tôi <em>bây giờ</em></h4>
                    <div class="line-dec"></div>
                </div>
            </div>
            <div class="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
                <form id="contact" action="#" method="post">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="contact-dec">
                                <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/contact-dec-v3.png"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div id="map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7864452.3833377445!2d101.4132682470241!3d15.747719357779795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31157a4d736a1e5f%3A0xb03bb0c9e2fe62be!2zVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1638199378416!5m2!1svi!2s"
                                        width="100%" height="636px" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="fill-form">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="info-post">
                                            <div class="icon">
                                                <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/phone-icon.png"
                                                     alt="">
                                                <a href="#"><?=$config->getValue('sdt')?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="info-post">
                                            <div class="icon">
                                                <img src="https://hknet.vn/admin/images/dichvu/facebook_vip.png"
                                                     alt="">
                                                <a href="https://www.facebook.com/<?=$config->getValue('fanpage')?>"><?=$config->getValue('admin_name')?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="info-post">
                                            <div class="icon">
                                                <img src="https://templatemo.com/templates/templatemo_568_digimedia/assets/images/location-icon.png"
                                                     alt="">
                                                <a href="#"><?=$config->getValue('fb_admin')?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <fieldset>
                                            <input type="name" name="name" id="name" placeholder="Name"
                                                   autocomplete="on" required>
                                        </fieldset>
                                        <fieldset>
                                            <input type="text" name="email" id="email" pattern="[^ @]*@[^ @]*"
                                                   placeholder="Your Email" required="">
                                        </fieldset>
                                        <fieldset>
                                            <input type="subject" name="subject" id="subject" placeholder="Subject"
                                                   autocomplete="on">
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-6">
                                        <fieldset>
                                            <textarea name="message" type="text" class="form-control" id="message"
                                                      placeholder="Message" required=""></textarea>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-12">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="main-button ">Send Message
                                                Now
                                            </button>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright © 2022 <?=$config->getValue('admin_name')?> Co., Ltd. All Rights Reserved.
                    <br>Design: <a href="/" target="_parent"
                                   title="free css templates"> <?=$config->getValue('admin_name')?></a></p>
            </div>
        </div>
    </div>
</footer>


<!-- Scripts -->
<script src="https://templatemo.com/templates/templatemo_568_digimedia/vendor/jquery/jquery.min.js"></script>
<script src="https://templatemo.com/templates/templatemo_568_digimedia/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://templatemo.com/templates/templatemo_568_digimedia/assets/js/owl-carousel.js"></script>
<script src="https://templatemo.com/templates/templatemo_568_digimedia/assets/js/animation.js"></script>
<script src="https://templatemo.com/templates/templatemo_568_digimedia/assets/js/imagesloaded.js"></script>
<script src="https://templatemo.com/templates/templatemo_568_digimedia/assets/js/custom.js"></script>

</body>

<!-- Mirrored from templatemo.com/templates/templatemo_568_digimedia/homepage_3.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Nov 2021 08:51:57 GMT -->
</html>
