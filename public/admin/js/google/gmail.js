$(function () {
    'use strict';

    $('#orderBtn').on('click', function () {
        const button = $(this);
        button.attr('disabled', true);
        const buffType = button.data('function');
        const backupBtn = button.html();
        const loadingBtn =
            '<i class="fa fa-spin fa-circle-o-notch"></i> Thực hiện';
        button.html(loadingBtn);

        const form = $('#infoForm');

        let formData = form.serializeArray();
        let data = {};
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        data.service_id = button.data('service');

        $.ajax({
            url: `/admin/dichvu/google/${buffType}`,
            method: 'POST',
            dataType: 'json',
            data
        }).always(function (data) {
            if (data.success == 1) {
                swal(
                    {
                        title: 'Thành công!',
                        text: data.message,
                        type: 'success'
                    },
                    function () {
                        location.reload(true);
                    }
                );
            } else {
                button.attr('disabled', false);
                button.html(backupBtn);
                swal(
                    {
                        title: 'Failed!',
                        text: data.message,
                        type: 'error'
                    },
                    function () {
                        button.attr('disabled', false);
                    }
                );
            }
            button.html(backupBtn);
        });
    });
});

$('#amount').change(e => {
    const amount = e.target.value;
    const price = $('#price').val();
    $('#total_price').val(amount * price);
});

