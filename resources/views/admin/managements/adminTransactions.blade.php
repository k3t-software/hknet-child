@php
    $type = ['<span class="badge badge-success">Nạp tiền</span>', '<span class="badge badge-warning">Sử dụng dịch vụ</span>', '<span class="badge badge-danger">Chuyển tiền</span>', '<span class="badge badge-info">Nhận tiền</span>', '<span class="badge badge-primary">Hoàn tiền dịch vụ</span>', '<span class="badge badge-primary">[Ví 2] Hoa hồng</span>'];
@endphp

@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Giao dịch toàn hệ thống</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Giao dịch hệ thống</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{!!number_format($tokenInfo['balance']) !!}</h2>
                                <h6>Số dư site chính</h6></div>
                            <div class="col-4 align-self-center text-right  p-l-0">
                                <div id="sparklinedash3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2 class="">{!! number_format($totalBalance)  !!}
                                    <i class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Tổng tiền hệ thống</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{!! number_format($totalProfit)  !!}<i
                                            class="ti-angle-up font-14 text-success"></i></h2>
                                <h6>Lợi nhuận</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <!-- Row -->
                        <div class="row">
                            <div class="col-8"><h2>{!! number_format($totalOrder) !!}<i
                                            class="ti-angle-down font-14 text-danger"></i></h2>
                                <h6>Tổng đơn hàng</h6></div>
                            <div class="col-4 align-self-center text-right p-l-0">
                                <div id="sparklinedash4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            tr td {
                text-align: center;
            }
        </style>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Giao dịch đơn hàng</h3>
                            </div>

                        </div>
                        <div class="table-responsive p-1">
                            <table class="myTableCustomCustom table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">IDs</th>
                                    <th class="text-center">Ngày thực hiện</th>
                                    <th class="text-center" style="max-width: 200px;">Người thực hiện</th>
                                    <th class="text-center">Dịch vụ</th>
                                    <th class="text-center">Số lượng</th>
                                    <th class="text-right">Số tiền</th>
                                    <th class="text-right">Lợi nhuận</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{$order->id}}_{{$order->api_id}}</td>
                                        <td>{{$order->created_at}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->service_name}}</td>
                                        <td>{{$order->quantity}}</td>
                                        <td>{!! number_format($order->total) !!}</td>
                                        <td>{!! number_format($order->profit) !!}</td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Danh sách giao dịch toàn hệ thống</h3>
                            </div>

                        </div>
                        <div class="table-responsive p-1">
                            <table class="myTableCustomCustom table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">User</th>
                                    <th class="text-center">Loại</th>
                                    <th class="text-right">Số tiền</th>
                                    <th class="text-center" style="max-width: 200px;">Ghi chú</th>
                                    <th class="text-right">Thời gian</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($userTransactions as $transaction)
                                    <tr>
                                        <td class="text-center">{{$transaction->id}}</td>
                                        <td class="text-center">
                                            {!!$transaction->name!!} <br />
                                            [ {!!$transaction->phone!!}]
                                        </td>
                                        <td class="text-center">
                                            {!!$type[$transaction->type]!!}
                                        </td>

                                        <td class="text-right @if ($transaction->type == 0)
                                                text-success
@else
                                                text-danger
@endif">
                                            @if ($transaction->type == 0)
                                                +
                                            @endif
                                            {!!number_format($transaction->amount)!!}
                                        </td>

                                        <td class="text-center">
                                            {!!$transaction->note!!}
                                        </td>

                                        <td class="text-right">
                                            {{date('H:i:s d/m/Y', strtotime($transaction->created_at))}}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('scripts')
    {{-- <script src = "{{asset("admin/js/features/services.js")}}"></script> --}}


    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

    <script src="{{asset("admin/js/dashboard4.js")}}"></script>
@endsection
