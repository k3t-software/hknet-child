<?php

namespace App\Helpers;

class Profile extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function getInfo()
    {
        return $this->get("/info", []);
    }

    public function getPrices()
    {
        return $this->get("/prices", []);
    }

    public function getRefunds()
    {
        return $this->get("/refunds", []);
    }
}
