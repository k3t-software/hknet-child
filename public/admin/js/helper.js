$(function () {
    'use strict';
    $('.form_ajax').submit(e => {
        const form = $(e.target);
        const submitButton = form.find('[type="submit"]');
        const url = form.attr('action');

        submitButton.attr('disabled', true);

        let values = {};
        const formData = form.serializeArray();
        $(formData).each(function (index, obj) {
            values[obj.name] = obj.value;
        });

        $.ajax({
            type: 'POST',
            url,
            data: values
        }).then(res => {
            const message = res && res.message;
            swal(
                {
                    title: 'Thành công !!',
                    text: message,
                    type: 'success'
                }, () => {
                    window.location.reload();
                });
        }).catch(err => {
            const message = err.responseJSON && err.responseJSON.message || 'Lỗi không xác định';
            swal({
                title: 'Lỗi !!',
                text: message,
                type: 'error'
            });
        }).always((e) => {
            submitButton.attr('disabled', false);
        });
        e.preventDefault();
    });

});
