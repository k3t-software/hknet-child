<?php

namespace App\Http\Controllers;

use App\Config;

class GuestController extends Controller
{

    private $configModel;

    public function __construct()
    {
        $this->configModel = new Config;
    }

    public function landingPage()
    {
        $token = $this->configModel->getValue('token');

        if ($token === "") {
            return response()->redirectTo('/install');
        }

        $data['title'] = $this->configModel->getValue('title');
        $data['config'] = $this->configModel;
        $theme = $this->configModel->getValue('landing_theme');

        if ($theme !== null) {
            return view("template/landing/$theme/index", $data);
        }
        return response()->redirectTo('/login');

    }

    public function loginPage()
    {
        return view('template/login/FlexStart/index');
    }

}
