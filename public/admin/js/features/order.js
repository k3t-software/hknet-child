$(function () {
    'use strict';

    const balance = parseInt($('#balance').text());
    $('#balance').html(
        balance.toLocaleString('en-US', { style: 'currency', currency: 'VND' })
    );

    $('#getID').on('click', function () {
        const profile = $('#profileLink').val();

        $('.profile-info [name=id]').val(Convert(profile));
    });

    $('.order-info [name=amount]').on('change', function () {
        const price = $(this).data('price');
        const amount = $(this).val();

        $('.order-info [name=total]').val(price * amount);
    });

    $('table').on('click', '.viewDetail', function () {
        $('#commentDetails').html($(this).data('details'));
    });

    $('table').on('click', '.deleteBtn', function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data('id');
        swal(
            {
                title: 'Xác nhận ?',
                text: 'Bạn có muốn xoá đơn hàng này không ? ',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Có',
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: `/admin/orders/delete`,
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        swal(
                            {
                                title: 'Completed!',
                                text: 'Xoá đơn hàng thành công',
                                type: 'success'
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                    error: function (data) {
                        swal(
                            {
                                title: 'Failed!',
                                text: `Đã có lỗi xảy ra !`,
                                type: 'error'
                            },
                            function () {
                                button.attr('disabled', false);
                            }
                        );
                    }
                });
            }
        );
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};

const Convert = (id) => {
    var result = null;
    var post_id = id['match'](/(.*)\/posts\/([0-9]{8,})/);
    var photo_id = id['match'](/(.*)\/photo.php\?fbid=([0-9]{8,})/);
    var video_id = id['match'](/(.*)\/video.php\?v=([0-9]{8,})/);
    var story_id = id['match'](/(.*)\/story.php\?story_fbid=([0-9]{8,})/);
    var link_id = id['match'](/(.*)\/permalink.php\?story_fbid=([0-9]{8,})/);
    var other_id = id['match'](/(.*)\/([0-9]{8,})/);
    var other_id_2 = id['match'](/^[0-9.]+$/);
    var comment_id = id['match'](/(.*)comment_id=([0-9]{8,})/);
    //console.log(other_id)
    if (post_id) {
        result = post_id[2];
    } else {
        if (photo_id) {
            result = photo_id[2];
        } else {
            if (video_id) {
                result = video_id[2];
            } else {
                if (story_id) {
                    result = story_id[2];
                } else {
                    if (link_id) {
                        result = link_id[2];
                    } else {
                        if (other_id) {
                            result = other_id[2];
                        } else {
                            if (other_id_2) {
                                result = other_id_2[0];
                            }
                        }
                    }
                }
            }
        }
    }
    if (comment_id) {
        result += '_' + comment_id[2];
    }
    //console.log(result);
    if (result == null) {
        result = 'Url không hợp lệ.';
    }
    return result;
};

$('table').on('click', '.changeStatusBtn', function (e) {
    const id = $(this).data('id');
    const backMoney = this.dataset.backmoney;
    $('#backMoney').val(backMoney);
    const modal = $('#changeStatusModal');
    const preloader = modal.find('.preloader');
    preloader.css('display', 'block');
    $('#changeStatusBtn').attr('data-id', id);
    $.ajax({
        method: 'GET',
        url: `/admin/orders/get/${id}`,
        dataType: 'json',
        success: function (data) {
            modal.find('[name=status]').val(data.order.status);
            if (data.order.status == 3) {
                $('#backMoneyContainer').show();
            } else {
                $('#backMoneyContainer').hide();
            }

            $('#notify').val(data.order.notify);

            preloader.css('display', 'none');
        }
    });
});

$('table').on('click', '.changeStartCountBtn', function () {
    const id = $(this).data('id');
    const modal = $('#changeStartCountModal');
    const preloader = modal.find('.preloader');
    preloader.css('display', 'block');
    $('#changeStartCountBtn').attr('data-id', id);
    $.ajax({
        method: 'GET',
        url: `/admin/orders/get/${id}`,
        dataType: 'json',
        success: function (data) {
            modal.find('[name=startcount]').val(data.order.startcount);

            preloader.css('display', 'none');
        }
    });
});

$('#statusChange').change(e => {
    const value = e.target.value;
    if (value == 3) {
        $('#backMoneyContainer').show();
    } else {
        $('#backMoneyContainer').hide();
    }
});

$('table').on('click', '.changeNotifyBtn', function () {
    const id = $(this).data('id');
    const modal = $('#changeNotifyModal');
    const preloader = modal.find('.preloader');
    preloader.css('display', 'block');
    $('#changeNotifyBtn').attr('data-id', id);
    $.ajax({
        method: 'GET',
        url: `/admin/orders/get/${id}`,
        dataType: 'json',
        success: function (data) {
            modal.find('[name=notify]').val(data.order.notify);

            preloader.css('display', 'none');
        }
    });
});

$('table').on('click', '.changeviplike_statusBtn', function () {
    const id = $(this).data('id');
    const modal = $('#changeviplike_statusModal');
    const preloader = modal.find('.preloader');
    preloader.css('display', 'block');
    $('#changeviplike_statusBtn').attr('data-id', id);
    $.ajax({
        method: 'GET',
        url: `/admin/orders/get/${id}`,
        dataType: 'json',
        success: function (data) {
            modal.find('[name=viplike_status]').val(data.order.viplike_status);

            preloader.css('display', 'none');
        }
    });
});

$('#changeStatusBtn').on('click', function () {
    const id = $(this).data('id');

    console.log({ backMoney });
    const modal = $('#changeStatusModal');

    let data = {};
    data.id = id;
    data.status = modal.find('[name=status]').val();
    data.notify = modal.find('[name=notify]').val();
    data.backMoney = modal.find('[name=backMoney]').val();

    $.ajax({
        method: 'POST',
        url: '/admin/orders/changeStatus',
        data,
        dataType: 'json',
        success: function (data) {
            swal(
                {
                    title: 'Completed!',
                    text: `Thay đổi trạng thái thành công !`,
                    type: 'success'
                },
                function () {
                    location.reload(true);
                }
            );
        }
    });
});

$('#changeStartCountBtn').on('click', function () {
    const id = $(this).data('id');
    const modal = $('#changeStartCountModal');

    let data = {};
    data.id = id;
    data.startcount = modal.find('[name=startcount]').val();

    $.ajax({
        method: 'POST',
        url: '/admin/orders/ChangeStartCount',
        data,
        dataType: 'json',
        success: function (data) {
            swal(
                {
                    title: 'Completed!',
                    text: `Thay đổi trạng thái thành công !`,
                    type: 'success'
                },
                function () {
                    location.reload(true);
                }
            );
        }
    });
});

$('#changeNotifyBtn').on('click', function () {
    const id = $(this).data('id');
    const modal = $('#changeNotifyModal');

    let data = {};
    data.id = id;
    data.notify = modal.find('[name=notify]').val();

    $.ajax({
        method: 'POST',
        url: '/admin/orders/ChangeNotify',
        data,
        dataType: 'json',
        success: function (data) {
            swal(
                {
                    title: 'Completed!',
                    text: `Thay đổi trạng thái thành công !`,
                    type: 'success'
                },
                function () {
                    location.reload(true);
                }
            );
        }
    });
});

$('#changeviplike_statusBtn').on('click', function () {
    const id = $(this).data('id');
    const modal = $('#changeviplike_statusModal');

    let data = {};
    data.id = id;
    data.viplike_status = modal.find('[name=viplike_status]').val();

    $.ajax({
        method: 'POST',
        url: '/admin/orders/Changeviplike_status',
        data,
        dataType: 'json',
        success: function (data) {
            swal(
                {
                    title: 'Completed!',
                    text: `Thay đổi trạng thái thành công !`,
                    type: 'success'
                },
                function () {
                    location.reload(true);
                }
            );
        }
    });
});

$('#infoModal').on('hidden.bs.modal', function (e) {
    console.log('Closed');
    location.reload(true);
});