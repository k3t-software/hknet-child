<?php

namespace App\Helpers;

use HTTP_Request2;
use HTTP_Request2_Exception;

class HkNet
{
    private $baseURL = 'https://hknet.vn/api';
//    private $baseURL = 'http://host.docker.internal:8000/api';

//    private $baseURL = 'http://103.154.177.155:8000/api';
    private $token;

    public function __construct($token = null)
    {
        $this->token = $token;
    }

    public function post($endPoint, $data)
    {
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $endPoint);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        ));
        $data["token"] = $this->token;
        $request->addPostParameter($data);

        try {
            $response = $request->send();
            $result = json_decode($response->getBody());

            if(isset($result->message) && str_contains($result->message, 'số dư')) {
                $result->message = 'Xảy ra lỗi khi thêm vào hệ thống [077]';
            }
            return (object)$result;
        } catch (HTTP_Request2_Exception $e) {
            $result = (object)['success' => 0, 'message' => "Xảy ra lỗi khi thêm vào hệ thống [016]"];
        }
        return $result;
    }

    public function get($endPoint, $data = [])
    {
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $endPoint . "?token=" . $this->token);
        $request->setMethod(HTTP_Request2::METHOD_GET);

        $request->setConfig(array(
            'follow_redirects' => true,
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));

        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        ));

        $data["token"] = $this->token;
        $request->addPostParameter($data);

        try {
            $response = $request->send();
            $result = $response->getBody();

            return json_decode($result, true);
        } catch (HTTP_Request2_Exception $e) {
            $result = ['success' => 0, 'message' => "Xảy ra lỗi khi thêm vào hệ thống [016]"];
        }
        return $result;
    }
}
