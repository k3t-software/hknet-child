<?php

namespace App\Http\Controllers;

use App\Config;
use App\Helpers\Profile;
use App\Order;
use App\Service;
use App\Constant;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    private $orderModel;
    private $serviceModel;

    public function __construct()
    {
        $this->orderModel = new Order;
        $this->serviceModel = new Service;
        $this->configModel = new Config;
        $token = $this->configModel->getValue('token');
        $profile = new Profile($token);
        $this->entryPrices = $profile->getPrices();
    }


    public function AdminView()
    {

        $data["services"] = $this->serviceModel->get();
        $data['entryPrices'] = $this->entryPrices;
        $data['configs'] = $this->configModel;

        return view("admin.managements.services", $data);
    }

    public function View(Request $req, $category, $feature)
    {
        $user = Auth::user();
        if ($category === "flash-sale" && $user->verify === 0) {
            $data["user"] = $user;
            return view('admin.features.flash-sale.verifyEmail', $data);
        }
        $data['service'] = $this->serviceModel->get(Constant::SERVICEID($category, $feature));
        if ($data['service']->status == Constant::STATUS_SERVICE_UNAVAILABLE) {
            return redirect('/errors/503');
        }

        $data['orders'] = $this->orderModel->GetByType(Constant::SERVICEID($category, $feature));
        $this->orderModel->CheckCompleted($data['orders']);
        return view("admin.features.$category.$feature", $data);
    }

    public function update(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }
        $service = Service::find($req->input('id'));
        $service->name = $req->input("name");
        $service->status = $req->input("status");
        $service->apiPrice = $req->input("apiPrice");
        $service->agencyPrice = $req->input("agencyPrice");
        $service->collaboratorsPrice = $req->input("collaboratorsPrice");
        $service->guestPrice = $req->input("guestPrice");
        $service->block_duplicate = $req->input("block_duplicate") === 'on' ? 1 : 0;
        $service->unit_duration = $req->input("unit_duration");
        $service->save();
        return response("OK", 200);
    }

    public function pricePercent(Request $req)
    {
        $percents = explode("|", $req->input('percent'));
        $daiLy = $percents[0];
        $ctv = $percents[1];
        $khach = $percents[2];
        if (!isset($daiLy) || !isset($ctv) || !isset($khach)) {
            return response()->json([
                "success" => 0,
                "message" => "Please enter three items"
            ], 400);
        }

        $services = $this->serviceModel->get();
        foreach ($services as $service) {
            $hknetPrice = $this->entryPrices[$service->id];
            if (isset($hknetPrice)) {
                $service->agencyPrice = $hknetPrice + ($hknetPrice * intval($daiLy) / 100);
                $service->collaboratorsPrice = $hknetPrice + ($hknetPrice * intval($ctv) / 100);
                $service->guestPrice = $hknetPrice + ($hknetPrice * intval($khach) / 100);
                $service->save();
            }
        }
    }

    public function getById(Request $req)
    {
        if ($req->input('id') == null) {
            return response("ID is invalid !", 400);
        }
        $service = $this->serviceModel->get($req->input('id'));
        if ($service) {
            return response(["status" => "OK", "service" => $service], 200);
        }
    }
}
