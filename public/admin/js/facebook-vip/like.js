$(function () {
    let reOrderId;
    Accounting();

    $('#getProfileBtn').on('click', function () {
        const profile = $('#profileLink').val();
        if (!profile) return;
        const button = $(this);
        button.attr('disabled', true);
        button.html('<i class="fa fa-spin fa-circle-o-notch"></i>');

        $.ajax({
            url: `/admin/helper/GetProfile?profile=${profile}`,
            method: 'GET',
            dataType: 'json'
        }).always(function (data) {
            if (data.status == 1) {
                $('.profile-info [name=id]').val(data.items.id);
                $('.profile-info [name=name]').val(data.items.name);
                button.attr('disabled', false);
                button.html('Lấy thông tin');
            } else {
                button.attr('disabled', false);
                button.html('Lấy thông tin');
                swal(
                    {
                        title: 'Failed!',
                        text: `Đã xảy ra lỗi ! Vui lòng thử lại !`,
                        type: 'error'
                    },
                    function () {
                        button.attr('disabled', false);
                    }
                );
            }
        });
    });

    $('#reOrderPeriod').change(e => {
        const period = e.target.value;
        $.get('/admin/facebook-vip/getPrice/', {
            reOrderId,
            period
        }).then(res => {
            const total = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(res.total);
            $('#totalReOrder').html(total);
        }).catch(err => {
            console.log(err);
        });
    });

    $('#reOrderForm').submit(e => {
        const form = $('#reOrderForm');
        let formData = form.serializeArray();
        let values = {
            reOrderId
        };
        $(formData).each(function (index, obj) {
            values[obj.name] = obj.value;
        });
        $(form).find('button[type="submit"]').attr('disabled', true);

        $.ajax({
            type: 'POST',
            url: '/admin/dichvu/facebook-vip/reOrder',
            data: values
        }).then(res => {
            const isSuccess = (res && res.success) || 0;
            const message = res && res.message;

            if (isSuccess) {
                swal(
                    {
                        title: 'Thành công !!',
                        text: message,
                        type: 'success'
                    }, () => {
                        window.location.reload();
                    });
            } else {
                swal(
                    {
                        title: 'Thất bại',
                        text: message,
                        type: 'error'
                    }, () => {
                    });
            }

        }).catch(err => {
            const message = err.responseJSON && err.responseJSON.message;
            swal({
                title: 'Lỗi !!',
                text: message,
                type: 'error'
            });
        }).always((e) => {
            $(form).find('button[type="submit"]').attr('disabled', false);
        });
        e.preventDefault();
    });

    $('.order-info [name=package]').on('change', function () {
        Accounting();
    });

    $('.order-info [name=period]').on('change', function () {
        Accounting();
    });
    $('.order-info [name=posts]').on('change', function () {
        Accounting();
    });

    $('button[data-action="reOrder"]').on('click', (e) => {
        const orderId = $(e.target).attr('data-orderId');
        reOrderId = orderId;
        $('#reOrderModal').modal('show');
    });

    $('#orderBtn').on('click', function () {
        const button = $(this);
        button.attr('disabled', true);
        const buffType = button.data('function');
        const backupBtn = button.html();
        const loadingBtn =
            '<i class="fa fa-spin fa-circle-o-notch"></i> Thực hiện';
        button.html(loadingBtn);

        let data = {};

        const form = $('#infoForm');

        let formData = form.serializeArray();
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        data.service_id = button.data('service');

        data.id = $('.profile-info [name=id]').val();
        data.amount = $('.order-info [name=package]').val();
        $.ajax({
            url: `/admin/dichvu/facebook-vip/${buffType}`,
            method: 'POST',
            dataType: 'json',
            data
        }).always(function (data) {
            if (data) {
                if (data.success == 1) {
                    swal(
                        {
                            title: 'Thành công!',
                            text: `Thêm yêu cầu thành công !`,
                            type: 'success'
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                } else {
                    button.attr('disabled', false);
                    button.html(backupBtn);
                    swal(
                        {
                            title: 'Failed!',
                            text: data.message,
                            type: 'error'
                        },
                        function () {
                            button.attr('disabled', false);
                        }
                    );
                }
            }
            button.html(backupBtn);
        });
    });

    $('button[data-action="delete"]').on('click', (e) => {
        const orderId = $(e.target).attr('data-orderId');
        swal({
                title: 'Chắc chắn xóa ? ',
                text: 'Đơn hàng sẽ bị xóa và bạn sẽ ko được hoàn tiền trở lại. ',
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Thôi ',
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '/admin/dichvu/facebook-vip/delete',
                        data: {
                            orderId
                        }
                    }).then((res) => {
                        if (res.success) {
                            swal(
                                {
                                    title: 'Thành công !!',
                                    text: 'Đã xóa đơn thành công',
                                    type: 'success'
                                }, () => {
                                    window.location.reload();
                                });
                        } else {
                            swal({
                                title: 'Lỗi !!',
                                text: 'Hiện tại bạn không thể xóa đơn này !!!',
                                type: 'error'
                            });
                        }
                    }).catch(() => {
                        swal({
                            title: 'Lỗi !!',
                            text: 'Hiện tại bạn không thể xóa đơn này !!!',
                            type: 'error'
                        });
                    });
                }
            });

    });

    function Accounting() {
        const price = $('.order-info [name=price]').val();
        const package = $('.order-info [name=package]').val();
        const period = $('.order-info [name=period]').val();
        const posts = $('.order-info [name=posts]').val();
        const total = Math.round((price / 30) * package * period);
        $('.order-info [name=total]').val(total);
    }

});
