<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = "transactions";
    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public function GetByUserID($id)
    {
        return self::where('user_id', $id)->orderBy('id', 'DESC')->get();
    }
    public function Create($user_id, $type, $amount, $note)
    {
        $transaction = new Transaction();
        $transaction->user_id = $user_id;
        $transaction->amount = $amount;
        $transaction->type = $type;
        $transaction->note = $note;
        $transaction->save();
    }
}
