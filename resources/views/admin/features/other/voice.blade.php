@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];

@endphp
@extends('admin.layouts.master')

@section('content')
<script>

function getVal()
{
document.getElementById('linephone').innerHTML = countLine()*500;
console.log("giá trị:");
}

function countLine(){
var t=document.phone.id.value;
return (t=='') ? '0' : (t.split("\n").length);
}
</script>
	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Gọi Thoại CSKH</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">Gọi Thoại</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tạo mới cuộc gọi</h4>
						<h6 class="card-subtitle">Cuộc gọi</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">

								<form name="phone" id = "infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Gọi Đến SDT:</label>

										<div class="input-group">

                                            <textarea data-price = maxlength="1000" name = "id" id = "profileLink" class="form-control" rows="3" onkeyup="getVal();"></textarea>

										</div>
									</div>
									<div class = "order-info pt-3">
										<input type="hidden" data-price ="{{$service[$role[Auth::user()->role]]}}" name="price">
										<div class="row">
											<div class="col-12 mb-3">
												<label>Gói Ký Tự</label>
												<select data-price="{{$service[$role[Auth::user()->role]]}}" class="form-control" name = "amount">
													<option value="125"> 125 Ký tư</option>
													<option value="250"> 250 Ký tự</option>
												</select>

											</div>
                                            <div class="col-12 mb-3">
                                            <label>Chọn Sim Gọi</label>
                                            <select class="form-control" name="sdt">
                                            <option value="8481888888"> ***2888888</option>
                                            <option value="8477779999"> ***7779999</option>
                                            <option value="8489999999"> ***9999999</option>
                                            </select>
                                            </div>
								    	<div class="col-12">
												<label>Giọng Đọc: </label>
												<input class = "radio-col-light-blue" value = "male" name="gender" type="radio" id="radio_1" checked="">
												<label for="radio_1">Nam</label>
												<input class = "radio-col-light-blue" value = "female" name="gender" type="radio" id="radio_2">
												<label for="radio_2">Nữ</label>

											</div>
										<div class="col-12">
												<div class="form-group">
													<label>Nội dung truyền tải</label>
													<textarea data-price = "{{$service[$role[Auth::user()->role]]}}" maxlength="125" name = "comment" class="form-control" rows="3"></textarea>
												</div>
											</div>

										    <div class="input-group col-12 pt-3">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Đơn giá</button>
												</div>
												<input name ="price" disabled type="text" class="form-control" value = "500đ / Cuộc gọi" aria-label="Username" aria-describedby="basic-addon1">
											</div>
										</div>


										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<span style="background-color:#8BC34A !important; color:white" class="form-control">
                                                        <span id="linephone">0 </span> đ
												<!--<input disabled data-price="10000" value=0 name="total" id="total" type="number" class="form-control">-->

												</span>
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "voice" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
                                        <p>- Cho Phép Gọi Thoại Marketing Chăm Sóc Khách Hàng, Nội dung sẽ được kiểm duyệt</p>
                                        <p><span class="text-danger">Nghiêm cấm sử dụng dịch vụ vào các hành vi vi phạm pháp luật, Thành viên chịu trách nhiệm hoàn toàn về nội dung của mình trước công chúng và pháp luật</span></p>
                                        <p><span class="text-danger">Khi phát hiện các hành vi vi phạm trên, chúng tôi có quyền chấm dứt hoạt động vĩnh viễn trên tài khoản của bạn</span></p>

								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-subtitle">Danh sách các đơn hàng vip like của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Thông báo</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thời gian</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class="text-right"><span class="badge btn btn-outline-danger">{{$order->notify}}</span></td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class = "text-right">{{$order->created_at}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src = "{{asset("admin/js/other/voice.js?szssssssssssss")}}"></script>
@endsection