const selectTheme = (theme, type) => {
    $.post('/admin/config/applyTheme', {
        theme,
        type
    }).then(res => {
        swal(
            {
                title: 'Thành công !!',
                text: 'Đã lưu tùy chọn. ',
                type: 'success'
            }, function () {
                location.reload(true);
            });
    }).catch(err => {
        console.log(err);
    });
};

const deleteBank = (configKey = '') => {
    swal(
        {
            title: 'Are you sure?',
            text: 'You will not be able to recover this bank!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: '/admin/config/delete/',
                method: 'POST',
                data: {
                    configKey: configKey
                },
                success: function (data) {
                    swal(
                        {
                            title: 'Completed!',
                            text: 'Your bank was deleted',
                            type: 'success'
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                },
                error: function (data) {
                    swal(
                        {
                            title: 'Failed!',
                            text: `Please check your bak and try again !`,
                            type: 'error'
                        },
                        function () {

                        }
                    );
                }
            });
        }
    );
};
