<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// --------------------------------------------
// CLIENT-ROUTE
// --------------------------------------------
Route::get('/', 'GuestController@landingPage');

Route::get('/dangnhap', function () {
    return view('admin.auth.homes');
});
//test view
Route::get('/BangGia', function () {
    return view('admin.managements.banggia');
});
Route::get('/VietBao', function () {
    return view('admin.managements.vietbao');
});
Route::post('/register', 'UserController@create');

Route::get('/register', 'AuthController@register');

Route::get('/get-facebook-id', 'OrderController@GetProfile');

Route::get('/api/info', 'TransactionController@TransactionInfo');


// --------------------------------------------
// END CLIENT-ROUTE
// --------------------------------------------

// --------------------------------------------
// ADMIN-ROUTE
// --------------------------------------------


Route::get('/errors/403', function () {
    return view('errors.403');
});

Route::get('/errors/503', function () {
    return view('errors.503');
});

Route::prefix('mail')->group(function () {
    Route::post('/sendOtp/{email}', 'UserController@sendOtp');
});

// Public API for update order details.
Route::post('/api/webhook/{type}', 'APIController@webhook');

Route::get('/install', 'DashboardController@install');

Route::post('/config/save', 'ConfigController@save');

Route::prefix('admin')->group(function () {

    Route::middleware('auth')->group(function () {

        Route::get('facebook-vip/getPrice', 'OrderController@FacebookVipGetPrice');
        Route::get('/api', 'APIController@view');
        Route::get('/referManager', 'UserController@referManager');

        Route::prefix('chat')->group(function () {
            Route::get('/', 'ChatController@index');
            Route::get('/getSenders', 'ChatController@getSenders');
            Route::post('/sendMessage', 'ChatController@sendMessage');
            Route::get('/getMessages', 'ChatController@getMessages');
            Route::get('/getUnReadMessage', 'ChatController@getUnreadMessage');
            Route::post('/seenMessage', 'ChatController@setSeen');
        });


        Route::prefix('mail')->group(function () {
            Route::post('/sendOtp', 'UserController@sendOtp');
            Route::post('/verify', 'UserController@verify');
        });

        //view
        Route::prefix('dichvu')->group(function () {

            Route::get('/{category}/{feature}', 'ServiceController@View');
            Route::post('facebook-vip/getPrice', 'OrderController@FacebookVipGetPrice');
            Route::post('facebook-vip/delete', 'OrderController@FacebookVipDeleteOrder');
            Route::post('facebook-vip/reOrder', 'OrderController@FacebookVipReOrder');
            Route::post('facebook-vip/delete', 'OrderController@FacebookVipDeleteOrder');
            Route::post('facebook-vip/comment/edit', 'OrderController@FacebookVipCommentEdit');

            Route::middleware('CheckBalance')->group(function () {

                Route::post('facebook-gia-re/{feature}', 'OrderController@CheapFacebook');

                Route::post('facebook-buff/{feature}', 'OrderController@FacebookBuff');

                Route::post('facebook-vip/{feature}', 'OrderController@FacebookVip');

                Route::post('instagram/{feature}', 'OrderController@Instagram');

                Route::post('tiktok/{feature}', 'OrderController@TikTok');

                Route::post('youtube/{feature}', 'OrderController@Youtube');

                Route::post('other/{feature}', 'OrderController@Other');

                Route::post('twitter/{feature}', 'OrderController@Twitter');

                Route::post('telegram/{feature}', 'OrderController@Telegram');

                Route::post('google/{feature}', 'OrderController@Google');

                Route::post('shopee/{feature}', 'OrderController@Shopee');
                Route::post('flash-sale/{feature}', 'OrderController@Flashsale');
            });
        });
        Route::prefix('helper')->group(function () {
            Route::get('GetProfile', 'OrderController@GetProfile');
            Route::get('GetInstagramProfile', 'OrderController@GetInstagramProfile');
            Route::get('GetUserFromPhone', 'UserController@getInfoFromPhone');
            Route::get('referUser', 'UserController@getLowerUser');
        });

        Route::get('/payments', 'PaymentController@index');
        Route::get('/withDraw', 'WithDrawController@index');
        Route::post('/payments', 'PaymentController@check');
        Route::post('/moneyTransfer', 'UserController@moneyTransfer');

        Route::post('/sendWithDrawRequest', 'WithDrawController@sendWithDrawRequest');
        Route::post('/withDraw/update', 'WithDrawController@updateDraw');
        Route::post('/changePassword', 'UserController@changePassword');

        /* Resource */
        Route::post('/resource/insert', 'ResourceController@insert');
        Route::get('/resource/view/{orderId}', 'ResourceController@view');


        Route::get('/config', 'ConfigController@index');
        Route::post('/config/save', 'ConfigController@save');
        Route::post('/config/bank/save', 'ConfigController@saveBank');
        Route::post('/config/applyTheme', 'ConfigController@applyTheme');
        Route::post('/config/delete', 'ConfigController@delete');

    });

    Route::get('/dashboard', "DashboardController@view");
    Route::get('/BangGia', "DashboardController@view");
    Route::get('/VietBao', "DashboardController@views");
    Route::get('/transactions', 'TransactionController@View');
    Route::get('/admin-transactions', 'TransactionController@AdminTransactions');

    Route::middleware('CheckAdmin')->group(function () {
        Route::get('/users', 'UserController@view');
        Route::get('/setting', 'SettingController@index');
        Route::get('/orders/{type}', 'OrderController@view');
        Route::get('/services', 'ServiceController@AdminView');
        Route::get('/refund', 'DashboardController@Refund');

        Route::get('/users/get', 'UserController@getById');
        Route::get('/orders/get/{id}', 'OrderController@getById');
        Route::get('/services/get', 'ServiceController@getById');
        Route::post('/services/update', 'ServiceController@update');
        Route::post('/services/pricePercent', 'ServiceController@pricePercent');
        Route::post('/users/create', 'UserController@create');
        Route::post('/setting', 'SettingController@update');
        Route::post('/setting/addToken', 'SettingController@addToken');
        Route::post('/users/addMoney', 'UserController@AddMoney');
        Route::post('/users/changeRole', 'UserController@ChangeRole');
        Route::post('/orders/changeStatus', 'OrderController@ChangeStatus');
        Route::post('/orders/ChangeStartCount', 'OrderController@ChangeStartCount');
        Route::post('/orders/ChangeNotify', 'OrderController@ChangeNotify');
        Route::post('/orders/Changeviplike_status', 'OrderController@Changeviplike_status');
        Route::post('/orders/delete', 'OrderController@DeleteById');
        Route::post('/users/update', 'UserController@update');
        Route::post('/partner/create', 'PartnerController@create');
        Route::get('/partner/delete', 'PartnerController@delete');
        Route::delete('/users/delete', 'UserController@delete');
        Route::post('/newfeed/save', 'SettingController@saveNewfeed');
        Route::post('/newfeed/delete', 'SettingController@deleteNewfeed');
    });
});

// Route::middleware('CheckToken')->group(function () {

// });

// --------------------------------------------
// END ADMIN-ROUTE
// --------------------------------------------

// --------------------------------------------
// AUTH-ROUTE
// --------------------------------------------

Route::post('login', 'AuthController@authenticate')->name('login');
Route::get('login', 'AuthController@view');
Route::get('logout', 'AuthController@logout')->name('logout');
Route::post('/giftCode/create', 'SettingController@createGifCode');
Route::get('/giftCode/delete', 'SettingController@deleteGiftCode');
Route::get('/cron/partnerReset', 'PartnerController@resetUsedView');


// --------------------------------------------
// END AUTH-ROUTE
// --------------------------------------------
