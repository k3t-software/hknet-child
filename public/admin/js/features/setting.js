function deleteGift(id) {
    // Fucking code =)) but i'm too lazy :)))
    swal({
            title: 'Chắc chắn xóa ? ',
            text: 'Giftcode se bi xoa.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Thôi ',
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (!isConfirm) return;
            fetch('/giftCode/delete?id=' + id).then(res => res.data).then(res => {
                console.log(res);
                swal(
                    {
                        title: 'Thành công !!',
                        text: 'Đã xoa. ',
                        type: 'success'
                    });
            }).catch(err => {
                swal(
                    {
                        title: 'Error !!',
                        text: 'Delete error ! Please try again. ',
                        type: 'warning'
                    });
            });
        });

}

$(function () {
    'use strict';

    $('#partnersSettings').submit(e => {
        let formData = $('#partnersSettings').serializeArray();
        let values = {};
        $(formData).each(function (index, obj) {
            values[obj.name] = obj.value;
        });
        $('#btn-submit').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/admin/partner/create',
            data: values,
            success: function (res) {
                swal(
                    {
                        title: 'Thành công !!',
                        text: 'Đã thêm tài khoản ',
                        type: 'success'
                    });
                $('#btn-submit').attr('disabled', false);
            },
            fail: function (error) {
                $('#btn-submit').attr('disabled', false);
            }
        });
        e.preventDefault();
    });

    $('#banner-settings').submit(e => {
        let formData = $('#banner-settings').serializeArray();
        let values = {};
        $(formData).each(function (index, obj) {
            values[obj.name] = obj.value;
        });
        $('#btn-submit').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/admin/setting',
            data: {
                name: 'banner',
                value: JSON.stringify(values)
            },
            success: function (res) {
                swal(
                    {
                        title: 'Thành công !!',
                        text: 'Đã lưu banner. ',
                        type: 'success'
                    });
                $('#btn-submit').attr('disabled', false);
            },
            fail: function (error) {
                $('#btn-submit').attr('disabled', false);
            }
        });
        e.preventDefault();
    });

    $('#gifCode').submit(e => {
        let formData = $('#gifCode').serializeArray();
        let values = {};
        $(formData).each(function (index, obj) {
            values[obj.name] = obj.value;
        });
        $('#btn-submit').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/giftCode/create',
            data: {
                code: $('#code').val().trim(),
                quantity: $('#quantity').val().trim(),
                value: $('#value').val().trim()
            },
            success: function (res) {
                swal(
                    {
                        title: 'Thành công !!',
                        text: 'Đã thêm giftCode ',
                        type: 'success'
                    });
                window.location = '/admin/setting#gifCode';
                $('#btn-submit').attr('disabled', false);
            },
            fail: function (error) {
                swal(
                    {
                        title: 'That bai',
                        text: 'Gift Code da ton tai',
                        type: 'info'
                    });
                $('#btn-submit').attr('disabled', false);
            }
        }).catch(() => {
            swal(
                {
                    title: 'That bai',
                    text: 'Gift Code da ton tai',
                    type: 'warning'
                });
            $('#btn-submit').attr('disabled', false);
        });
        e.preventDefault();
    });

    async function checkTokens(tokens) {
        const avaiableTokens = [];
        for (const token of tokens) {
            // avaiableTokens.push({
            //     fb_id: Math.floor(Math.random() * 9999999999999),
            //     token: token
            // });

            const response = await $.get('/helper/checkToken?access_token=' + token).catch(err => {
                return { error: true };
            });
            if (response && response.id) {
                avaiableTokens.push({
                    fb_id: response.id,
                    token
                });
            }
        }
        return avaiableTokens;
    }

    $('#token-setting').submit(async e => {
        $('#token-setting #btn-submit').attr('disabled', true);
        $('#token-setting #btn-submit').html('Đang thêm...');
        const tokens = $('#listTokens').val().trim().split('\n');
        checkTokens(tokens).then(availableTokens => {
            $.ajax({
                type: 'POST',
                url: '/admin/setting/addToken',
                data: {
                    tokens: availableTokens
                },
                success: function () {
                    swal(
                        {
                            title: 'Đã thêm ' + availableTokens.length + ' tokens',
                            text: `Thêm ${tokens.length}, token khả dụng ${availableTokens.length} !!`,
                            type: 'success'
                        });
                    $('#token-setting #btn-submit').attr('disabled', false);
                    $('#token-setting #btn-submit').html('Cập nhật');

                },
                fail: function (error) {
                    $('#token-setting #btn-submit').attr('disabled', false);
                    $('#token-setting #btn-submit').html('Cập nhật');
                }
            });
        });
        e.preventDefault();
    });

});



const deleteFeed = (id = '') => {
    swal(
        {
            title: 'Are you sure?',
            text: 'You will not be able to recover this bank!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: '/admin/newfeed/delete/',
                method: 'POST',
                data: {
                    id
                },
                success: function (data) {
                    swal(
                        {
                            title: 'Completed!',
                            text: 'Your feed was deleted',
                            type: 'success'
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                },
                error: function (data) {
                    swal(
                        {
                            title: 'Failed!',
                            text: `Please check your bak and try again !`,
                            type: 'error'
                        },
                        function () {

                        }
                    );
                }
            });
        }
    );
};


