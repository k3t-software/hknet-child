<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;

	protected $table = 'users';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function CountOrderByUserId($user_id, $status)
	{
		$count = Order::where('user_id', $user_id);
		if ($status != -1) {
			$count = $count->where('status', $status);
		}

		return $count->count();
	}

	public function GetByToken($token)
	{
		return self::where('api_token', $token)->first();
	}


	public function GetTotal($user_id, $type)
	{
		return Transaction::where('type', $type)->where('user_id', $user_id)->sum("amount");
	}
	public function get($id = null)
	{
		if ($id) {
			return self::find($id);
		} else {
			return self::all();
		}
	}
}
