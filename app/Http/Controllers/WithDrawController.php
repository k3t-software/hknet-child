<?php

namespace App\Http\Controllers;

use App\User;
use App\WithDrawRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WithDrawController extends Controller
{
    private $withdrawRequestModel;
    private $withDrawModel;
    private $userModel;

    public function __construct()
    {
        $this->withdrawRequestModel = DB::table("withdrawing_requests");
        $this->withDrawModel = new WithDrawRequests();
        $this->userModel = new User();
    }

    public function index()
    {
        return view("admin.managements.withdraw");
    }

    public function sendWithDrawRequest(Request $req)
    {
        $req->validate([
            'stk' => 'required',
            'ctk' => 'required',
            'bank' => 'required',
            'amount' => 'required',
        ]);

        $bank_info = "STK: {$req->input('stk')} <br /> CTK: {$req->input('ctk')} <br /> BANK: {$req->input('bank')}";
        $balance = $req->input("amount");
        $user = Auth::user();

        if ($balance <= $user->balance_2) {
            $this->withdrawRequestModel->insert([
                "user_id" => $user->id,
                "bank_info" => $bank_info,
                "balance" => $balance,
                "status" => 2,
                "note" => ""
            ]);
            return response()->json([
                "success" => 1,
                "message" => "Yêu cầu rút tiền của bạn đã được gửi ! Bạn sẽ nhận được thông báo khi xét duyệt thành công !"
            ]);
        } else {
            return response()->json([
                "success" => 0,
                "message" => "Số tiền trong tài khoản của bạn hiện không đủ"
            ], 402);
        }
    }

    public function updateDraw(Request $req)
    {
        $req->validate([
            'id' => 'required',
            'status' => 'required'
        ]);

        $withDraw = $this->withDrawModel->where("id", $req->input("id"))->first();
        // Users
        $user = $this->userModel->get($withDraw->user_id);
        if ($user->balance_2 < $withDraw->balance) {
            return response()->json([
                "success" => 0,
                "message" => "Số tiền trong tài khoản không đủ"
            ], 402);
        }
        $withDraw->status = $req->input("status");
        $withDraw->note = $req->input("note");
        $withDraw->save();

        if ((int)$req->input("status") === 1) {
            $user->balance_2 -= $withDraw->balance;
            $user->save();
        }
    }
}
