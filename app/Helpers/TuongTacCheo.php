<?php

namespace App\Helpers;

use HTTP_Request2;
use HTTP_Request2_Exception;

class TuongTacCheo
{
    private $baseURL = 'https://tuongtaccheo.com';
    public $cookie;

    public $account;

    public function getSession()
    {
        $request = new HTTP_Request2();
        $request->setUrl('https://tuongtaccheo.com');
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig(array(
            'follow_redirects' => true,
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));

        $response = $request->send();

        $cookies = $response->getCookies();
        foreach ($cookies as $key => $cookie) {
            if ($cookie['name'] === "PHPSESSID") {

                $this->cookie = $cookie['value'];
                return $cookie['value'];
            }
        }

        return null;
    }

    public function login()
    {
        try {
            // Get sessions
            $sessionTraoDoiCheo = $this->getSession();


            $request = new HTTP_Request2();
            $request->setUrl('https://tuongtaccheo.com/login.php');
            $request->setMethod(HTTP_Request2::METHOD_POST);
            $request->setConfig(array(
                'follow_redirects' => true,
                'ssl_verify_peer' => false,
                'ssl_verify_host' => false
            ));
            $request->setHeader(array(
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Cookie' => 'PHPSESSID=' . $sessionTraoDoiCheo
            ));
            $request->addPostParameter(array(
                'username' => 'seuiikt3xs',
                'password' => 'dhkytv0812',
                'submit' => 'ĐĂNG NHẬP'
            ));
            try {
                $response = $request->send();

                if ($response->getStatus() == 200) {
                    $wrongPasswordString = "Sai tài khoản/mật khẩu";

                    $isWrongPassword = str_contains($response->getBody(), $wrongPasswordString);
                    if ($isWrongPassword) {
                        return (object)[
                            'success' => 0,
                            'message' => 'Quả tải vui lòng thử lại hoặc đổi qua server khác !! [068]'
                        ];
                    }
                    $cookies = $response->getCookies();
                    foreach ($cookies as $key => $cookie) {
                        if ($cookie['name'] === "PHPSESSID") {
                            $this->cookie = $cookie['value'];
                            return $cookie['value'];
                        }
                    }
                    return null;
                } else {
                    echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                        $response->getReasonPhrase();
                    return false;
                }
            } catch (HTTP_Request2_Exception $e) {
                echo 'Error: ' . $e->getMessage();
            }


        } catch (HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return 'loi me roi ?';
        }
    }

    private function post($url, $data)
    {
        if (!isset($this->cookie)) {
            return (object)[
                'success' => 0,
                'message' => 'Quả tải vui lòng thử lại hoặc đổi qua server khác !! [068]'
            ];
        }
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $url);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true,
            'ssl_verify_peer' => true,
            'ssl_verify_host' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cookie' => 'PHPSESSID=' . $this->cookie
        ));

        $request->addPostParameter($data);


        try {
            $response = $request->send();

            if ($response->getStatus() == 200) {
                $isSuccess = str_contains($response->getBody(), "thành công");

                if ($response->getBody() == "1" || $response->getBody() == "0") {
                    return (object)[
                        'success' => 0,
                        'message' => 'Quả tải vui lòng thử lại hoặc đổi qua server khác !! [099]'
                    ];
                }

                return (object)[
                    'success' => $isSuccess,
                    'message' => $response->getBody()
                ];
            } else {
                return (object)['success' => 0, 'message' => "Xảy ra lỗi khi thêm vào hệ thống [016]"];
            }
        } catch (HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return (object)['success' => 0, 'message' => 'Unknown error: ' . $e->getMessage()];
        }
    }

    public function buyReaction($idPost, $amount, $reaction)
    {
        return $this->post('/tangcamxuc/themvip.php', array(
            'maghinho' => "",
            'id' => $idPost,
            'link' => "https://www.facebook.com/$idPost",
            'idtrongbainhieuanh' => false,
            'loai' => $reaction,
            'tocdolen' => 0,
            'sl' => (int)$amount,
            'magiamgia' => '',
            'dateTime' => date('Y-m-d H:i:s'),
        ));
    }


    public function buyLike($idPost, $amount)
    {
        return $this->post('/tanglike/themvip.php', array(
            'maghinho' => "",
            'id' => $idPost,
            'link' => "https://www.facebook.com/$idPost",
            'idtrongbainhieuanh' => false,
            'loai' => 'LIKE',
            'tocdolen' => 0,
            'sl' => (int)$amount,
            'magiamgia' => '',
            'dateTime' => date('Y-m-d H:i:s'),
        ));
    }
}


//$tuongTacCheo = new TuongTacCheo();
//$tuongTacCheo->login();
//$result = $tuongTacCheo->buyLike('2998040027101362', 10);
//var_dump($result);
