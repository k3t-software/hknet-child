@php
    $type = ['<span class="badge badge-success">Nạp tiền</span>', '<span class="badge badge-warning">Sử dụng dịch vụ</span>', '<span class="badge badge-danger">Chuyển tiền</span>', '<span class="badge badge-info">Nhận tiền</span>', '<span class="badge badge-primary">Hoàn tiền dịch vụ</span>', '<span class="badge badge-primary">[Ví 2] Hoa hồng</span>'];
@endphp

@extends('admin.layouts.master')

@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Hoàn tiền dịch vụ</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Hoàn tiền</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        @if(isset($userTransactions))
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row p-3">
                                <div class="col-md-8">
                                    <h3 class="card-title">Hoàn tiền dịch vụ</h3>
                                </div>
                            </div>

                            <div class="table-responsive p-1">
                                <table class="myTableCustomCustom table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">User</th>
                                        <th class="text-center">Loại</th>
                                        <th class="text-right">Số tiền</th>
                                        <th class="text-center" style="max-width: 200px;">Ghi chú</th>
                                        <th class="text-right">Thời gian</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($userTransactions as $transaction)
                                        <tr>
                                            <td class="text-center">{{$transaction->id}}</td>
                                            <td class="text-center">
                                                {!!$transaction->name!!} <br />
                                                [ {!!$transaction->phone!!}]
                                            </td>
                                            <td class="text-center">
                                                {!!$type[$transaction->type]!!}
                                            </td>

                                            <td class="text-right @if ($transaction->type == 0)
                                                    text-success
@else
                                                    text-danger
@endif">
                                                @if ($transaction->type == 0)
                                                    +
                                                @endif
                                                {!!number_format($transaction->amount)!!}
                                            </td>

                                            <td class="text-center">
                                                {!!$transaction->note!!}
                                            </td>

                                            <td class="text-right">
                                                {{date('H:i:s d/m/Y', strtotime($transaction->created_at))}}
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Danh sách giao dịch cá nhân</h3>
                            </div>

                        </div>
                        <div class="table-responsive p-1">
                            <table id="myTable" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-right">Số tiền</th>
                                    <th class="text-center" style="max-width: 200px;">Ghi chú</th>
                                    <th class="text-right">Thời gian</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($refunds as $transaction)
                                    <tr>
                                        <td class="text-center">{{$loop->index + 1}}</td>
                                        <td class="text-right @if ($transaction['type'] == 0 || $transaction['type'] === 5)
                                                text-success
@else
                                                text-danger
@endif">
                                            @if ($transaction['type'] == 0 || $transaction['type'] === 5)
                                                +
                                            @endif
                                            {!!number_format($transaction['amount'])!!}
                                        </td>

                                        <td class="text-center" style="max-width: 400px">
                                            {!!$transaction['note']!!}
                                        </td>

                                        <td class="text-right">
                                            {{date('H:i:s d/m/Y', strtotime($transaction['created_at']))}}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('scripts')
    {{-- <script src = "{{asset("admin/js/features/services.js")}}"></script> --}}


    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

    <script src="{{asset("admin/js/dashboard4.js")}}"></script>
@endsection
