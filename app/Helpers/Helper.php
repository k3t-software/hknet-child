<?php

namespace App\Helpers;

use HTTP_Request2;

class Helper
{

    public static function HTTP_POST($url, $data, $isGetHeader = false, $cookie = null)
    {

        $request = new HTTP_Request2();
        $request->setUrl($url);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setConfig(array(
            'ssl_verify_peer' => false,
            'ssl_verify_host' => false
        ));

        $headers = array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        );
        if ($cookie !== null) {
            $headers["Cookie"] = $cookie;
        }

        $request->setHeader($headers);
        $request->addPostParameter($data);

        $response = $request->send();
        if ($response->getStatus() == 200) {
            if ($isGetHeader) {
                return $response->getCookies();
            } else {
                return json_decode($response->getBody(), true);
            }
        } else {
            return [
                "status" => 0,
                "message" => $response->getReasonPhrase()
            ];
        }
    }
}


