<?php

namespace App\Http\Controllers;

use App\Config;
use App\Helpers\Profile;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;


use App\Transaction;
use App\Service;
use App\Constant;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    private $model;
    private $serviceModel;

    public function __construct()
    {
        $this->model = new Transaction;
        $this->serviceModel = new Service;
        $this->orderModel = new Order;
        $this->configModel = new Config;
        $this->userModel = new User;
    }

    public function View(Request $req)
    {
        $user = Auth::user();
        if ($user->role === 0) {
            $data['userTransactions'] = $this->model->join('users', 'users.id', '=',
                'transactions.user_id')->select('transactions.*', 'users.name', 'users.phone')->orderBy('id',
                'DESC')->limit(300)->get();
        }
        $data["transactions"] = $this->model->GetByUserID(Auth::user()->id);
        return view("admin.managements.transactions", $data);
    }


    public function AdminTransactions(Request $req)
    {
        $user = Auth::user();
        $token = $this->configModel->getValue('token');

        $profile = new Profile($token);
        $tokenInfo = $profile->getInfo();
        $data['tokenInfo'] = $tokenInfo;

        $data['totalBalance'] = $this->userModel->get()->sum("balance");
        $orders = $this->orderModel->get();
        $data['totalProfit'] = $orders->sum("profit");
        $data['totalOrder'] = $orders->count();

        $data['userTransactions'] = $this->model->join('users', 'users.id', '=',
            'transactions.user_id')->select('transactions.*', 'users.name', 'users.phone')->orderBy('id',
            'DESC')->limit(300)->get();

        $data['orders'] = DB::select("SELECT orders.*, users.name, services.name AS service_name, services.unit FROM `orders` INNER JOIN services ON orders.service_id = services.id INNER JOIN users ON orders.user_id = users.id;");
        return view("admin.managements.adminTransactions", $data);
    }

    public function TransactionInfo(Request $req)
    {

        $token = $this->configModel->getValue('token');

        $profile = new Profile($token);
        $tokenInfo = $profile->getInfo();
        $data['tokenInfo'] = $tokenInfo;

        $data['totalBalance'] = $this->userModel->get()->sum("balance");
        $orders = $this->orderModel->get();
        $data['totalProfit'] = $orders->sum("profit");
        $data['totalOrder'] = $orders->count();

        $data['userTransactions'] =  $this->model->join('users', 'users.id', '=',
            'transactions.user_id')->select('transactions.*', 'users.name', 'users.phone')->orderBy('id',
            'DESC')->limit(300);

        $data['orders'] = DB::select("SELECT orders.*, users.name, services.name AS service_name, services.unit FROM `orders` INNER JOIN services ON orders.service_id = services.id INNER JOIN users ON orders.user_id = users.id;");

        return $data;
    }

    // public function update(Request $req)
    // {
    //     if ($req->input('id') == NULL) return response("ID is invalid !", 400);
    //     $service = Service::find($req->input('id'));
    //     $service->name = $req->input("name");
    //     $service->status = $req->input("status");
    //     $service->apiPrice = $req->input("apiPrice");
    //     $service->agencyPrice = $req->input("agencyPrice");
    //     $service->collaboratorsPrice = $req->input("collaboratorsPrice");
    //     $service->guestPrice = $req->input("guestPrice");
    //     $service->save();
    //     return response("OK", 200);
    // }

    // public function getById(Request $req)
    // {
    //     if ($req->input('id') == NULL) return response("ID is invalid !", 400);
    //     $service = $this->serviceModel->get($req->input('id'));
    //     if ($service)
    //         return response(["status" => "OK", "service" => $service], 200);
    // }
}
