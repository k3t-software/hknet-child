<?php

namespace App\Http\Controllers;

use App\Partner;
use http\Message;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    //
    private $model;

    public function __construct()
    {
        $this->model = new Partner;
    }

    public function create(Request $request)
    {
        $accounts = explode("\n", $request->input('accounts'));
        $type = $request->input('type');

        foreach ($accounts as $account) {
            $accountInfo = explode("|", $account);
            $username = $accountInfo[0];
            $password = $accountInfo[1];

            if ($username && $password) {
                $partner = new Partner();
                $partner->username = $username;
                $partner->password = $password;
                $partner->partner_name = $type;
                $partner->used = 0;
                $partner->note = "";
                $partner->is_stop = 0;
                $partner->save();
            }
        }

        return response()->json([
            "status" => 1,
            "message" => "thêm thành công "
        ]);
    }

    public function delete(Request $req)
    {
        $id = $req->input('id');
        $partner = $this->model->get($id);
        if (isset($partner)) {
            $partner->delete();
            echo("Deleted ! Please close this tab. ");
        } else {
            echo("Sorry ! We can't find partner");
        }
    }

    public function resetUsed($partnerName = 'traodoisub.com')
    {
        $this->model->where('partner_name', $partnerName)->update(array(
            'used' => 0
        ));
    }

    public function resetUsedView(Request $request)
    {
        $this->resetUsed('traodoisub.com');
        $this->resetUsed('traodoisub.com-sale');
        echo "Done";
    }
}
