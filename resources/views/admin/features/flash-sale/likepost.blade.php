@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Thành công</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Lỗi ID</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];

@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Tăng Like Miễn Phí</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">Tăng Like Miễn Phí</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tăng Like Miễn Phí</h4>
						<h6 class="card-Like Posttitle">Chức năng buff Like Post cho tài khoản facebook</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm">
									{{ csrf_field() }}
									<div class="form-group">
										<label>Facebook post</label>
										<div class="input-group">
											<input type="text" class="form-control" id = "profileLink" placeholder="Nhập link hoặc page id">
											<div class="input-group-append">
												<button class="btn btn-primary" id = "getID" type="button">Lấy thông tin</button>
											</div>
										</div>
									</div>

									<div class = "profile-info">

										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">Post ID</button>
											</div>
											<input name ="id" type="text" disabled class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<div class="row">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Số lượng</button>
												</div>
												<input data-price = "{{$service[$role[Auth::user()->role]]}}" value="50" min="50" name = "amount" type="number" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
											</div>
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Đơn giá</button>
												</div>
												<input name ="price" disabled type="text" class="form-control" value = "{{$service[$role[Auth::user()->role]]}} đ / {{$service['unit']}}" aria-label="Username" aria-describedby="basic-addon1">
											</div>
										</div>

									<div class = "order-info pt-3">
                                    <div class="input-group-prepend">
													<button class="btn btn-info" type="button">Cảm Xúc</button><select class="form-control" name="reaction" id="reaction">
                                                                    <option value="LIKE">LIKE</option>
                                    </select>
												</div>
                                    </div>
										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value =0 name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "likepost" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>
                                                                                     
										</div><br><code>Lưu ý: Dịch vụ miễn phí chỉ tối đa 50 like / bài viết</code>
									</div>

<p></p>

								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
									    <p>Lên ngay khi order</p>
                                        <p>50 like / bài</p>
                                        <p>Mỗi ngày 2 lượt dịch vụ miễn phí</p>
								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-Like Posttitle">Danh sách các đơn hàng buff like Post của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thời gian</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class = "text-right">{{$order->created_at}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src="{{asset("admin/js/flash-sale/likepost.js")}}"></script>
@endsection