<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title><?=$title?></title>
    <meta content="" name="description">

    <meta content="" name="keywords">
    <!-- Favicons -->
    <link href="template/FlexStart/assets/img/favicon.png" rel="icon">
    <link href="template/FlexStart/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="template/FlexStart/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/FlexStart/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="template/FlexStart/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="template/FlexStart/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="template/FlexStart/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="template/FlexStart/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="template/FlexStart/assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: FlexStart - v1.9.0
    * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

        <a href="/" class="logo d-flex align-items-center">
              <span><?=$config->getValue('admin_name')?></span>
        </a>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#hero">Trang Chủ</a></li>
                <li><a class="nav-link scrollto" href="#services">Dịch Vụ</a></li>
                <li><a class="nav-link scrollto" href="#pricing">Bảng giá</a></li>
                <li><a class="nav-link scrollto" href="#footer">Liên Hệ</a></li>
                <li><a class="getstarted scrollto" href="/login">Đăng nhập</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">Trang web quảng cáo truyền thông xã hội tốt nhất</h1>
                <h2 data-aos="fade-up" data-aos-delay="400">Cung cấp dịch vụ Mạng xã hội với chi phí tối ưu nhất, chuyên cung cấp các dịch vụ mạng xã hội như Facebook, Instagram, Tiktok, Youtube,...</h2>
                <div data-aos="fade-up" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <a href="login"
                           class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                            <span>Đăng Nhập / Đăng Ký</span>
                            <i class="bi bi-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                <img src="template/FlexStart/assets/img/hero-img.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>

</section><!-- End Hero -->

<main id="main">
 



    <!-- ======= Services Section ======= -->
 <section id="services" class="services">

      <div class="container aos-init aos-animate" data-aos="fade-up">

        <header class="section-header">
          <p>Dịch Vụ</p>
       
        </header>

        <div class="row gy-4">

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
            <div class="service-box blue">
              <img src="https://img.icons8.com/ios-filled/100/000000/facebook-new.png">
              <h3>Facebook</h3>
              <p>Cung cấp những dịch vụ về nền tảng này như buff follows, buff tym, buff share, buff view v.v.v…</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
            <div class="service-box orange">
              <img src="https://img.icons8.com/ios-filled/100/000000/instagram-new.png">
              <h3>Instagram</h3>
              <p>Tăng follows, tăng like bài viết, tăng like theo tháng v.v.v… các vấn đề liên quan tới tài khoản</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
            <div class="service-box green">
              <img src="https://img.icons8.com/ios-filled/100/000000/shopee.png">
              <h3>Shopee</h3>
              <p>Tài khoản follow là tài khoản người Việt Nam, mua follow Shopee không mật khẩu</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="500">
            <div class="service-box red">
              <img src="https://img.icons8.com/ios-filled/100/000000/youtube-play.png">
              <h3>Youtube</h3>
              <p>Cung cấp những dịch vụ buff sub, buff view, buff giờ xem, buff like và comment cho youtube của bạn</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="600">
            <div class="service-box purple">
              <img src="https://img.icons8.com/ios-filled/100/000000/tiktok--v1.png">
              <h3>Tiktok</h3>
              <p>Cung cấp những dịch vụ buff sub, buff view, buff giờ xem, buff like và comment…</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="700">
            <div class="service-box pink">
              <img src="https://img.icons8.com/ios-filled/100/000000/code.png">
              <h3>Dịch vụ khác</h3>
              <p>Thiết kế web, bán hosting + Domain,,,,</p>
              <a href="#" class="read-more"><span>Xem ngay</span> <i class="bi bi-arrow-right"></i></a>
            </div>
          </div>

        </div>

      </div>

    </section>
   <!-- ======= Values Section ======= -->
    <section id="values" class="values">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <p>CHÚNG TÔI CAM KẾT</p>
            </header>

            <div class="row">

                <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="box">
                        <img src="template/FlexStart/assets/img/values-1.png" class="img-fluid" alt="">
                        <h3>Tạo ra lợi nhuận</h3>
                        <p>Với tôn chỉ hoạt động của chúng tôi “Tạo ra lợi nhuận cho khách hàng” Cho nên lợi nhuận của khách hàng chính là sự sống còn của chúng tôi. Chúng tôi nghiên cứu, vận hành làm sao cho các bạn mang lại càng nhiều lợi nhuận càng tốt.</p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
                    <div class="box">
                        <img src="template/FlexStart/assets/img/values-2.png" class="img-fluid" alt="">
                        <h3>Hợp tác lâu dài</h3>
                        <p>Với hơn 4 năm hoạt động trong lĩnh vực Digital Marketing, chúng tôi đã thiết lập hệ thống kinh doanh cho rất nhiều khách hàng với rất nhiều sản phẩm. Bạn có cơ hội được giới thiệu sản phẩm đến những đối tác phù hợp với bạn.</p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="600">
                    <div class="box">
                        <img src="template/FlexStart/assets/img/values-3.png" class="img-fluid" alt="">
                        <h3>Hỗ trợ khách hàng</h3>
                        <p>Chúng tôi sẽ luôn cùng đồng hành với sự phát triển của bạn cho đến khi bạn không còn cần chúng tôi nữa. Sự cam kết này thể hiện sự tận tâm của chúng tôi đối với khách hàng đúng theo tiêu chí “một ngày cộng tác, trọn đời làm bạn”!

</p>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- End Values Section -->
    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <p>Cấp bậc và ưu đãi</p>
            </header>

            <div class="row gy-4" data-aos="fade-left">

                <div class="col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="100">
                    <div class="box">
                        <h3 style="color: #07d5c0;">Thành viên</h3>

                        <img src="template/FlexStart/assets/img/pricing-free.png" class="img-fluid" alt="">
                        <ul>
                            <li>Sử dụng đầy đủ dịch vụ</li>
                            <li>Box chat hỗ trợ</li>
                            <li>Có bảo hành theo dịch vụ</li>
                            <li class="na">Ưu đãi / Giảm giá</li>
                            <li class="na">Tạo website riêng</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="200">
                    <div class="box">
                        <span class="featured">Featured</span>
                        <h3 style="color: #65c600;">Cộng tác viên</h3>
                        <img src="template/FlexStart/assets/img/pricing-starter.png" class="img-fluid" alt="">
                        <ul>
                            <li>Sử dụng đầy đủ dịch vụ</li>
                            <li>Box chat hỗ trợ</li>
                            <li>Có bảo hành theo dịch vụ</li>
                            <li>Ưu đãi / Giảm giá</li>
                            <li class="na">Tạo website riêng</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="300">
                    <div class="box">
                        <h3 style="color: #ff901c;">Đại lý</h3>
                        <img src="template/FlexStart/assets/img/pricing-business.png" class="img-fluid" alt="">
                        <ul>
                            <li>Sử dụng đầy đủ dịch vụ</li>
                            <li>Box chat hỗ trợ</li>
                            <li>Có bảo hành theo dịch vụ</li>
                            <li>Ưu đãi / Giảm giá lớn</li>
                            <li>Tạo website riêng</li>
                        </ul>
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>


            </div>

        </div>

    </section><!-- End Pricing Section -->


</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">


    <div class="footer-top">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="index.html" class="logo d-flex align-items-center">
                        <img src="template/FlexStart/assets/img/logo.png" alt="">
                        <span><?=$config->getValue('admin_name')?></span>
                    </a>
                    <p><?=$config->getValue('admin_name')?> chuyên cung cấp các dịch vụ, giải pháp và tiện ích kinh doanh giúp gia tăng doanh số, mở rộng nhận diện thương hiệu và tầm ảnh hưởng trên các kênh truyền thông và mạng xã hội!.!.</p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6 footer-links">

                </div>

                <div class="col-lg-2 col-6 footer-links">

                </div>

                <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                    <h4>Liên Hệ</h4>
                        <strong>Phone:</strong> <?=$config->getValue('sdt')?><br>
                        <strong>Facebook:</strong> <a href="https://www.facebook.com/<?=$config->getValue('fanpage')?>">Liên hệ</a><br>
                    </p>
<?=$config->getValue('admin_name')?>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span><?=$config->getValue('admin_name')?></span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexstart-bootstrap-startup-template/ -->
            Designed by <a href="#"><?=$config->getValue('admin_name')?></a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="template/FlexStart/assets/vendor/purecounter/purecounter.js"></script>
<script src="template/FlexStart/assets/vendor/aos/aos.js"></script>
<script src="template/FlexStart/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="template/FlexStart/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="template/FlexStart/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="template/FlexStart/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="template/FlexStart/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="template/FlexStart/assets/js/main.js"></script>

</body>

</html>
