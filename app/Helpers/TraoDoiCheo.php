<?php

namespace App\Helpers;

use HTTP_Request2;

class TraoDoiCheo
{
    private $baseURL = 'https://traodoicheo.com';
    private $token = 'dHJpIGRlcCB0cmFpIHZhaSBkYWkg';

    public function post($url, $data)
    {
        $request = new HTTP_Request2();
        $request->setUrl($this->baseURL . $url);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        ));
        $data["token"] = $this->token;
        $request->addPostParameter($data);

        $response = $request->send();
        if ($response->getStatus() == 200) {
            return (object)json_decode($response->getBody(), true);
        } else {
            return (object)[
                "status" => 0,
                "message" => $response->getReasonPhrase()
            ];
        }
    }

    public function buyFollow($id, $amount, $type = 0)
    {
        return $this->post('/api/tangFollow', [
            "id" => $id,
            "qty" => (int)$amount,
            "type" => $type
        ]);
    }

    public function buyLike($id, $amount, $type = 0)
    {
        return $this->post('/api/tangLike', [
            "id" => $id,
            "qty" => (int)$amount,
            "type" => $type
        ]);
    }

    public function buyPageLike($id, $amount, $type = 0)
    {
        return $this->post('/api/tangLikePage', [
            "id" => $id,
            "qty" => (int)$amount,
            "type" => $type
        ]);
    }
}


