<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cấu hình website</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
    <h1>Cấu hình website</h1>
    @if (isset($profile['phone']))
        <p><strong class="text-success">Trạng thái: Đã cấu hình thành công ! </strong>
            <br /><span>Username: <b>{{$profile['phone']}}</b>  </span> <br />Password: <b>[Trùng
                với TK của HKNET.VN]</b></p>
        <p>Tài khoản admin sẽ trùng với tài khoản của HKNET.VN của bạn.</p>
    @else
        <p><strong class="text-danger">Trạng thái: Chưa cấu hình</strong></p>
        <p><strong class="text-info">Token phải được duyệt mới có thể thêm. <br /> Vui lòng liên hệ với Admin để duyệt token.</strong></p>
    @endif

</div>

<div class="container">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form id="rut_tien_vi_2" class="form_ajax" action="/config/save">
                    <div class="form-group">

                        <div class="form-group">
                            <label for="">Token <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="token"
                                   value="<?=$config->get('token')->value?>" required>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label for="">Tiêu đề <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="title"
                                   value="<?=$config->get('title')->value?>" required>
                        </div>

                        <div class="form-group">
                            <label for="">Logo <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="logo"
                                   value="<?=$config->get('logo')->value?>" required>
                        </div>

                        <div class="form-group">
                            <label for="">Favicon <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="favicon"
                                   value="<?=$config->get('favicon')->value?>" required>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />


                        <div class="form-group">
                            <label for="">ID Fanpage <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="fanpage"
                                   value="<?=$config->get('fanpage')->value?>" required>
                        </div>


                        <div class="form-group">
                            <label for="">Tên Admin <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="admin_name"
                                   value="<?=$config->get('admin_name')->value?>" required>
                        </div>


                        <div class="form-group">
                            <label for="">Facebook Admin <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="fb_admin"
                                   value="<?=$config->get('fb_admin')->value?>" required>
                        </div>


                        <div class="form-group">
                            <label for="">SĐT <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="sdt"
                                   value="<?=$config->get('sdt')->value?>" required>
                        </div>


                        <hr />

                        <h3>Cấp bậc</h3>

                        <div class="form-group">
                            <label for="">Tên gọi cấp 1 <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="agency_name"
                                   value="<?=$config->get('agency_name')->value?>" required>
                        </div>

                        <div class="form-group">
                            <label for="">Tên gọi cấp 2 <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="collaborators_name"
                                   value="<?=$config->get('collaborators_name')->value?>" required>
                        </div>

                        <div class="form-group">
                            <label for="">Tên gọi cấp 3 <span class="text-danger">(*)</span>:</label>
                            <input class="form-control" type="text" name="guest_name"
                                   value="<?=$config->get('guest_name')->value?>" required>
                        </div>

                        @php
                            $defaultRole = $config->getValue('default_role');
                        @endphp
                        <div class="form-group">
                            <label for="">Cấp mặc định khi tạo tài khoản: <span
                                        class="text-danger">(*)</span>:</label>
                            <select class="form-control" id="sel1" name="default_role">
                                <option value="1" @if ($defaultRole === '1') selected @endif>Cấp 1</option>
                                <option value="2" @if ($defaultRole === '1') selected @endif>Cấp 2</option>
                                <option value="3" @if ($defaultRole === '3') selected @endif>Cấp 3</option>
                            </select>
                        </div>

                        <div style="display: flex; justify-content: space-between;">
                            <button type="submit" class="btn btn-primary">Lưu cấu hình</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>
<script>
    $(function () {
        'use strict';
        $('.form_ajax').submit(e => {
            const form = $(e.target);
            const submitButton = form.find('[type="submit"]');
            const url = form.attr('action');

            submitButton.attr('disabled', true);

            let values = {};
            const formData = form.serializeArray();
            $(formData).each(function (index, obj) {
                values[obj.name] = obj.value;
            });

            $.ajax({
                type: 'POST',
                url,
                data: values
            }).then(res => {
                const message = res && res.message;
                swal(
                    {
                        title: 'Thành công !!',
                        text: message,
                        type: 'success'
                    }, () => {
                        window.location.reload();
                    });
            }).catch(err => {
                const message = err.responseJSON && err.responseJSON.message || 'Lỗi không xác định';
                swal({
                    title: 'Lỗi !!',
                    text: message,
                    type: 'error'
                });
            }).always((e) => {
                submitButton.attr('disabled', false);
            });
            e.preventDefault();
        });

    });
</script>

</body>
</html>

