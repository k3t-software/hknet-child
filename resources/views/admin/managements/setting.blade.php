@extends('admin.layouts.master')

@section('content')
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Cài đặt bổ sung</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Quản lý</a></li>
                <li class="breadcrumb-item active">Bổ sung</li>
            </ol>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row p-3">
                            <div class="col-md-8">
                                <h3 class="card-title">Cài đặt bổ sung</h3>
                            </div>
                        </div>


                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#banner">Banner</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#newfeed">Newfeeds</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#gifCode">Gif Code</a>
                            </li>
                        </ul>

                        <hr />
                        <div class="tab-content">
                            <div class="tab-pane fade" id="newfeed">
                                <form class="form_ajax" action="/admin/newfeed/save">
                                    <div class="form-group">
                                        <label for="">Tiêu đề: </label>
                                        <input class="form-control" type="text" name="title" target="_blank"
                                               value="" placeholder="Tiêu đề" required>
                                    </div>

                                    <div style="height: 400px;">

                                    <textarea id="editor"  name="content" data-autosave="editor-content" autofocus required>
</textarea>

                                    </div>

                                    <div style="display: flex; justify-content: space-between;">
                                        <button type="submit" class="btn btn-primary">Thêm thông báo</button>
                                    </div>
                                </form>
                                <hr />
                                <ul class="list-group">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Content</th>
                                            <th scope="col">Created</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($newfeeds as $index => $newfeed)
                                            <tr>
                                                <th scope="row">{{$index + 1}}</th>
                                                <td>{{$newfeed['title']}}</td>
                                                <td style="max-width: 200px; max-height: 200px;">{!!$newfeed['content']!!}</td>
                                                <td>{{$newfeed['created_at']}}</td>
                                                <td>
                                                    <button class="btn btn-danger btn-xs"
                                                            onclick="deleteFeed('{{$newfeed['id']}}')">Xóa
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </ul>
                            </div>

                            <div class="tab-pane fade" id="gifCode">
                                <form id="gifCode">
                                    <div class="form-group">
                                        <label for="">GifCode: </label>
                                        <input class="form-control" type="text" name="gifCode" id="code" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="">Quantity: </label>
                                        <input class="form-control" type="number" name="quantity" id="quantity"
                                               required />
                                    </div>

                                    <div class="form-group">
                                        <label for="">Value: </label>
                                        <input class="form-control" type="number" name="value" id="value" required />
                                    </div>


                                    <button type="submit" class="btn btn-primary" id="btn-submit">Add</button>
                                </form>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">GifCode</th>
                                        <th scope="col">Value</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Used</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($giftCodes as $index => $giftCode)
                                        <tr>
                                            <th scope="row">{{$index + 1}}</th>
                                            <td>{{$giftCode->code}}</td>
                                            <td>{{$giftCode->value}}</td>
                                            <td>{{$giftCode->quantity}}</td>
                                            <td>{{$giftCode->used}}</td>
                                            <td>
                                                <button class="btn  btn-xs btn-danger"
                                                        onClick="deleteGift({{$giftCode->id}})">
                                                    Xóa
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                            @foreach ($settings as $setting)
                                @if($setting->name ==='banner')
                                    @php
                                        $popupSettings = json_decode($setting->value, true)
                                    @endphp
                                    <div class="tab-pane active" id="banner">
                                        <form id="banner-settings">
                                            <div class="custom-control" style="padding-left: 0;">
                                                <input type="checkbox" id="isShow" name="isShow" {{isset($popupSettings['isShow']) ?
                                    'checked': ''}}>
                                                <label for="isShow">Bật Popup</label>
                                            </div>

                                            <br />

                                            <div class="form-group">
                                                <label for="">Tiêu đề:</label>
                                                <input class="form-control" type="text" rows="5" name="title" required
                                                       value="{{isset($popupSettings['title']) ? $popupSettings['title'] : ''}}"></input>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Banner HTML:</label>
                                                <textarea id="html"  name="html" data-autosave="editor-content" autofocus required>{{isset($popupSettings['html']) ? $popupSettings['html'] : ''}}</textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary" id="btn-submit">Cập nhật
                                            </button>
                                        </form>
                                    </div>


                                @endif
                            @endforeach


                        </div>
                    </div>
                </div>


            </div>
            @endsection

            @section('scripts')
                <script src="{{asset("admin/js/features/setting.js")}}"></script>
                <script>

                    $('#editor').summernote({
                        height: 300
                    });

                    $('#html').summernote({
                        height: 200
                    });


                </script>
@endsection
