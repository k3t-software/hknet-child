@extends('admin.layouts.master')


@section('content')

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">API</h3>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="input-group col-md-12">
                        <div class="input-group-prepend">
                            <button class="btn btn-info" type="button">API Token</button>
                        </div>
                        <input name="price" disabled="" type="text" class="form-control" value="{{$token}}">
                    </div>

                </div>
                <br>
                <div role="alert" class="alert alert-warning">
                    <h6 class="font-bold">Chú ý:</h6>
                    <h6 class="font-bold"> Không chia sẽ token cho bất kỳ ai</h6>
                </div>

            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="panel-heading"><h2 class="panel-title"><span
                                        class="glyphicon glyphicon-ok-circle"></span> API Facebook</h2></div>

                        <div class="col-sm-12">
                            <div class="well">
                                <div class="row header-card">
                                    <div class="col-sm-12">
                                    </div>
                                </div>
                                <div class="row well-body">
                                    <div class="col-sm-12">
                                        <div class="center-big-content-block">

                                        </div>
                                    </div>
                                </div>
                                <div class="PaddingDiv">
                                    <p>
                                    </p>
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <select class="form-control input-sm" id="service_type">
                                                <option value="1">Tăng Follow</option>
                                                <option value="2">Tăng Follow Nhanh</option>
                                                <option value="3">Tăng Like Post</option>
                                                <option value="4">Tăng Like Page</option>
                                                <option value="5">Tăng Comment</option>
                                                <option value="6">Tăng Đánh Giá Page</option>
                                                <option value="7">Tăng Mắt Livestream</option>
                                                <option value="8">Vip Like</option>
                                                <option value="9">Vip Comment</option>
                                            </select>
                                        </div>
                                    </form>
                                    <p></p>
                                </div>
                                <div class="idContainer">

                                    <div id="type_1" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/subthuc</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>amount</td>
                                                    <td>Số lượng</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_2" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/subnhanh</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>amount</td>
                                                    <td>Số lượng</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_3" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/likepost2</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>amount</td>
                                                    <td>Số lượng</td>
                                                </tr>
                                                <tr>
                                                    <td>reaction</td>
                                                    <td>Loại cảm xúc(LIKE)</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_4" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/likepagethuc</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>amount</td>
                                                    <td>Số lượng</td>
                                                </tr>
                                                <tr>
                                                    <td>reaction</td>
                                                    <td>Loại cảm xúc(LIKE)</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_5" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/buffcomment</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>noidung</td>
                                                    <td>Mỗi nội dung cách nhau dấu |</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_6" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/buffreviewpage</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>noidung</td>
                                                    <td>Mỗi nội dung cách nhau dấu |</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_7" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/bufflive</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>amount</td>
                                                    <td>Số lượng</td>
                                                </tr>
                                                <tr>
                                                    <td>min</td>
                                                    <td>Thời gian giữ mắt (bội của 30)</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_8" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/viplike</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>package</td>
                                                    <td>Gói like (Bội của 50)</td>
                                                </tr>
                                                <tr>
                                                    <td>period</td>
                                                    <td>Thời gian cài (Bội của 30)</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="type_9" style="display: none;" class="paramFields">
                                        <div class="table table-bordered dataTable no-footer">
                                            <li class="list-group-item">URL : <strong style="color:red">https://hknet.vn/api/vipcomment</strong>
                                            </li>
                                            <li class="list-group-item">Method : <strong style="color:red">POST</strong>
                                            </li>
                                            <table class="table service-tablwa service-tablwa-V2 service-well">
                                                <thead>
                                                <tr>
                                                    <th class="width-40">Params</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Token</td>
                                                    <td> API Token của bạn</td>
                                                </tr>
                                                <tr>
                                                    <td>id</td>
                                                    <td>ID Facebook</td>
                                                </tr>
                                                <tr>
                                                    <td>package</td>
                                                    <td>Gói like (Bội của 50)</td>
                                                </tr>
                                                <tr>
                                                    <td>period</td>
                                                    <td>Thời gian cài (Bội của 30)</td>
                                                </tr>
                                                <tr>
                                                    <td>gender</td>
                                                    <td>giới tính (nam/nữ)</td>
                                                </tr>
                                                <tr>
                                                    <td>noidung</td>
                                                    <td>Mỗi nội dung cách nhau dấu |</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="PaddingDiv">
                                    <p><strong>Example response</strong></p>
                                    <div class="code-container">
                                <pre id="editor-2">{
                            "order": 23501
                        }</pre>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->


    <script type="text/javascript" src="https://cdn.mypanel.link/libs/jquery/1.12.4/jquery.min.js">
    </script>
    <script type="text/javascript" src="https://cdn.mypanel.link/global/596z6ya3isgxcipy.js">
    </script>
    <script type="text/javascript" src="https://cdn.mypanel.link/global/5hxylqaxi6p0i2wr.js">
    </script>



    <script type="text/javascript">
        window.modules.layouts = { 'theme_id': 3, 'auth': 1 };     </script>
    <script type="text/javascript">
        window.modules.api = [];     </script>
    <script type="text/javascript">
        document.getElementById('service_type').addEventListener('change', function (event) {
            const value = event.target.value;
            for (let element of document.getElementsByClassName('paramFields')) {
                if (element.id.includes(value)) {
                    element.style.display = 'block';
                } else {
                    element.style.display = 'none';
                }
            }
        });
    </script>


@endsection

@section('scripts')


@endsection
