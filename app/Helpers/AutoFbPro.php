<?php

namespace App\Helpers;

class AutoFbPro
{
    private $baseURL = 'https://autofb.pro';
    private $token;

    public function login($username, $password)
    {
        $request = new \HTTP_Request2();
        $request->setUrl($this->baseURL . "/api/auth");
        $request->setMethod(\HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/x-www-form-urlencoded'
        ));
        $request->addPostParameter(array(
            'username' => $username,
            'password' => $password
        ));
        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {
                $userInfo = json_decode($response->getBody(), true);
                if (isset($userInfo['token'])) {
                    $this->token = $userInfo['token'];
                    return true;
                }
                return false;
            } else {
                echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                    $response->getReasonPhrase();
                return false;
            }
        } catch (\HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return false;
        }
    }

    private function post($url, $data, $dataType = 'form')
    {
        if (!isset($this->token)) {
            echo "[Error] Please login first";
            return;
        }

        if ($dataType === 'form') {
            $contentType = 'application/x-www-form-urlencoded';
        } else {
            $contentType = 'application/json';
        }

        $request = new \HTTP_Request2();
        $request->setUrl($this->baseURL . $url);
        $request->setMethod(\HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => true
        ));
        $request->setHeader(array(
            'Content-Type' => $contentType,
            'ht-token' => $this->token
        ));

        if ($dataType === 'form') {
            $request->addPostParameter($data);
        } else {
            $request->setBody($data);
        }

        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {
                $res = json_decode($response->getBody(), true);
                if ($res['status'] === 200) {
                    return (object)[
                        'success' => 1
                    ];
                }
                return (object)[
                    'success' => 0,
                    'message' => $res['message']
                ];
            } else {
                return (object)[
                    'success' => 0,
                    'message' => 'Server xảy ra lỗi ko xác dinh'
                ];
            }
        } catch (\HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
            return (object)[
                'success' => 0,
                'message' => $e->getMessage()
            ];
        }
    }

    public function buyComment($idPost, $comments, $gender)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');

        $data = array(
            "dataform" => array(
                "locnangcao" => 0,
                "locnangcao_gt" => $gender,
                "locnangcao_dotuoi_start" => 0,
                "locnangcao_dotuoi_end" => 13,
                "locnangcao_banbe_start" => 0,
                "locnangcao_banbe_end" => 100,
                "profile_user" => $idPost,
                "loaiseeding" => "comment_sv4",
                "baohanh" => 0,
                "sltang" => count($comments),
                "giatien" => 300,
                "ghichu" => "",
                "startDatebh" => $startDateBH,
                "EndDatebh" => $endDateBH,
                "type" => "",
                "list_messages" => $comments
            ),
            "type_api" => "buff_likecommentshare"
        );
        return $this->post('/api/facebook_buff/create', $data);
    }
    public function buyLikeComment($idPost, $amount, $reaction)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');
        $data = '{"dataform": {"locnangcao": 0,"locnangcao_gt": 0,"locnangcao_dotuoi_start": 0,"locnangcao_dotuoi_end": 13,"locnangcao_banbe_start": 0,"locnangcao_banbe_end": 100,"profile_user": "'.$idPost.'","loaiseeding": "like_cmt_sv3","baohanh": 0,"sltang": ' . strval($amount) . ',"giatien": 50,"ghichu": "hknet.vn","startDatebh": "' . $startDateBH . '","EndDatebh": "' . $endDateBH . '","type": "'.$reaction.'","list_messages": [],"tocdolike": 0},"type_api": "buff_likecommentshare"}';
        return $this->post('/api/facebook_buff/create', $data, 'json');
    }
    public function buyLike($idPost, $amount, $reaction)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');
        $data = '{"dataform": {"locnangcao": 0,"locnangcao_gt": 0,"locnangcao_dotuoi_start": 0,"locnangcao_dotuoi_end": 13,"locnangcao_banbe_start": 0,"locnangcao_banbe_end": 100,"profile_user": "'.$idPost.'","loaiseeding": "like_v4","baohanh": 0,"sltang": ' . strval($amount) . ',"giatien": 12,"ghichu": "hknet.vn","startDatebh": "' . $startDateBH . '","EndDatebh": "' . $endDateBH . '","type": "'.$reaction.'","list_messages": [],"tocdolike": 0},"type_api": "buff_likecommentshare"}';
        return $this->post('/api/facebook_buff/create', $data, 'json');
    }
    public function buyLiveTiktok($idPost, $amount, $min)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');
        $data = '{"dataform": {"profile_user": "'.$idPost.'","loaiseeding": "tiktok_buffmat","link": "'.$idPost.'","sltang": "' . strval($amount) . '","giatien": 1,"ghichu": "","tgdtm": '.$min.',"list_messages": [],"infoTiktok": []}';
        return $this->post('/api/tiktok_buff/create', $data, 'json');
    }
    public function buyLike3($idPost, $amount, $reaction)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');
        $data = '{"dataform": {"locnangcao": 0,"locnangcao_gt": 0,"locnangcao_dotuoi_start": 0,"locnangcao_dotuoi_end": 13,"locnangcao_banbe_start": 0,"locnangcao_banbe_end": 100,"profile_user": "'.$idPost.'","loaiseeding": "like_v2","baohanh": 0,"sltang": ' . strval($amount) . ',"giatien": 5,"ghichu": "hknet.vn","startDatebh": "' . $startDateBH . '","EndDatebh": "' . $endDateBH . '","type": "","list_messages": [],"tocdolike": 0},"type_api": "buff_likecommentshare"}';
        return $this->post('/api/facebook_buff/create', $data, 'json');
    }
    public function buyLikePage($idPost, $amount)
    {
        $today = new \DateTime('Asia/Ho_Chi_Minh');
        $startDateBH = $today->format('Y-m-d\TH:i:s');
        $endDateBH = date_add($today, date_interval_create_from_date_string('6 days'))->format('Y-m-d\TH:i:s');
        $data = '{"dataform": {"locnangcao": 0,"locnangcao_gt": 0,"locnangcao_dotuoi_start": 0,"locnangcao_dotuoi_end": 13,"locnangcao_banbe_start": 0,"locnangcao_banbe_end": 100,"profile_user": "'.$idPost.'","loaiseeding": 5,"baohanh": 0,"sltang": ' . strval($amount) . ',"giatien": 31,"ghichu": "hknet.vn","startDatebh": "' . $startDateBH . '","EndDatebh": "' . $endDateBH . '","type": "","list_messages": [],"tocdolike": 0},"type_api": "like_page"}';
        return $this->post('/api/facebook_buff/create', $data, 'json');
    }

}
