
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
                <meta property="og:image" content="https://esms.vn/pic-esms.png" />
                <meta property="og:description" content="Ưu đãi lên tới 1.500.000đ - Hỗ trợ data KH - Target nhiều tiêu chí" />
                <meta property="og:title" content="Esms - Hệ thống SMS Marketing Automation" />
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>HKNet - Chuyên cung cấp và tư vấn dịch vụ marketing tổng thể</title>
    <link rel="shortcut icon" href="http://hknet.vn/newbiz/img/log.ico">
    <meta property="fb:admins" content="100004185550959" />
    <meta name="description" />
    <meta name="keywords" />
    <meta name="author" />
    <meta property="og:title" content="Dịch vụ facebook, Google, Instagram, Youtube, Tư vấn hỗ trợ giải phải tiếp cận khách hàng" />
    <meta property="og:description" />
    <meta property="og:image" content="" />
    <meta property="og:image:secure_url" content="" />
    <meta property="og:image:width" content="" />
    <meta property="og:image:height" content="" />
    <meta property="og:type" content="Website" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta name="twitter:card" content="" />
    <meta name="twitter:title" content="Cung cấp dịch vụ trên nền tảng các mạng xã hội Facebook, Instagram, Youtube,... " />
    <meta name="twitter:description" />
    <meta name="twitter:image" content="" />
    <meta name="DC.language" content="scheme=utf-8 content=vi" />
    <link href="https://hknet.vn/" rel="canonical" />

    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/css/smooth-scroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/jquery.lazy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/Themes/js/isotope.min.js') }}"></script>

    

    <link media="screen" rel="stylesheet" type="text/css" href="{{ asset('/template2/Content/1/v2/css/bootstrap.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/h.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/n.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/r.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/index.css') }}" />
    <link media="screen" type="text/css" rel="stylesheet" href="{{ asset('/template2/Content/1/v2/css/owl.carousel.css') }}" />
        <link media="screen" rel="stylesheet" type="text/css" href="https://hknet.vn/template2/Content/1/v2/css/cubeportfolieo.min.css" />



    <style>


        .client__item > a.btn-background {
            height: auto;
            border: none;
        }

        .bloghome__title {
            text-overflow: ellipsis;
            overflow: hidden;
            height: 1.2em;
            white-space: nowrap;
        }

        .client__body > p.btn-background {
            float: right;
        }

        .list-flex li {
            display: flex;
        }

        @media only screen and (max-width: 2400px) and (min-width: 1600px) {
            .footer:before {
                background-color: #F2F7FF;
                top: -332px;
            }
        }
    </style>

</head>

<body>


    





<section class="head">
    <div class="head__one">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head__one--inner">
                        <div class="head__one--left">
                            <div class="head__one--phone"><a href="tel:0812888896">HOTLINE: 0812 8888 96</a></div>
                        </div>
                        <div class="head__one--right">
                            <div class="head__one--social">
                                <a href="https://facebook.com/hknet"><i class="fa fa-facebook"></i></a>
                                <a href="https://plus.google.com/sales.hknet"><i class="fa fa-google-plus"></i></a>
                                <a href="https://www.youtube.com/channel/UCULxe8wylG3jYCnbjO3g73A"><i class="fa fa-youtube"></i></a>
                            </div>
                            <div class="head__one--language">
                                <div class="head__one--language--selected" id="currentLanguage">Langue</div>
                                <div class="head__one--language--list">
                                    <a onclick="changeLanguage('vi')">Việt Nam</a>
                                    <a onclick="changeLanguage('en')">English</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="head__two">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head__two--inner">
                        <div class="head__two--logo">
                            <h1 title="">
                                <a href="/">
                                    <img src="{{ asset('http://hknet.vn/newbiz/img/logo-hknet.png') }}" align="logo">
                                </a>
                            </h1>
                        </div>
                        <div class="head__two--nav">
                            <ul>
                                <li><a class="scroll" href="#dichvu">Dịch Vụ</a></li>
                                <li><a href="/chinh-sach-gia">Chính sách giá</a></li>
                                <li><a href="/api/docs">API</a></li>
                                	<!-- <li class="parent">
                                    <a href="/huong-dan">Dự Án</a>
                                    <ul>
                                        <li><a href="/huong-dan/huong-dan-su-dung" style="text-decoration:none;">Dịch Vụ Facebook & Instagram</a></li>
                                        <li><a href="/huong-dan/huong-dan-tich-hop" style="text-decoration:none;">sss</a></li>
                                        <li>
                                            <a href="/huong-dan/thu-thuat-marketing" style="text-decoration: none;">th&#7911; thu&#7853;t marketing</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li><a href="/dai-ly">Đại lý</a></li>
                                <li><a href="#contact">Liên hệ</a></li>
                            </ul>
                        </div>
                        <div class="head__two--user">
                            <a id="aSignin" class="btn-outline" href="/login">Đăng Nhập</a>
                            <a id="aSignup" class="btn-background" href="/register">Đăng Ký</a>
                            <div class="btn-group" role="group" id="divAccount" style="display: none;">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                        style="border: 1px solid #7eb142;color: #7eb142;border-radius: 23px;">
                                    <i class="fa fa-user-circle"></i> <span id="spanDisplayName"></span>
                                    <span class="caret"></span>
                                </button>
                                
                            </div>
                        </div>
                        <div class="head__two--nav-mobile">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" id="btn-menu-mobile">
                            <div class="sidebar-nav navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            
                        </div>

                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var currentLang = getCookie('lang');
    if (currentLang) {
        switch (currentLang) {
            case 'vi':
                document.getElementById('currentLanguage').innerHTML = "Việt Nam"; break;
            case 'en':
                document.getElementById('currentLanguage').innerHTML = "English"; break;
            default:
                break;
        }
    }
</script>

    


<section class="hero hero--left lazy" data-src="https://hknet.vn/template2/Content/1/v2/img/bg_slide-min_optimized-compressed.jpg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <figure class="hero__image">
                    <div class="hero__content">
                        <h2 class="hero__title ">Về chúng tôi <br /></h2>
                        <div class="hero__intro">HKNet là đội ngũ phát triển ra các sản phẩm trên nền tảng Mạng Xã Hội (Facebook, Instagram, Google..)<br/> Giúp khách hàng của bạn tiếp cận thương hiệu một cách dễ dàng và hiệu quả.</div>
                        <div class="hero__button"><a class="btn-background" href="/login">Sử Dụng Ngay</a> <a class="btn-white" href="/register">Đăng Ký Ngay</a></div>
                    </div>
                    <a class="hero__btn-down" href="#"><i class="fa fa-long-arrow-down"></i></a>
                </figure>
            </div>
        </div>
    </div>
</section>


        

<section id="dichvu">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-lg-12">
						<h1 class="wow fadeInRightBig animated" data-wow-offset="80" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInRightBig;">
							<center>CHI TIẾT CÁC DỊCH VỤ</center>
						</h1>
						
					
				<!--SERVICE 1-->	
				<div class="col-sm-4 col-lg-4 text-center wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="1s" style="visibility: visible; animation-duration: 2s; animation-delay: 1s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-facebook"></i>
					</div>
					<h6><b>Dịch Vụ FaceBook</b></h6>
					<p>
					HKNet tự hào là đơn vị đối tác cung cấp các dịch vụ facebook một cách nhanh chóng và toàn diện đáp ứng đầy đủ yêu cầu của khách hàng khi kinh doanh phát triển trên internet.
					</p>
				</div>
				
				<!--SERVICE 2-->
				<div class="col-sm-4 col-lg-4 text-center wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="1.5s" style="visibility: visible; animation-duration: 2s; animation-delay: 1.5s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-magic"></i>
					</div>
					<h6><b>Dịch vụ Instagram</b></h6>
					<p>
					Dịch vụ Instagram hiện đang là cách giúp tài khoản Instagram của bạn tăng khả năng hiển thị khi người dùng tìm kiếm, tiếp cận thêm nhiều khách hàng, tăng khả năng bán hàng. 
					</p>
				</div>
				
				<!--SERVICE 3-->
				<div class="col-sm-4 col-lg-4 text-center  wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="2s" style="visibility: visible; animation-duration: 2s; animation-delay: 2s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-youtube"></i>
					</div>
					<h6><b>Dịch Vụ Youtube</b></h6>
                    Chúng tối cung cấp các dịch vụ tăng lượt like, sub, lượt xem nhằm tăng độ uy tín và tin tưởng cho khách hàng và ngoài ra cũng có thể dùng để đạt điều kiện bật kiếm tiền cho kênh ! 
					</p>
				</div>
				
				<!--SERVICE 4-->	
				<div class="col-sm-4 col-lg-4 text-center wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="1s" style="visibility: visible; animation-duration: 2s; animation-delay: 1s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-camera-retro"></i>
					</div>
					<h6><b>Dịch vụ Tiktok</b></h6>
					<p>
						Hiện nay mạng xã hội Tiktok đang phát triển rát thần tốc và để đáp ứng được nhu cầu người sử dụng nhằm tăng độ uy tín và tiếp cận cho thương hiệu, đó chính là lý do chúng tôi ra đời và phục vụ ! 
					</p>
				</div>
				
				<!--SERVICE 5-->
				<div class="col-sm-4 col-lg-4 text-center wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="1.5s" style="visibility: visible; animation-duration: 2s; animation-delay: 1.5s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-phone"></i>
					</div>
					<h6><b>Dịch vụ gọi thoại CSKH</b></h6>
					<p>
					    Bước quan trọng sau khi tìm kiếm được khách hàng đó chính là giữ khách hàng, và dịch vụ gọi thoại chăm sóc khách hàng đã ra đời nhằm hỗ trợ quý khách trong việc telesales và chăm sóc khách hàng của mình !
					</p><br>
				</div>
				
				<!--SERVICE 6-->
				<div class="col-sm-4 col-lg-4 text-center  wow fadeIn animated" data-wow-offset="80" data-wow-duration="2s" data-wow-delay="2s" style="visibility: visible; animation-duration: 2s; animation-delay: 2s; animation-name: fadeIn;">
					<div class="service">
						<i class="fa fa-lightbulb-o"></i>
					</div>
					<h6><b>Dịch Vụ SMS BrandName</b></h6>
					<p>
						Hệ thống tin nhắn SMS quảng cáo và chăm sóc khách hàng, sử dụng đơn giản, dễ dàng ngay trên Web hỗ trợ hiển thị tên thương hiệu (Brandname),Tích hợp được với Website, phần mềm sẵn có của doanh nghiệp thông qua API
					</p><br>
				</div>
				
			</div>
		</div>
			</div>			
		</section>

        
        
        


    <!-- Footer Columns region -->
    <section id="contact" class="center footer">
        <div class="container">
         <div class="row">
                <div class="col-md-12">
                    <div class="footer__products">
                        <label><h3>Liên Hệ:</h3></label>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer__contact">
                        <div class="footer__logo2">
                            <img class="footer__logo footer__logo--2" src="http://hknet.vn/newbiz/img/logo-hknet.png" align="Logo" />
                        </div>
                        <div class="footer__address">
                            <div class="footer__address--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_address@2x.png"></figure>
                            </div>
                            <div class="footer__address--right">
                                <div class="footer__address--item"> 113 Trần Văn Dư, Bắc Mỹ An, Ngũ Hành Sơn, Đà Nẵng 550000</div>
                               <a href="https://goo.gl/maps/Sveinox6eUnvQuL78" target="_blank" class="map-link">
									<i class="fa fa-map-marker"></i>
									<span> Open in Google Maps</span>
								</a>
                            </div>
                        </div>
                        <div class="footer__phone">
                            <div class="footer__phone--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/footer_phone@2x.png"></figure>
                            </div>
                            <div class="footer__phone--right">
                                <div class="footer__address--item">SDT: <a href="tel:0812888896">0812.8888.96</a></div>
                                <div class="footer__address--item">Mail: <a href="mail:Support@hknet.vn">Support@hknet.vn</a></div>
                            </div>
                        </div>
                        <div class="footer__site">
                            <div class="footer__site--left">
                                <figure class="footer__address--image lazy" data-src="/template2/Content/1/v2/img/icon-www@2x.png"></figure>
                            </div>
                            <div class="footer__site--right">
<div data-id="27d1016" class="elementor-element elementor-element-27d1016 elementor-column elementor-col-25 elementor-top-column" data-element_type="column"><div class="elementor-column-wrap  elementor-element-populated"><div class="elementor-widget-wrap"><div data-id="3648821" class="elementor-element elementor-element-3648821 elementor-widget elementor-widget-heading" data-element_type="heading.default"><div class="elementor-widget-container"><h4 class="elementor-heading-title elementor-size-default">Kết nối chúng tôi</h4></div></div><div data-id="0318d71" class="elementor-element elementor-element-0318d71 elementor-widget elementor-widget-facebook-page" data-element_type="facebook-page.default"><div class="elementor-widget-container"><div class="elementor-facebook-widget fb-page fb_iframe_widget" data-href="https://www.facebook.com/hknet/" data-tabs="" data-height="100px" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-hide-cta="false" style="min-height: 1px;height:200px" data-width="290px" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=290&amp;height=100&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;width=290px"><span style="vertical-align: bottom; width: 290px; height: 180px;"><iframe name="f195248541b8c18" height="200px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.10/plugins/page.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dff709b40f7446%26domain%3Dvaway.vn%26origin%3Dhttps%253A%252F%252Fvaway.vn%252Ff3c2f0cb48b2298%26relation%3Dparent.parent&amp;container_width=290&amp;height=200&amp;hide_cover=false&amp;hide_cta=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhknet%2F&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;width=290px" style="border: none; visibility: visible; width: 290px; height: 180px;" class=""></iframe></span></div></div></div></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </section>



    



    

    <script type="text/javascript" src="{{ asset('/template2/Scripts/CustomValidate/eSMS.Uitl.js') }}" id="eSMSUtil') }}"></script>
    
        <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
            n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1273287776096843');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=1273287776096843&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- /JavaScripts -->
    <!-- Google Tag Manager (noscripts) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNMZ896" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="alert alert-success" id="divAlert" style="position: fixed;z-index: 9999999;top: 100px;right: 0px; display:none;">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong id="alertTitle">Thất Bại</strong>
        <hr class="message-inner-separator" />
        <p id="alertContent">
            <strong>Success! </strong> Product have added to your wishlist.
        </p>
    </div>

    <script type="text/javascript">
        $(function () {
        try {
                window.eSMSSDK.Util.init(1, function (s) { console.log(s); })
            } catch (e) {
                console.log(e);
            }
        });

        function showAlert(id) {
            $("#" + id).fadeTo(2000, 500).slideUp(1000, function () {
                $("#" + id).slideUp(1000);
            });
        }

        function ShowNotifyAlert(message, typealert) {
            var popup = $("#divAlert");
            var title = $("#alertTitle");
            var content = $("#alertContent");

            if (typealert === 'info') {
                popup.removeClass('alert-danger');
                popup.addClass('alert-success');
                title.html('Thành công');
                content.html(message);
                showAlert('divAlert');
                return;
            }

            if (typealert === 'error') {
                popup.removeClass('alert-success');
                popup.addClass('alert-danger');
                title.html('Thất bại');
                content.html(message);
                showAlert('divAlert');
                return;
            }
        }
    </script>
    <script type="text/javascript" src="{{ asset('/template2/Scripts/feedback.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/template2/Content/1/v2/js/a.js') }}"></script>


    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5c5e4dd86cb1ff3c14cbba0e/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script async=async defer=defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=203111630936970&autoLogAppEvents=1" nonce="0lIgmPJ4') }}"></script>


    

</body>
</html>
