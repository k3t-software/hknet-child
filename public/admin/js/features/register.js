$(function () {
    'use strict';

    $('#registerBtn').on('click', function () {
        const button = $(this);
        const form = $('#registerForm');
        button.attr('disabled', true);
        let formData = form.serializeArray();
        let data = {};
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        if (data['password'] != data['passwordConfirm']) {
            swal(
                {
                    title: 'Failed!',
                    text: `Mật khẩu xác nhận không đúng !`,
                    type: 'error'
                },
                function () {
                    button.attr('disabled', false);
                }
            );
            return;
        }
        $.ajax({
            url: `/register`,
            method: 'POST',
            data,
            success: function (data) {
                swal(
                    {
                        title: 'Completed !',
                        text: `Đăng ký thành công !`,
                        type: 'success'
                    },
                    function () {
                        location.href = '/logout';
                    }
                );
            },
            error: function (xhr, status, data) {
                const errorMessage = xhr.responseText.length > 150 ? 'Số điện thoại này đã được sử dụng ' : xhr.responseText;
                swal(
                    {
                        title: 'Failed!',
                        text: errorMessage,
                        type: 'error'
                    },
                    function () {
                        button.attr('disabled', false);
                    }
                );
            }
        });
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
