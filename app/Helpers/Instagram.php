<?php

namespace App\Helpers;

class Instagram extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffSub($id, $amount)
    {
        return $this->post('/subinstagram', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLike($id, $amount)
    {
        return $this->post('/likeinstagram', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffView($id, $amount)
    {
        return $this->post('/viewinstagram', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

}
