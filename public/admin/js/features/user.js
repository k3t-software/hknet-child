$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#saveBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const form = $("#infoForm");
        const method = $(this).data("function");

        let formdata = form.serializeArray();
        let data = {};
        $(formdata).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.id = $(this).data("id");
        $.ajax({
            url: `/admin/users/${method}`,
            method: "POST",
            data,
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `Your customer was ${method}d`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
        });
    });

    $("#myTable").on("click", ".deleteBtn", function () {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/users/delete",
                    method: "DELETE",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your user was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                });
            }
        );
    });

    $("#myTable").on("click", ".editBtn", function () {
        const modal = $("#infoModal");
        const form = $("#infoModal").find("#infoForm");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        modal.find("#saveBtn").attr("data-function", "update");
        modal.find("#saveBtn").attr("data-id", id);

        $.ajax({
            method: "GET",
            url: `/admin/users/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                form.find("[name=name]").val(data.user.name);
                form.find("[name=phone]").val(data.user.phone);
                form.find("[name=code]").val(data.user.code);
                form.find("[name=organization]").val(data.user.organization);
                form.find("[name=city]").val(data.user.city);
                form.find("[name=role]").val(data.user.role);
                form.find("[name=password]").parent().hide();

                preloader.css("display", "none");
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "Failed!",
                        text: `Please check your info and try again !`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $("#myTable").on("click", ".addMoneyBtn", function () {
        const id = $(this).data("id");
        $("#addMoneyBtn").attr("data-id", id);
    });

    $("#myTable").on("click", ".changeRoleBtn", function () {
        const id = $(this).data("id");
        const modal = $("#changeRoleModal");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        $("#changeRoleBtn").attr("data-id", id);
        $.ajax({
            method: "GET",
            url: `/admin/users/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                const isAcceptMoneyTransfer = data.user.accept_money_transfer === 1;
                modal.find('#accept_money_transfer').prop('checked', isAcceptMoneyTransfer);
                modal.find("#user-role").val(data.user.role);
                preloader.css("display", "none");
            },
        });
    });

    $("#addMoneyBtn").on("click", function () {
        const id = $(this).data("id");
        const form = $("#addMoneyForm");
        let data = {};
        data.id = id;
        data.amount = form.find("[name=amount]").val();
        console.log(data);
        $.ajax({
            method: "POST",
            url: "/admin/users/addMoney",
            data,
            dataType: "json",
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `Nạp tiền thành công !`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
        });
    });

    $("#changeRole_form").submit(function (e) {
        e.preventDefault();
        const id = $('#changeRoleBtn').data("id");
        const modal = $("#changeRoleModal");

        let data = {};
        data.id = id;

        const form = $("#changeRole_form");
        let formdata = form.serializeArray();
        $(formdata).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        // data.role = modal.find("[name=role]").val();
        // data.accept_money_transfer = $('#accept_money_transfer').val() === "on" ? 1 : 0;
        $.ajax({
            method: "POST",
            url: "/admin/users/changeRole",
            data,
            dataType: "json",
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `Thay đổi cấp bậc thành công !`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
        });
    });

    $("#infoModal").on("hidden.bs.modal", function (e) {
        console.log("Closed");
        location.reload(true);
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
