﻿(function ($) {
    //'use strict'
    if (!window.eSMSSDK) { window.eSMSSDK = {}; }

    function pad(number, length) { var str = "" + number; while (str.length < length) { str = '0' + str; } return str; }

    window.eSMSSDK.Library = {
        scriptNotify: function () {
            var tag = document.createElement('script');
            tag.src = window.location.protocol + '//' + window.location.host + "/Scripts/notify.min.js";
            tag.defer = true;
            tag.async = true;
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        },
        scriptEsmsAds: function () {
            var tag = document.createElement('script');
            tag.src = window.location.protocol + '//' + window.location.host + "/Scripts/CustomValidate/ads.js";
            tag.defer = true;
            tag.async = true;
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        },
    };

    window.eSMSSDK.Util = {
        AgencyID: null,
        init: function (agency, callback) {
            window.eSMSSDK.Util.AgencyID = agency;
            window.eSMSSDK.Library.scriptNotify();
            if (agency == 1) {
                //window.eSMSSDK.Library.scriptEsmsAds();
            }

            if (callback)
                callback({ status: true, message: 'instance success' });
        },
        getCurrentTimezone: function () {
            var offset = new Date().getTimezoneOffset();
            offset = ((offset < 0 ? '+' : '-') + pad(parseInt(Math.abs(offset / 60)), 2) + pad(Math.abs(offset % 60), 2));

            return offset;
        },
        getCurrentPosition: function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    console.log(position);
                    window.eSMSSDK.Util.reverseGeocodingWithGoogle(position.coords.latitude, position.coords.longitude);
                }, function (error) {
                    try {
                        switch (error.code) {
                            case error.PERMISSION_DENIED:
                                console.log("User denied the request for Geolocation.");
                                break;
                            case error.POSITION_UNAVAILABLE:
                                console.log("Location information is unavailable.");
                                break;
                            case error.TIMEOUT:
                                console.log("The request to get user location timed out.");
                                break;
                            case error.UNKNOWN_ERROR:
                                console.log("An unknown error occurred.");
                                break;
                        }
                    } catch (e) {
                        console.log(e);
                    }
                });
            } else {
                console.log("Geolocation is not supported by this browser.");
            }
        },
        reverseGeocodingWithGoogle: function (latitude, longitude) {
            var GOOGLE_MAP_KEY = 'AIzaSyCeYZT9Xe4jt7-lPG_fiD9roeoJ2IH3bHs';
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key={GOOGLE_MAP_KEY}')
                .then(function (res) { return res.json(); })
                .then(function (response) {
                    console.log("User's Location Info: ", response)
                })
                .catch(function (status) {
                    console.log('Request failed.  Returned status of', status)
                });
        },
        writeFeedback: function (myname, email, subject, content, phone) {
            if (content || content == '') content = subject;
            var url = 'https://esms.vn/Contact/ExternalFeedback?CustomerName=' + myname + '&Email=' + email + '&Subject=' + subject + '&Content=' + content + '&PhoneNumber=' + phone + '&DaiLyId=' + 1;

            $.get(url, function (result) {
                console.log(result);
                try {
                    if (result.code === 200) {
                        console.log(result);
                    } else {
                        console.log(result);
                    }
                } catch (e) {
                    console.log(e);
                }
            });
        },
        setCookie: function (cname, cvalue, exdays) {
            if (!exdays) exdays = 1;
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        getCookie: function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
    };

    //$(function () {
    //    console.log("ready!");
    //    window.eSMSSDK.Util.init(function (s) { console.log(s); })
    //});
})(jQuery);