<?php

namespace App\Http\Controllers;

use App\Order;
use App\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResourceController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = new Resources;
    }


    public function view(Request $req, $orderId)
    {
        $user = Auth::user();
        $data['referCode'] = strtoupper(substr($user->phone, -4). "_" . $user->id);
        $order = Order::where("id", $orderId)->where('user_id', $user->id)->first();
        if (isset($order)) {
            if ($order->status === 0) {
                return "Đơn đang đợi duyệt !";
            }
            return str_replace("\n", "<br />", $order->note);
        } else {
            return "Không tìm thấy đơn hàng này !";
        }
    }

    public function insert(Request $req)
    {
        $data = $req->input("data");
        $type = $req->input("type");

        $accounts = explode("\n", $data);

        $successCount = 0;
        foreach ($accounts as $account) {
            $resource = new Resources();
            $resource->data = $account;
            $resource->type = $type;
            $resource->save();
            $successCount++;
        }

        return response()->json([
            "success" => 1,
            "message" => "Insert success " . $successCount . " resource(s)"
        ]);
    }

}
