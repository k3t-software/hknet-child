
<!DOCTYPE html>
<html lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("admin/images/favicon.png")}}" />

    <title>TIGVIET Event System</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset("admin/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("admin/css/style.css")}}" rel="stylesheet" />
    <!-- You can change the theme colors from here -->
    <link href="{{asset("admin/css/colors/blue.css")}}" id="theme" rel="stylesheet" />

    <style>
        .login-box {
            width: 90%;
            max-width: 900px!important;
        }
        html {
            height: 100%;
        }
        body {
        min-height: 100%;
        }
        #wrapper { 
            height: 100vh!important;
        }
        .preloader {
            position: absolute;
            left: 0;
        }
    </style>

</head>

<body>

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{asset("admin/images/background/login-register.jpg")}}); overflow: auto;">        
            <div class="login-box card">
                <div class="card-header">
                    <strong>Register Form</strong>
                </div>
                <div class="card-body">
                    <div class="preloader">
                        <svg class="circular" viewbox="25 25 50 50">
                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
                    </div>
                    <div class = "infoPanel hidden-xs-up">
                        <div class="row">
                            <div class="col-md-6 b-r"> <strong class = "card-title">Registered Information</strong>
                                <br>
                                <p class="text-muted mt-3 post-title" >Name: <strong class = "text-inverse" name = "name"></strong></p>
                                <p class="text-muted post-title" >Email: <strong class = "text-inverse" name = "email"></strong></p>
                                <p class="text-muted post-title" >Phone number: <strong class = "text-inverse" name = "phone"></strong></p>
                                <p class="text-muted post-title" >Ticket point: <strong class = "text-inverse" name = "address"></strong></p>
                            </div>
                            <div class="col-md-6"> <strong>Share Information</strong>
                                <br>
                                <div class="row mt-3">
                                    <div class = "col-6">
                                        <img id ="qrCode" src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=tigviet.com" alt="qrcode">
        
                                    </div>
                                    <div class = "col-6"><strong>Share this code with your friends to get VIP ticket <span class="text-danger">*</span><strong></div>
    
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-danger" type="button">Sharing link</button>
                                        </div>
                                        <input type="text" id = "sharingLink"  class="form-control" placeholder="Sharing link">
                                        <div class="input-group-append">
                                            <button class="btn btn-success" id ="copyBtn" type="button">Copy to clipboard</button>
                                        </div>
                                        
                                    </div>
                                    <span class="help-block text-muted"><small><span class="text-danger">*</span> If you invite 10 guests, you will be a VIP guest in this event.</small></span> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <form id = "registerForm" class="m-t-20" novalidate="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <h5>Fullname<span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" name="name" class="form-control" required="" data-validation-required-message="This field is required" /> </div>
                        </div>
                        <div class="form-group">
                            <h5>Email <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="email" name="email" class="form-control" required="" data-validation-required-message="This field is required" /> </div>
                        </div>

                        <div class="form-group">
                            <h5>Phone number <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <input type="text" name="phone" class="form-control" required="" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="No Characters Allowed, Only Numbers" /> </div>
                        </div>

                        <div class="form-group">
                            <h5>You will get the ticket at <span class="text-danger">*</span></h5>
                            <div class="controls">
                                <select name="ticket_point" id="select" required="" class="form-control">
                                    <option value="" />Select your ticket point
                                    @foreach ($users as $user)
                                    <option value="{{$user->id}}" />{{$user->address}}
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <input type="hidden" id ="refCode" value="{{$ref_code ?? ""}}">

                    </form>
                </div>
                <div class="card-footer">
                    <div class="row button-group">
                        <div class="col-lg-6 col-md-4">
                            <button type="submit" id = "registerBtn" form = "registerForm" class="btn waves-effect waves-light btn-block btn-info">Submit</button>
                        </div>
        
                        <div class="col-lg-6 col-md-4">
                            <button type="button" id = "resetBtn" class="btn waves-effect waves-light btn-block btn-secondary">Reset</button>
                        </div>

                    </div>
                </div>
              </div>
        </div>
        
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
       <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset("admin/plugins/jquery/jquery.min.js")}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset("admin/plugins/bootstrap/js/popper.min.js")}}"></script>
    <script src="{{asset("admin/plugins/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset("admin/js/jquery.slimscroll.js")}}"></script>
    <!--Wave Effects -->
    <script src="{{asset("admin/js/waves.js")}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset("admin/js/sidebarmenu.js")}}"></script>
    <!--stickey kit -->
    <script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
    <!--stickey kit -->
    <script src="{{asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")}}"></script>
    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
    <script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>
    

    <!--Custom JavaScript -->
    <script src="{{asset("admin/js/validation.js")}}"></script>
    <script src="{{asset("admin/js/features/register.js")}}"></script>
    <script src="{{asset("admin/js/custom.min.js")}}"></script>
    
</body>

</html>