<?php

namespace App\Helpers;

class Twitter extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffFollow($id, $amount)
    {
        return $this->post('/followTwitter', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLike($id, $amount): object
    {
        return $this->post('/likeTwitter', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }
}
