const loadingImage = `<img src="https://mir-s3-cdn-cf.behance.net/project_modules/max_632/04de2e31234507.564a1d23645bf.gif" />`;
const userRoles = [
    '<span class="badge badge-primary">Admin</span>',
    '<span class="badge badge-success">Đại lý</span>',
    '<span class="badge badge-info">Cộng Tác Viên</span>',
    '<span class="badge badge-danger">Khách</span>'];

const formatPrice = (price = 0) => {
    return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(price);
};

const getLowerUser = (userId = '', tableId = 'refer_user_table', shouldShowModal = false, fatherId = null) => {
    return axios.get('/admin/helper/referUser', {
        params: {
            id: userId
        }
    }).then(res => {
        const users = res.data || [];
        const userList = users.reduce((result, user, index) => {
            result += `<tr>
                                    <td class="text-center">${index + 1}</td>
                                    <td class="text-center">${user.name}</td>
                                    <td class="text-center">${user.phone}</td>
                                    <td class="text-center">${userRoles[user.role]}</td>
                                    <td class="text-center">${formatPrice(user.balance)}</td>
                                    <td class="text-center">${formatPrice(user.total_charge)}</td>
                                    <td class="text-center">${moment(user.created_at).format('DD/mm/yyyy hh:mm:ss')}</td>
                                    <td class="text-center">
                                        <button class="btn btn-info btn-xs" onclick="viewChildUser(${user.id}, '${user.name}', '${user.phone}', '${user.role}', ${userId})">${user.lower_count} Khách hàng</button>
                                    </td>
                                </tr>`;
            return result;
        }, '');

        const tbodyElement = $('#' + tableId).find('tbody')[0];
        $(tbodyElement).html('');
        $(tbodyElement).append(userList);

        if (shouldShowModal) {
            $('#refer_user_modal_' + userId).modal('show');
            $('#refer_user_modal_' + userId).on('hidden.bs.modal', function (e) {
                if (fatherId) {
                    $('#refer_user_modal_' + fatherId).css('opacity', 1);
                }
            });
            if (fatherId) {
                $('#refer_user_modal_' + fatherId).css('opacity', 0.5);
            }
        }
    }).catch((error) => {
        console.log(error);
        alert('Some error when get user.');
        return [];
    });
};

const renderModal = (userId, name, phone, role) => {
    const modalContainer = $('#modal-container');

    const isExistModal = $('#refer_user_modal_' + userId)[0];
    if (isExistModal) return null;

    modalContainer.append(`<div class="modal fade bd-example-modal-lg" id="refer_user_modal_${userId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="max-width: 55vw;" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Thành viên dưới của ${name} ${userRoles[role]} (${phone}) </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <div class="table-responsive p-1">
                                <img style="margin: 0 auto; display: none;" class="loading-image" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_632/04de2e31234507.564a1d23645bf.gif" />
                                <table id="refer_user_table_${userId}" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Phone</th>
                                        <th class="text-center">Role</th>
                                        <th class="text-center">Balance</th>
                                        <th class="text-center">Total Charge</th>
                                        <th class="text-center">Created at</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="refer_user_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
          </div>
        </div>
      </div>
    </div>`);
};

const viewChildUser = (userId, name, phone, role, fatherId = null) => {
    renderModal(userId, name, phone, role);
    getLowerUser(userId, 'refer_user_table_' + userId, true, fatherId);
};

getLowerUser('').then(res => {

});
