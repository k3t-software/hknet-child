function showDefaultAPIDoc() {
    console.log("Có chạy")
    setTimeout(function () {
        $("a[href=#blog6]").click();
        $("#blog6").addClass("active");
    }, 1000)
}
(function ($, root, undefined) {

    (function () {
        // DOM ready, take it away
        var $grid = $('.all-client').isotope({
            // options
        });
        $('.filter').on('click', 'a', function (e) {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });
            $(".isotope-filter_links ul li").find(".active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });
        // console.log(window.location.pathname)
        if (window.location.pathname.toLowerCase() === "/SMSApi/ApiDetail".toLowerCase()) {
            $('.accordion .inner a').click(function (e) {
                e.preventDefault()
                $(this).tab('show');
                $(".tab-content.main-tab-content > div.tab-pane").removeClass("active");
                $("#" + e.target.hash.replace("#", "")).addClass("active");
            })
            if (window.location.hash) {
                //$(".tab-content.main-tab-content > div.tab-pane").removeClass("active");
                switch (window.location.hash) {
                    case "#ApiGetBalance":
                        $("a[href=#hamtinnhanhethongsodu]").click();
                        $("#hamtinnhanhethongsodu").addClass("active");
                        break;
                    case "#ApiSendSMSNormal":
                        $("a[href=#hamtinnhanhethongguiTinNhan]").click();
                        $("#hamtinnhanhethongguiTinNhan").addClass("active");

                        break;
                    case "#ApiGetSendStatus":
                        $("a[href=#hamtinnhanhethongkiemtratrangthaitinnhan]").click();
                        $("#hamtinnhanhethongkiemtratrangthaitinnhan").addClass("active");

                        break;
                    case "#ApiSendSMSBrandname":
                        $("a[href=#hamtinnhanhethonglaydanhsachbrandname]").click();
                        $("#hamtinnhanhethonglaydanhsachbrandname").addClass("active");
                        break;
                    default:
                        showDefaultAPIDoc();
                        break;
                }
            } else {
                showDefaultAPIDoc();
            }
        }
        console.log("abc " + window.location.pathname)
    })();
})(jQuery, this);

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhoneNumber10(sData) {
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (sData.match(phoneno)) {
        return true;
    }
    else {
        return false;
    }
}

function validatePhoneNumber11(sData) {
    var phoneno = /^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (sData.match(phoneno)) {
        return true;
    }
    else {
        return false;
    }
}
var moreDesForFeedback = "";
function updateMoreDesForFeedback(mes) {
    moreDesForFeedback = mes;
    $('html, body').animate({
        scrollTop: $($("[id^=form-contact-]")[0]).parent().parent().offset().top
    }, 1000);
}
$("[id^=name_u-]").focusout(function () {
    $("[id^=name_u-]").val($("[id^=name_u-]").val().trim());
})
$("[id^=email_u-]").focusout(function () {
    $("[id^=email_u-]").val($("[id^=email_u-]").val().trim());
})
$("[id^=phone_u-]").focusout(function () {
    $("[id^=phone_u-]").val($("[id^=phone_u-]").val().trim());
})
$("#code").focusout(function () {
    $("#code").val($("#code").val().trim());
})
var sendFeedbackCommon = function (e) {
    e.preventDefault();
    var lang = getCookie('lang');
    fbq('track', 'Purchase', { currency: "USD", value: 30.00 });
    if ($('[id^=name_u-]').val() === '') {
        $('#name_err').removeClass('hidden');
        return;
    } else {
        $('#name_err').addClass('hidden');
    }
    if ($('[id^=email_u-]').val() !== undefined) {
        if ($('[id^=email_u-]').val() === '') {
            $('#email_err').html('Vui lòng nhập email.');
            $('#email_err').removeClass('hidden');
            return;
        } else {
            if (!validateEmail($("[id^=email_u-]").val())) {
                $('#email_err').html('Vui lòng nhập email đúng định dạng.');
                $('#email_err').removeClass('hidden');
                return;
            } else {
                $('#email_err').addClass('hidden');
            }
        }
    }

    if ($('[id^=phone_u-]').val() === '') {
        $('#phone_err').removeClass('hidden');
        $('#phone_err').html("Vui lòng nhập Số điện thoại")
        return;
    } else {
        if (!validatePhoneNumber10($("[id^=phone_u-]").val()) && !validatePhoneNumber11($("[id^=phone_u-]").val())) {
            $('#phone_err').html("Vui lòng nhập số điện thoại thực.")
            $('#phone_err').removeClass('hidden');
            return;
        } else {
            $('#phone_err').addClass('hidden');
        }
    }
    var $btn = $("[id^=btn_SendContact-]").button('loading');
    var name = $('[id^=name_u-]').val();
    var email = $('[id^=email_u-]').val();
    var phone = $('[id^=phone_u-]').val();
    var note = $('[id^=note_u-]').val() === undefined ? "" : $('[id^=note_u-]').val();
    var subject = "";
    switch (location.pathname) {
        case "/dich-vu/dich-vu-email":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ Email-Marketing, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu/dich-vu-sms-marketing":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ SMS-Marketing, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu/dich-vu-sms-2-chieu":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ SMS 2 chiều, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu-sms/sms-theo-dia-diem":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ SMS theo địa điểm, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu/dich-vu-sms-brandname":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ SMS BRANDNAME, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/lien-he":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ SMS của công ty ở trang liên hệ, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu/dich-vu-sms-viber":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn về dịch vụ gửi SMS qua Viber, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dai-ly":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn để trở thành đại lý đối tác, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/chinh-sach-gia":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn ở trang chính sách giá, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        case "/dich-vu/dich-vu-voice":
            subject = 'Khách hàng ' + name + ' vừa yêu cầu tư vấn dịch vụ gọi tự động Evoice, vui lòng liên hệ sớm. Email khách hàng là ' + email + '. Số điện thoại của khách hàng là ' + phone + '.';
            break;
        default:
            break;
    }
    note += moreDesForFeedback;
    moreDesForFeedback = "";
    var dataString = 'name=' + name + '&email=' + email + '&phone=' + phone + '&note=' + note + '&location=' + location.href;
    dataString += subject != "" ? '&Subject=' + subject : "";
    $.ajax({
        type: "POST",
        url: "/SMS/SaveFeelbackCommon",
        dataType: 'JSON',
        data: dataString,
        success: function (result) {
            $btn.button('reset');
            if (result.Status == true) {
                $('[id^=name_u-]').val('');
                $('[id^=email_u-]').val('');
                $('[id^=phone_u-]').val('');
                $('[id^=note_u-]').val('');

                if (lang == "vi") {
                    new swal(
                        'Cảm ơn bạn!',
                        'Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất có thể!',
                        'success'
                    )
                } else {
                    new swal(
                        'Thank You!',
                        'We will get back to you as soon as possible!',
                        'success'
                    )
                }

            }
        },
        error: function (error) {
            console.log(error);
            $btn.button('reset');
            if (lang == "vi") {
                new swal(
                    'Có lỗi!',
                    'Đường truyền không ổn định!',
                    'error'
                )
            } else {
                new swal(
                    'Something something went wrong!',
                    "connection isn't stable",
                    'error'
                )
            }

        }
    });
}

$(document).ready(function () {
    if ($("#PhoneNumber") !== undefined && $("#PhoneNumber").val() !== undefined) {
        if (!validatePhoneNumber10($("#PhoneNumber").val()) && !validatePhoneNumber11($("#PhoneNumber").val())) {
            $("#PhoneNumber").change(function () {
                $.ajax({
                    type: "POST",
                    url: "/Account/WarringCSKHinformation",
                    dataType: "JSON",
                    data: { Phonecustomer: $("#Phonecustomer").val() },
                    success: function (result) {//Ham success xu li
                        if (result.result == true) { }
                    },
                    error: function (error) {
                    }
                });
            });
            return;
        }
    }
    if ($("[id^=phone_u-]").val() !== undefined && $("[id^=phone_u-]").val() !== undefined) {
        if (!validatePhoneNumber10($("[id^=phone_u-]").val()) && !validatePhoneNumber11($("[id^=phone_u-]").val())) {
            $("[id^=phone_u-]").change(function () {
                $.ajax({
                    type: "POST",
                    url: "/Account/WarringCSKHinformation",
                    dataType: "JSON",
                    data: { Phonecustomer: $("#phone_u").val() },
                    success: function (result) {//Ham success xu li
                        if (result.result == true) { }
                    },
                    error: function (error) {
                    }
                });
            });
        }
    }

});