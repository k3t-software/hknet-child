@php
	$status = ['<span class="badge badge-warning">Đang Chạy</span>','<span class="badge badge-success">Đã hoàn thành</span>','<span class="badge badge-warning">Đang Chạy</span>', '<span class="badge badge-danger">Hoàn tiền</span>'];
    $role = ['apiPrice','agencyPrice', 'collaboratorsPrice', 'guestPrice'];

@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Tăng Like Comment</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Chức năng</a></li>
				<li class="breadcrumb-item active">Buff Like Comment</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Tạo mới buff like comment</h4>
						<h6 class="card-Like Posttitle">Chức năng tăng like comment</h6>
						<div class="row pt-3">
							<div class="col-md-6 b-r">
								<form id = "infoForm">
									{{ csrf_field() }}

									<div class = "profile-info">

										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-info" type="button">ID Comment</button>
											</div>
											<input name ="id" type="text" placeholder="Nhập ID comment"  class="form-control" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class = "order-info pt-3">

										<div class="row">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Số lượng</button>
												</div>
												<input data-price = "{{$service[$role[Auth::user()->role]]}}" value="100" min="100" name = "amount" type="number" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
											</div>
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Đơn giá</button>
												</div>
												<input name ="price" disabled type="text" class="form-control" value = "{{$service[$role[Auth::user()->role]]}} đ / {{$service['unit']}}" aria-label="Username" aria-describedby="basic-addon1">
											</div>
										</div>

									<div class = "order-info pt-3">
                                    <div class="input-group-prepend">
													<button class="btn btn-info" type="button">Cảm Xúc</button>

                                    <select class="form-control" name="reaction" id="reaction">
                                                                    <option value="like">LIKE</option>
                                                                    <option value="care">THƯƠNG</option>
                                                                    <option value="love">LOVE</option>
                                                                    <option value="haha">HAHA</option>
                                                                    <option value="wow">WOW</option>
                                                                    <option value="sad">SAD</option>
                                                                    <option value="angry">ANGRY</option>
                                    </select>
												</div>
                                    </div>
										<div class="row pt-3">
											<div class="input-group col-md-6">
												<div class="input-group-prepend">
													<button class="btn btn-info" type="button">Tổng tiền</button>
												</div>
												<input disabled data-price = "10000" value =0 name = "total" type="number" class="form-control">
											</div>
											<div class="col-md-6">
												<button type="button" data-service = "{{$service->id}}" data-function = "likecomment" id = "orderBtn" class="btn waves-effect waves-light btn-danger btn-block"><i class="fa fa-shopping-cart"></i> Thực hiện</button>
											</div>

										</div>
									</div><br>

                                <p></p>

								</form>
							</div>
							<div class="col-md-6">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"> </button>
									<h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Lưu ý</h3>
									  
                                                   <p>Like người thật, Lên ngay khi order</p>
                                                    <p>Tối thiểu 50 và tối đa 10000 like</p>
								</div>
							</div>
						</div>
						<hr class = "py-3">
						<h4 class="card-title">Đơn hàng</h4>
						<h6 class="card-Like Posttitle">Danh sách các đơn hàng buff like Post của bạn</h6>
							<div class="table-responsive p-1">
								<table id ="myTable" class="table table-bordered">
									<thead>
										<tr>
											<th class = "text-center">#</th>
											<th>Dịch vụ</th>
											<th class = "text-center">Số lượng</th>
											<th>Facebook ID</th>
											<th class = "text-right">Tổng tiền</th>
											<th class = "text-right">Trạng thái</th>
											<th class = "text-right">Thời gian</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($orders as $order)
											<tr>
												<td></td>
												<td>{{$order->type->name}}</td>
												<td class = "text-center">{{$order->quantity}}</td>
												<td>{{$order->target}}</td>
												<td class = "text-right">{{$order->total}}</td>
												<td class = "text-right">{!!$status[$order->status]!!}</td>
												<td class = "text-right">{{$order->created_at}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

					</div>

				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src="{{asset("admin/js/facebook-buff/sub.js")}}"></script>
@endsection