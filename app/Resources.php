<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
    protected $table = "resources";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public static function buy($amount, $type, $orderId, $user)
    {
        $data = "";
        $amount = (int)$amount;
        $resources = self::where("type", $type)->where("user_id", null)->limit($amount)->get();
        if (count($resources) === $amount) {
            foreach ($resources as $resource) {
                if (strlen($resource) < 1) continue;
                $resource->user_id = $user->id;
                $resource->order_id = $orderId;
                $resource->save();
                $data .= $resource->data . "<br />";
            }
        }
        return $data;
        return self::all()->count();
    }
}
