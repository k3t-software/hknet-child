@extends('admin.layouts.master')


@section('content')

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">API</h3>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->

	<div class="row">

		<div class="col-md-12">

			<div class="row">

				<div class="input-group col-md-12">
					<div class="input-group-prepend">
						<button class="btn btn-info" type="button">API Token</button>
					</div>
					<input name="price" disabled="" type="text" class="form-control" value="{{$token}}">
				</div>

			</div>
			<br>
			<div role="alert" class="alert alert-warning">
				<h6 class="font-bold">Chú ý:</h6>
				<h6 class="font-bold"> Không chia sẽ token cho bất kỳ ai</h6>
			</div>

		</div>

		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Danh sách API</h4>
					<h6 class="card-subtitle">Liên hệ admin nếu gặp vấn đề trong quá trình sử dụng</h6>
					<div class="table-responsive table-bordered">
						<table class="table">
							<tbody>
								<tr>
									<td colspan="2" class="text-center table-info">
										<strong>
											SUB ĐỀ XUẤT
										</strong>
									</td>
								</tr>
								<tr>
									<td>URL</td>
									<td>https://viplike.com.vn/api/subdexuat</td>

								</tr>
								<tr>
									<td>Method</td>
									<td>POST</td>
								</tr>
								<tr>
									<td class = "text-center" colspan="2">
										<strong>
											Body

										</strong>
									</td>
								</tr>
								<tr>
									<td>id</td>
									<td>Facebook ID của đối tượng cần buff (string)</td>
								</tr>
								<tr>
									<td>amount</td>
									<td>Số lượng cân buff (integer)</td>
								</tr>

								<tr>
									<td>token</td>
									<td>Token mà bạn được cấp bởi hệ thống (string)</td>
								</tr>

							</tbody>
						</table>
					</div>

					<div class="table-responsive table-bordered">
						<table class="table">
							<tbody>
								<tr>
									<td colspan="2" class="text-center table-info">
										<strong>
											GET THÔNG TIN ĐƠN HÀNG
										</strong>
									</td>
								</tr>
								<tr>
									<td>URL</td>
									<td>https://viplike.com.vn/api/orders/{id}</td>

								</tr>
								<tr>
									<td>Method</td>
									<td>GET</td>
								</tr>
								<tr>
									<td class = "text-center" colspan="2">
										<strong>
											Query Params
										</strong>
									</td>
								</tr>
								<tr>
									<td>token</td>
									<td>Token mà bạn được cấp bởi hệ thống (string)</td>
								</tr>

								<tr>
									<td class = "text-center" colspan="2">
										<strong>
											Thông Tin Body

										</strong>
									</td>
								</tr>
								<tr>
									<td>id</td>
									<td> ID của đối tượng cần buff</td>
								</tr>
								
								<tr>
									<td>status: </td>
									<td>Trạng thái của đơn hàng (0: Đang Xử Lý, 2 Đang chạy,  1: Hoàn Thành)</td>
								</tr>
								
								<tr>
									<td>quantity:</td>
									<td>Số lượng cân buff </td>
								</tr>

								<tr>
									<td>startcount: </td>
									<td>Số lượng gốc ban đầu</td>
								</tr>


							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection

@section('scripts')


@endsection