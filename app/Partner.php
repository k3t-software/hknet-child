<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = "partners";

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public function getAccountTraoDoiSub($partner_name = "traodoisub.com")
    {
        return self::where('partner_name', $partner_name)->where('used', '<', 9)->where('is_stop', '=',
            0)->get()->first();
    }
}
