<?php

namespace App\Helpers;

class Shopee extends HkNet
{
    public function __construct($token = null)
    {
        parent::__construct($token);
    }

    public function buffSub($id, $amount)
    {
        return $this->post('/buffSubShopee', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }

    public function buffLike($id, $amount): object
    {
        return $this->post('/buffLikeShopee', [
            "id" => $id,
            "amount" => (int)$amount,
        ]);
    }
}
